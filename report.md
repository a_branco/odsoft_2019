# Report

### **Times:**

After running all configurations in a successful build, their performance was compared. It was observed that the sequential build pipeline took 10 minutes and 4 seconds to complete, while the parellel took 6 minutes and 15 seconds. As for the Jenkinsfile configurations, the sequential pipeline took 6 minutes and 27 seconds and the parallel version of it lasted for 3 minutes and 40 seconds. It is worth noting that these times are dependent mostly on the mutation tests. In the sequential Jenkins Build Pipeline configuration these took 6 minutes and 34 seconds to execute and in the parallel version they took 4 minutes and 55 seconds. In the Jenkinsfile configurations, the mutation tests took 3 minutes and 7 seconds and 1 minute and 41 seconds, in the sequential and parallel configurations, respectively. 

In comparison the parallel build is always faster than a sequential build, since the jobs are the same and the parallel can run multiple jobs at the same time and it doesn�t have to wait for the other jobs to finish. This is specially evident when running tests simultaneously to mutation tests, since the mutation tests are the longest tasks in the whole pipeline, taking up most of the time that the pipeline lasts for. This way the unit and integration tests finish before the mutation tests and do not influence the pipeline's total time. 

In comparison the JenkinsFile also runs faster than the Build Pipeline, since scripting is faster than a normal configuration of a pipeline.
It is also worth noting that factors like CPU, RAM and Internet speed can change the time of the build.