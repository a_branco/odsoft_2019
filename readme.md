## **1. Structure of Folders**

![StructureFolders](https://bitbucket.org/mei-isep/odsoft-19-20-nlm-g111/raw/master/images/StructureFolders.jpg)

This repository contains all the developments made within the **Curricular Unit of Organization and Software Development** of the **Master in Informatics Engineering**.  
  
The image above reflects the structure of folders and files we adopt, follows a short description of each:   
  
*  `images`: contains the images used in the documentation of the whole repository;  
*  `odsoft`: contains the relevant files of the two performed Class Assignment, as well as their Self-Assessment;
*  `project`: contains all code and developed implementations;  
*  `calculator`: in the first Class Assignment some of the tasks used the calculator design. This folder contains the implementation of this project;  
*  `gwt`: contains the main project of the entire curricular unit - CMS. The Dockerfile files developed in the project and database are also inserted here.

## **2. How to Run**

### **2.1. Calculator Project**

Inside the `project/calculator` folder execute in a terminal: `gradlew runApp`  

The application starts and the screen displays something like this:  
  
	gradlew runApp
	Starting a Gradle Daemon (subsequent builds will be faster)

	> Task :runApp
	>
	<=========----> 75% EXECUTING [11s]
	> :runApp
	
Calculator project was only used on Class Assignment 1. For more information see: [Class Assignment 1](http://localhost:8081/Showcase.html)	
  
  
### **2.2. GWT Project**

Inside the `project/gwt` folder execute in a terminal: `gradlew gwtRun`

The server starts and the screen displays something like this:

	Starting a Gradle Daemon, 1 stopped Daemon could not be reused, use --status for details
	:buildSrc:compileJava UP-TO-DATE                        
	:buildSrc:compileGroovy UP-TO-DATE                                                         
	:buildSrc:processResources UP-TO-DATE
	:buildSrc:classes UP-TO-DATE
	:buildSrc:jar UP-TO-DATE
	:buildSrc:assemble UP-TO-DATE
	:buildSrc:compileTestJava UP-TO-DATE
	:buildSrc:compileTestGroovy UP-TO-DATE
	:buildSrc:processTestResources UP-TO-DATE
	:buildSrc:testClasses UP-TO-DATE
	:buildSrc:test UP-TO-DATE
	:buildSrc:check UP-TO-DATE
	:buildSrc:build UP-TO-DATE
	:compileJava UP-TO-DATE                                        
	:processResources UP-TO-DATE
	:classes UP-TO-DATE
	:gwtCompile UP-TO-DATE
	:war UP-TO-DATE      
	:gwtRun                                                       
	Listening for transport dt_socket at address: 8000
	2019-10-22 20:56:49.831:INFO::main: Logging initialized @228ms
	2019-10-22 20:56:49.838:INFO:oejr.Runner:main: Runner
	2019-10-22 20:56:50.015:INFO:oejs.Server:main: jetty-9.2.7.v20150116
	2019-10-22 20:57:18.459:INFO:oejsh.ContextHandler:main: Started o.e.j.w.WebAppContext@7e9a5fbe{/,file:/C:/Users/flavi/AppData/Local/Temp/jetty-0.0.0.0-8081-gwt-1.0.war-_-any-4245435667402377885.dir/webapp/,AVAI
	LABLE}{D:\repos\odsoft-19-20-nlm-g111\project\gwt\build\libs\gwt-1.0.war}
	2019-10-22 20:57:18.460:WARN:oejsh.RequestLogHandler:main: !RequestLog
	2019-10-22 20:57:18.530:INFO:oejs.ServerConnector:main: Started ServerConnector@76897ec{HTTP/1.1}{0.0.0.0:8081}
	2019-10-22 20:57:18.530:INFO:oejs.Server:main: Started @28933ms
	> Building 83% > :gwtRun

Now you can open Chrome and use the following url to access the application: [http://localhost:8081/Showcase.html](http://localhost:8081/Showcase.html)	

**Important:** The port for the GWT application is 8081 since the elements of the group already had Jenkins running on the port 8080.

For more information about GWT Project see: [Class Assignment 2](http://localhost:8081/Showcase.html)	

## **3. Gradle**

**Gradle** is an open source build automation system that builds on the concepts of Apache Ant and Apache Maven and introduces a Groovy-based domain specific language (DSL) instead of the XML used by Apache Maven to declare project configuration. Gradle uses a directed acyclic graph ("DAG") to determine the order in which tasks can be performed. Gradle is designed for high-growth multi-projects, and supports incremental builds when it intelligently determines which parts of the tree are up to date, so any task dependent on those parts need not be re-performed.

### ** 3.1. Using the Command Line with Gradle **

To get a list of available GWT tasks simple type **gradle tasks** in a terminal or console.

Some common commands:

*  `gradlew build`: builds the application (a **war** file is produced in **build/libs**);

*  `gradlew gwtRun`: the jetty web server is started (using port 8081) to serve the application. You can open the application in a browser with the following url <http://127.0.0.1:8081/Showcase.html>

*  `gradlew gwtDev`: gwt starts in development mode. You should be able to update the code of the application and the changes should be automatically visible. You can open the application in a browser with the following url <http://127.0.0.1:8081/Showcase.html>

*  `gradlew gwtStop`: gwt stop server.  


## **4. Continuous Integration and Continuous Delivery**
 
**Software Delivery** can be a difficult process. Long test cycles and divisions between development and operations teams are some of the contributing factors. With a view to resolution, the concept of **Continuous Software Delivery** appears.  
**Continuous Software Delivery** is an approach in which development teams develop software so that it can be sent to production at any time. Instead of making large deliveries at once, several small and fast deliveries are made, reducing the possibility of errors and contributing to higher software quality.  
Associated with Continuous Software Delivery is the **Pipeline Concept**. The main goal of a Pipeline is to automate the production software delivery process, increasing quality, stability and resilience.

The following image illustrates the main phases:

![ContinuousIntegration_ContinuousDelivery] (https://bitbucket.org/mei-isep/odsoft-19-20-nlm-g111/raw/master/images/CI_CD_Diagram.png)


## **5. Jenkins **  

**Jenkins** is a free and open source automation server. Jenkins helps to automate the non-human part of the software development process, with continuous integration and facilitating technical aspects of continuous delivery. It is a server-based system that runs in servlet containers such as Apache Tomcat. It supports version control tools, including AccuRev, CVS, Subversion, Git, Mercurial, Perforce, TD/OMS, ClearCase and RTC, and can execute Apache Ant, Apache Maven and sbt based projects as well as arbitrary shell scripts and Windows batch commands.

Builds can be triggered by various means, for example by commit in a version control system, by scheduling via a cron-like mechanism and by requesting a specific build URL. It can also be triggered after the other builds in the queue have completed. Jenkins functionality can be extended with plugins.

The Jenkins project was originally named Hudson, and was renamed after a dispute with Oracle, which had forked the project and claimed rights to the project name. The Oracle fork, Hudson, continued to be developed for a time before being donated to the Eclipse Foundation. Oracle's Hudson is no longer maintained and was announced as obsolete in February 2017.


### **5.1. Jenkins Build Promotions and Build Pipelines **  

#### **5.1.1. Promoted Build Plugin **   

This plugin allows you to distinguish good builds from bad builds by introducing the notion of 'promotion'. Put simply, a promoted build is a successful build that passed additional criteria (such as more comprehensive tests that are set up as downstream jobs.) The typical situation in which you use promotion is where you have multiple 'test' jobs hooked up as downstream jobs of a 'build' job. You'll then configure the build job so that the build gets promoted when all the test jobs passed successfully. This allows you to keep the build job run fast (so that developers get faster feedback when a build fails), and you can still distinguish builds that are good from builds that compiled but had runtime problems.

Another variation of this usage is to manually promote builds (based on instinct or something else that runs outside Jenkins.) Promoted builds will get a star in the build history view, and it can be then picked up by other teams, deployed to the staging area, etc., as those builds have passed additional quality criteria. In more complicated scenarios, one can set up multiple levels of promotions. This fits nicely in an environment where there are multiple stages of testings (for example, QA testing, acceptance testing, staging, and production.)

Information taken from the Jenkins Official Website: https://wiki.jenkins.io/display/JENKINS/Promoted+Builds+Plugin

#### **5.1.2. Build Pipeline Plugin **

This plugin provides a Build Pipeline View of upstream and downstream connected jobs that typically form a build pipeline. In addition, it offers the ability to define manual triggers for jobs that require intervention prior to execution, e.g. an approval process outside of Jenkins.

Information taken from the Jenkins Official Website: https://wiki.jenkins.io/display/JENKINS/Build+Pipeline+Plugin


#### **5.1.3. JenkinsFile**

In Jenkins, a pipeline is defined as a group of jobs that can be connected to each other in a logical sequence. In simple words, Jenkins Pipeline is a combination of plugins that support the integration and implementation of **continuous delivery pipelines** using Jenkins.

Jenkins pipelines can be defined using a text file called **JenkinsFile**. With JenkinsFile, it is possible to write the steps needed for running a Jenkins pipeline.

The benefits of using **JenkinsFile** are:

Pipelines can be automatically created for all branches and execute pull requests with just one **JenkinsFile**.
Code can be reviewed on the pipeline
Singular source for the pipeline and can be modified by multiple users.

**Declarative versus Scripted pipeline syntax:**

There are two types of syntax used for defining a JenkinsFile.

* 1.Declarative
* 2.Scripted

**Declarative:**

Declarative pipeline syntax offers an easy way to create pipelines. It contains a predefined hierarchy to create Jenkins pipelines. It gives you the ability to control all aspects of a pipeline execution in a simple, straight-forward manner.

**Scripted:**

Scripted Jenkins pipeline runs on the Jenkins master with the help of a lightweight executor. It uses very few resources to translate the pipeline into atomic commands. Both declarative and scripted syntax are different from each other and are defined totally differently.




