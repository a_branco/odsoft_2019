## **Exercise 1 - 1º Class Assignment **

This folder corresponds to developments related to 1 Class Assignment.

## **1. Tasks performed:**

### **Component 1**

**Warmup:**  

*  Implement the “Factorial” operator;  
*  Create at least one unit test for the “Factorial” operator;  
*  Generate PlantUML and Javadoc using Gradle;

**Analysis/Design: ** 

*  Write sequence diagram puml file for Add Contact;  
*  Generate sequence diagram for Add Contact using Gradle;  
*  Build the project using a freeStyle Jenkins Project;

**Development/Testing**

*  Develop “View Teacher Details” functionality;  
*  Add Unit and Integration tests; 

### **Component 2**

**Warmup:**

*  Implement the “Third” operator;  
*  Create at least one unit test for the “Third” operator;  
*  Generate PlantUML and Javadoc using Gradle;

Note: The “Third” operator divides a number by 3

**Analysis/Design: ** 

*  Write sequence diagram puml file for Delete Contact;  
*  Generate sequence diagram for Delete Contact using Gradle;  
*  Build the project using a freeStyle Jenkins Project;


**Development/Testing**

*  Develop “Update Teacher” functionality;  
*  Add Unit and Integration tests;  



### **Component 3:**

**Warmup:**

*  Implement the “Double” operator;
*  Create at least one unit test for the “Double” operator;
*  Generate PlantUML and Javadoc using Gradle;

Note: The “Double” operator multiplies a number by 2.

**Analysis/Design: ** 

*  Write sequence diagram puml file for Update Contact;
*  Generate sequence diagram for Update Contact using Gradle;
*  Build the project using a freeStyle Jenkins Project;


**Development/Testing**

*  Develop “Delete Teacher” functionality;
*  Add Unit and Integration tests;


### **Component 4:**  

**Warmup:**

*  Implement the “Exponential” operator;
*  Create at least one unit test for the “Exponential” operator;
*  Generate PlantUML and Javadoc using Gradle;

**Analysis/Design: ** 

*  Write sequence diagram puml file for View Contact;
*  Generate sequence diagram for View Contact using Gradle;
*  Build the project using a freeStyle Jenkins Project;


**Development/Testing**

*  Develop “Create Teacher” functionality;
*  Add Unit and Integration tests.

## **2. Generate Javadoc and Plant UML **  
   
  
```java 
	task renderPlantUml(type: RenderPlantUmlTask){
	}

	task movePlantumlFiles(type: Copy){
    	from 'build/puml'
    	include '**/*.png'
    	into 'build/docs/javadoc'
	}

	javadoc{
    	source = sourceSets.main.allJava
    	options.overview = "src/main/javadoc/overview.html" // relative to source root
	}
	
	javadoc.dependsOn renderPlantUml, movePlantumlFiles
	movePlantumlFiles.mustRunAfter renderPlantUml
```

## **3. Sequence Diagram**

### Component 1 - Add Contact 

![Add Contact Diagram](https://bitbucket.org/mei-isep/odsoft-19-20-nlm-g111/raw/master/odsoft/exercise1/component1/gwt/add_contact_sequence_diagram.png)


### Component 2 - Delete Contact

![Delete Contact Diagram](https://bitbucket.org/mei-isep/odsoft-19-20-nlm-g111/raw/master/odsoft/exercise1/component2/gwt/delete_contact_sequence_diagram.png)


### Component 3 - Update Contact 

![Update Contact Diagram](https://bitbucket.org/mei-isep/odsoft-19-20-nlm-g111/raw/master/odsoft/exercise1/component3/gwt/update_contact_sequence_diagram.png)

### Component 4 - View Contact 

![View Contact Diagram](https://bitbucket.org/mei-isep/odsoft-19-20-nlm-g111/raw/master/odsoft/exercise1/component4/gwt/view_contact_sequence_diagram.png)