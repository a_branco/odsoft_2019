# **Exercise 2 - 2 Class Assignment** 

## **Tasks**

* A Jenkins Pipeline using a “Scripted Jenkins File”, performing a parallel build.

	* **Repository Checkout:** checkout the GIT repository;
	* **War file:** the .war fileis built and published on Jenkins;
	* **Javadoc:** the Javadoc is built and published on Jenkins;
	* **Unit Tests:**
		* The Unit Tests are executed;
		* The Unit Tests Report is generated and published on Jenkins;
		* The Unit Tests Coverage Report is generated and published on Jenkins;
	* **Integration Tests:**
		* The Integration Tests are executed;
		* The Integration Tests Report is generated and published on Jenkins;
		* The Integration Tests Coverage Report is generated and published on Jenkins;
	* **Mutation Tests:**
		* The Mutation Tests are executed;
		* The Mutation Coverage Report is generated and published on Jenkins;
	* **System Test:**
		* The application (.war file) is deployed to a pre-configured Tomcat Server instance (Tomcat Server is running offsite)
		* An automatic smoke test is performed;
	* **UI Acceptance Manual Tests:**
		* An email notification is sent, regarding the successful execution of all the previous testsand asking to perform a manual test;
		* An UI Acceptance Manual Test is executed in order to cancel or proceed with the pipeline;
		* The pipeline must wait for a user manual confirmation on Jenkins;
	* **Continuous Integration Feedback** - a tag to the repository with the Jenkins build number and statusis published.
	
	


### **Pipeline Design**

The main objective is to develop a pipeline that satisfies the following tasks:

* **Repository Checkout;**
* **War file;**
* **Javadoc;**
* **Unit Tests;**
* **Integration Tests;**
* **Mutation Tests;**
* **System Test;**
* **UI Acceptance Manual Tests;**
* **Continuous Integration Feedback.**

The pipeline must be developed using a Jenkinsfile and performing a parallel build.

A pipeline following a sequential build means that the various pipeline tasks must be performed sequentially (in the order that makes sense). Whenever a task fails the entire pipeline must fail: a task only starts once the previous one has been successfully completed.

The control version part is dedicated to checkout the source code from Bitbucket to the Jenkins workspace. The app is then built and the WAR file is archived. The javadoc is published sequentially to this since in the first class assignment the javadoc should only be published if the WAR file was created.

In the parallel versions of the pipeline (components 2 and 4), Unit, Integration and Mutation tests are all ran in parallel since they do not depend on each other to be ran and they are all part of the same development phase - testing. Javadoc could be published in parallel as well, but since conceptually this is a completely different action from testing, it was left out. Deployment could also technically be executed simultaneously to the tests, but it was decided to execute this action only after all tests are complete so that we could deploy an application that had been fully tested.

From this point on, all stages are sequential since they all depend on each other, in all components. The smoke test directly checks the result of the system deployment so it must be executed after. The acceptance test is the last test to be executed so that the developer can fully analyze all previous tasks. Once all jobs have been successfully built or whenever a job execution fails (this last part is not representated in diagram for the sake of simplicity), a tag on the Bitbucket repository is created indicating the build status.

The Design varies from a sequential build and a parallel one.

* **Sequential Build:**

![](https://bitbucket-assetroot.s3.amazonaws.com/repository/X5Mnakj/1054106902-Sketch_Pipeline_Component1-Page-2.jpg?AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ&Expires=1573680100&Signature=PBqp5hhasx0uTY0qsilv2wxwV8c%3D)

* **Parallel Build:**

![](https://bitbucket-assetroot.s3.amazonaws.com/repository/X5Mnakj/4188113669-image.png?AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ&Expires=1573680042&Signature=BkR2qZgVuxl21WUyHjZzw7EAzYM%3D)