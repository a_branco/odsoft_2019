#!/usr/bin/env groovy
node {
	jdk = tool name: 'JDK8u161'
	env.JAVA_HOME = "${jdk}"
	
	// Variavel a ser usada no input do user
	boolean userInput = true

	try { 
	    try{
        	stage('Checkout') {
        		git branch: 'master',
        			credentialsId: '50723f0d-7a99-4680-9a39-cd31f5ff3631',
        			url: 'https://joaomagalhaes10@bitbucket.org/mei-isep/odsoft-19-20-nlm-g111.git'
        		echo 'Checkout Done'
        	}
	    }catch (e) {
    	    error("Error in Checkout")     
	    }
    	
    	stage('Build') {
    		dir ('project/gwt') {
    			echo 'Building Project'
    			
    		    // Building Project and generating javadoc
            	if (isUnix()) {	
        			sh './gradlew clean build javadoc -b build.gradle'
        		} else {
        			bat './gradlew.bat clean build javadoc -b build.gradle'
        		}
        		
    		    echo 'Build Done'
    		    
                //Publish War
    			echo 'Publish War'
    			archiveArtifacts artifacts: '**/*.war', fingerprint: true      
    			echo 'Publish War Finished'          
                
                // Publish javadoc
    			echo 'Publish Javadoc'
                step([$class : 'JavadocArchiver', javadocDir: 'build/docs/javadoc', keepAll: true])
    			echo 'Publish Javadoc Finished'
    		}
    	}
	
    	stage('Test'){
    	    echo 'Testing Project'
    		dir ('project/gwt') {
    			echo 'Running Unit/Integration/Mutation Tests'
    
                parallel 'Unit Testing':{
                    
                    //Executes Unit Tests
                    if (isUnix()) {
                        sh './gradlew cleanTest test jacocoTestReport -b ./build.gradle'
                    }else{
            	        bat 'gradlew.bat cleanTest test jacocoTestReport -b build.gradle'
                    }
                
                    //Publish Unit Test Report
                    junit '**/build/test-results/test/*.xml'
                   
                    echo 'Unit Tests Done'
                }, 'Integration Testing': {
                    
                    //Executes Integration Tests
                    if (isUnix()) {
            			sh './gradlew integrationTest jacocoIntegrationReport -b ./build.gradle'
                    }else{
            			bat 'gradlew.bat integrationTest jacocoIntegrationReport -b build.gradle'
                    }
                    
                    //Publish Integration Test Report
                    jacoco()
                    
                    echo 'Integration Tests Done'
                    
            	}, 'Mutation Testing': {
            	    
            	    if (isUnix()) {
        				sh './gradlew pitest -b ./build.gradle'
                    }else{
            			bat 'gradlew.bat pitest -b build.gradle'
            		} 
            		
            		//Publish Mutation Test Report
            		step([$class : 'PitPublisher', mutationStatsFile: '**/build/reports/pitest//mutations.xml', minimumKillRatio: 0.0, killRatioMustImprove: false])
                    
                    echo 'PitMutation Tests Done'
                }
                
                //Publish Coverage Reports
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: './build/reports/test', reportFiles: 'index.html', reportName: 'HTML Unit Test Report', reportTitles: ''])
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: './build/reports/integrationTest', reportFiles: 'index.html', reportName: 'HTML Integration Test Report', reportTitles: ''])  
    		    echo 'HTML reports Published'
    
    		}
    	}
    	
    	stage ('Deploy'){
    		echo 'Deploying Project'
        	deploy adapters: [tomcat9(credentialsId: 'd63e9d7b-3912-4bb9-83e9-3eac730128bd', path: '', url: 'http://localhost:8082/' )], contextPath: 'mywebapp', onFailure: false, war: 'project/gwt/build/libs/gwt-1.0.war'
    		echo 'Project deployed to tomcat '
        }
        
        stage('Smoke Test') {
    		echo 'Smoke Test Started'
    	    def returnCode;
    	    
    	    if (isUnix()) {
    	       returnCode = sh script: 'curl -f http://localhost:8082/mywebapp', returnStdout: true
    	    }else{
    	       returnCode = bat script: 'curl -f http://localhost:8082/mywebapp', returnStdout: true
    	    }
    
    		//if response has 404, Smoke Test failed
    		if (returnCode.contains("404")) {
    			currentBuild.result = 'FAILURE'
    			error("Smoke Test Failed");
    		} else {
    			echo 'Smoke Test Succeded!'
    		}
        	echo 'Smoke Test Done'
    	}
    	
    	stage('User Validation') {
    		echo 'User Validation Started'
    		// Enviar e-mail para confirmação
    
    	    emailext subject: '$DEFAULT_SUBJECT',
                        body: '$DEFAULT_CONTENT',
                        recipientProviders: [
                            [$class: 'CulpritsRecipientProvider'],
                            [$class: 'DevelopersRecipientProvider'],
                            [$class: 'RequesterRecipientProvider']
                        ], 
                        replyTo: '$DEFAULT_REPLYTO',
                        to: '$DEFAULT_RECIPIENTS'
                            
    		try {//this try catch fails if the pit mutations test is run.
        		// Espera pelo input de um utilizador para poder continuar a execução (espera 1 hora)
        		timeout(activity: true, time: 1, unit: 'HOURS') {
        			userInput = input(
        				id: 'Proceed1',
        				message: 'Proceed with build?',
        				parameters: [
        					[$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: 'Please confirm']
        				]
        			)
        		}
    		} catch (e) { // timeout reached or input false
                	userInput = false
                	echo "Build Aborted"
    		}
    		
    		echo 'User Validation Finished'
    	} 
    	
    }catch (e) {
		// Corre apenas se houver algum erro em algum dos stages
		if (userInput == false) {
			currentBuild.result = 'ABORTED'
		} else {
			currentBuild.result = 'FAILURE'
		}
		throw e
	} 
    finally {
    	stage('Git Push'){
    	    
    	    echo 'Git Push Started'
    	    
    	    if (isUnix()) {
    			sh "git config user.name joaomagalhaes10"
    			sh "git config user.email 1160763@isep.ipp.pt" 
    		} else {
    			bat "git config user.name joaomagalhaes10"
    			bat "git config user.email 1160763@isep.ipp.pt" 
    		}
		    
		    echo 'Configuration Finished'
		    
		    withCredentials([usernamePassword(credentialsId: '50723f0d-7a99-4680-9a39-cd31f5ff3631',
    		passwordVariable: 'GIT_PASSWORD',
    		usernameVariable: 'GIT_USERNAME')]) {
    			if (isUnix()) {
    				sh "git tag -a Build#${env.BUILD_NUMBER}-${currentBuild.currentResult} -m \"Tagged autommatically by Jenkins\""
    				sh "git push https://joaomagalhaes10:${GIT_PASSWORD}@bitbucket.org/mei-isep/odsoft-19-20-nlm-g111.git --tags"
    			} else {
    				bat "git tag -a Build#${env.BUILD_NUMBER}-${currentBuild.currentResult} -m \"Tagged autommatically by Jenkins\""
    				bat "git push https://joaomagalhaes10:${GIT_PASSWORD}@bitbucket.org/mei-isep/odsoft-19-20-nlm-g111.git --tags"
    			}
    		}
    		
    		echo 'Git Push Finished'
    	}
    }
}