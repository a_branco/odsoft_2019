# Project Assignment - CMS (Web Application College Management System)
-----------------------

This folder contains all files and implementations related to the project.

# 1. Persistance Layer
-----------------------
As already mentioned, for implementation of the persistence layer we had to choose a **Database Management System (SGBD)**. The only requirement placed on us at this point was that it had to be a **relational database**.
We could have chosen **SQL Server**, **Oracle**, **Postgres**, **DB2**, among others, however each has its advantages and disadvantages.  
**MySQL** in addition to meeting all our needs in this project we had worked with it in the past and so we chose it.

## 1.1. JDBC (Java Database Connectivity)

Since we chose to use **MySQL** and to be able to connect this to the Java project we had to resort to using **JDBC**.  
**Java Database Connectivity (JDBC)** is a set of Java-written classes and interfaces (APIs) that send SQL statements to any relational database.
To connect the Java project with MySQL it was necessary to integrate a dependency on the project called [MySQL Connector](https://dev.mysql.com/downloads/connector/j/).

This code snippet is responsible for establishing the connection to the database:  
 _  
```java
  
public Connection connectDatabase() {
	Connection conn = null;

    try {
    	conn = DriverManager.getConnection("jdbc:mysql://localhost/cms_odsoft", "root", ""); // Address, user and password
		System.out.println("Successful connection established!");

	} catch (SQLException ex) {
    	// Handle any errors
		System.out.println("SQLException: " + ex.getMessage());
		System.out.println("SQLState: " + ex.getSQLState());
		System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
    }
	
    return conn;
}
```

## 1.2. Entities and Relationships Diagram (ERD)

To create a good database, it is necessary that the project goes through several phases, one of the most important being the creation of models that allow us to structure and organize the database - **Entities and Relationships Diagram (ERD)**.  
The initial **ERD** is composed of **entities and attributes**, and their **relationships**. Relationships can be of various types, namely: **"one" to "many"**, **"one" to "one"**, **"many to many"**.  
After the entities and their links, some changes are necessary, namely the **unfolding of the relations from "many" to "many"**. Relationships of this kind have to be separated by a new associative entity that links the two original entities with a "one" to "many" relationship.  
If there are no "many" to "many" relationships then this step is not performed. If it had to be done then it would give rise to a new ERD.

For the implementation of a persistence layer in the project a relational database was developed, which allows to store the information of **Students**, **Teachers**, **Classes** and **Subjects**.  

## 1.3. Table Schema

After the **ERD models** made it is necessary to turn it into a **table schema**, for this, **it is necessary to know that each entity will correspond to a table, and that the attributes correspond to the fields of the table**. Relationships are made using **Primary Key (PK)** and **Foreign Key (FK)** creation.  
A **Primary Key** is one or more fields in the table that make it possible to unambiguously identify a record, so it is unique and mandatory. When in a table, neither field can be a primary key, you must create a field that identifies the record (for example: idStudent).  
A **Foreign Key**  is a common field between two tables, with the same name and data type, that allows us to create the relationship between the tables.  
In the case of “one” to “many” relations, the foreign key must be placed in the table that receives the “many” side so that they can be related to each other.  
When there are relations of "one" to "one" the primary key is the same in both tables.  
  
When moving from a ERD to a Table Scheme you have to pay attention to **database optimization**.
For a database to be optimized it must be normalized. **Normalization is a process that simplifies and organizes the structure of a database so that it is in its optimal state**.
A database is normalized if it is in **Normal Forms (NF)**.  

### 1.3.1. 1st Normal Form
The goal of the first normal form is to **eliminate repetition of information groups**. In this normal form, there are 2 steps to consider: **eliminating multivalue attributes**, ie attributes that are composed, and **eliminating repetitive groups**.

### 1.3.2. 2nd Normal Form
For a table schema to be in the 2nd Normal Form, it must be in the 1st NF.  
The main purpose of this NF is to **eliminate redundancy** and **for this all non-key attributes completely depend on the entire primary key**.  However, this normal form is only applicable to entities whose primary key is composed, that is, contains more than one attribute.

### 1.3.3. 3rd Normal Form
The main purpose of the 3rd Normal Form is as in 2nd NF, to **eliminate redundancy**. For the table schema to be in the 3rd Normal Form, it must be in the 2nd, and it is necessary: **that all non-key attributes functionally depend on each other, or no non-key attributes depend on the transitivity of the primary key**.

In this project the database was normalized to the **3rd Normal Form**.

![Database_ERD](https://bitbucket.org/mei-isep/odsoft-19-20-nlm-g111/raw/master/images/Database_DER.jpg)


### 1.4. Setup Database without Docker Image

Although the goal of this project is for the database to run on a docker image, at an early stage and even while developing project features, we chose to run the database locally and outside the container.  
To do this, here are some important steps to get the database setup:

#### 1.4.1. Install local server (xampp)
To connect to the database you need to install a local server with MySQL support. We chose to choose xampp as it was already known and there is extensive documentation on the internet.  
[Download xampp](https://www.apachefriends.org/download.html)

After installation is complete, you must start the apache and mysql services.
Now enter in browser http://localhost/phpmyadmin/
The database maintenance page will be displayed. All operations can be performed here.
However, for easier and more intuitive manipulation we also use the MySQL Workbench which allows us to manipulate the database without using the browser.  
[Download MySQL Workbench](https://dev.mysql.com/downloads/workbench/) 

#### 1.4.2. Import Database
To import an existing database, follow these steps:

**1º Step)** [Download SQL file](https://bitbucket.org/mei-isep/odsoft-19-20-nlm-g111/src/master/project/gwt/containers/database/database.sql);   
**2º Step)** After the file has been downloaded and xampp is running, open the database maintenance page in your browser ([http://localhost/phpmyadmin/](http://localhost/phpmyadmin/));  
**3º Step)** In the top menu click on "Import";  
**4º Step)** Choose the SQL file that was previously downloaded.  

**Note: In section 4.1.2 it addressed the issue of containerization database.**  

# 2. Documentation 
-----------------------

## 2.1. Generate .zip file
To create the **zip file** with the requested files, it was necessary to create a new task in the `build.gradle` file. This task creates a file called **ODSOFT_CMS1920_G111.zip** in the build folder with the artifacts (war file, report.pdf and test reports).

```
task zip(type: Zip) {
	archiveName = "ODSOFT_CMS1920_G111.zip"
	destinationDir = file("build")

	from "Jenkinsfile";
	//from "report.pdf";
	from('build/libs/') {
		include '*.war'
	}
	from('build/reports/') {
		include 'checkstyle/*'
		include 'findbugs/*'
		include 'integrationTest/*'
		include 'test/*'
	}
}
```

## 2.2. Generate Report.pdf

It was also a project requirement to generate a **Report.pdf** based on the readme.md of the project. For this task we use **MarkdownJ** [MarkdownJ](https://gist.github.com/Arakade/932bd8c9d3b359fe30a3?fbclid=IwAR0tGe9Fq0Dm-04NFQsPwVdfdba-1VJTdGe-krt9bAKjZf49RR7ydatD-9E) technology.
In this repository in the project/gwt/folder there is a gradle file (`md2pdf.gradle`) responsible for generating the report.


# 3. CMS Features (Teachers, Students, Classes and Subjects)
-----------------------

One of the requirements proposed in the course of the project development of this discipline was the implementation of some features, namely the implementation of the concept **Teachers, **Students**, **Classes** and **Subjects**. Students can only be enrolled in a single class, each class can have no more than twenty students and only one teacher for each subject can be assigned to a class.   
For both students, classes and subjects, a class has been created and their methods added.
Implementation of these features also required the implementation of a **persistence layer** (Point 1).  

Overall, both **Students**, **Teachers**, **Subjects**, and **Classes** needed 3 main methods that were responsible for the basic **Add**, **Edit**, and **Delete** operations.  
**Note: Since the methods are very similar to each other, we will only explain to the Students case.**  

## 3.1. Add

Some features for adding a student were already in place, such as filling out the form, adding all students to HashMap, however this information was not stored persistently. Since there is now a persistence layer this data should be stored there.
An auxiliary java file (`DatabaseConnection.java`) has been created that contains all methods that use database connection. In this case for adding a student, the following method was created:  

```java
public void addStudent(String firstName, String lastName, String gender, Date birthday, String email, String classe) {
    Connection conn = connectDatabase(); // Connect to database

    int idClasse = getIdClass(classe);
		
	try {
	   String query = "INSERT INTO student (firstName, lastName, gender, birthDate, email, class_idClass) values (?, ?, ?, ?, ?, ?)";

	   PreparedStatement preparedStmt = conn.prepareStatement(query);

	   preparedStmt.setString(1, firstName);
	   preparedStmt.setString(2, lastName);
	   preparedStmt.setString(3, gender);

	   SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	   String strDate = formatter.format(birthday);
	   preparedStmt.setString(4, strDate);
			
	   preparedStmt.setString(5, email);
	   preparedStmt.setInt(6, idClasse);
       preparedStmt.execute();

	} catch (SQLException ex) {
	   // Handle any errors
	   System.out.println("SQLException: " + ex.getMessage());
       System.out.println("SQLState: " + ex.getSQLState());
	   System.out.println("VendorError: " + ex.getErrorCode());
	} finally {

	}
}
```

First the database connection is established (`connectDatabase()` - method explained in the previous point) and then it is necessary to construct the query to insert the data into the `student` table.  
**Note: In all developed methods we always chose to use Prepared Statments queries.**  

Since the `student` table has a Foreign Key (FK) for the `idClass` field of the `class` table, it is necessary to associate the class with its ID. To do this we implemented a `getIdClass` helper method, which takes the `class` as a parameter and returns its ID.  
The `getIdClass()` method is also a database query, but in this case it is a **SELECT query**: `"SELECT idClass FROM class WHERE designation LIKE ?"`

## 3.2. Update
  
In the case of the Update operation, the logic implemented is quite similar to adding, except in this case we want to edit a database record based on its ID.
For this we used a query type **UPDATE**, we send the new values to be entered and the value of your **Primary Key (PK)**. Sending the Primary Key value in the query's WHERE clause ensures that we are only editing the table record whose ID is the same as the one selected for editing.

```java
public void updateStudent(int idStudent, String firstName, String lastName, String gender, Date birthday, String email, String classe) {
	Connection conn = connectDatabase(); // Connect to database

	try {
		String query = "UPDATE student set firstName = ?, lastName = ?, gender = ?, birthDate = ?, email = ?, class_idClass = ? where idStudent = ?";
			
		PreparedStatement preparedStmt = conn.prepareStatement(query);
			
		preparedStmt.setString(1, firstName);
		preparedStmt.setString(2, lastName);
		preparedStmt.setString(3, gender);
			
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(birthday);

		preparedStmt.setString(4, strDate);
		preparedStmt.setString(5, email);

		int idClass = getIdClass(classe);
		preparedStmt.setInt(6, idClass);
		
		preparedStmt.setInt(7, idStudent);
			
		preparedStmt.executeUpdate();

    } catch (SQLException ex) {
		// Handle any errors
		System.out.println("SQLException: " + ex.getMessage());
		System.out.println("SQLState: " + ex.getSQLState());
		System.out.println("VendorError: " + ex.getErrorCode());
	} finally {

    }
}

```
  
## 3.3. Delete

To delete a student we implemented a method that takes as a parameter the ID of the student to be deleted. Once the database connection is established, a **DELETE** query is made that deletes the record whose idStudent is the one received as a function parameter.  

```java
public void deleteStudent(String id) {

		int idStudent = Integer.parseInt(id); // Convert string id to integer

		Connection conn = connectDatabase(); // Connect to database

		try {
			// MySQL Delete Statement
			String query = "DELETE FROM student where idStudent = ?";
			PreparedStatement preparedStmt = conn.prepareStatement(query);

			preparedStmt.setInt(1, idStudent);

			// Execute the PreparedStatement
			preparedStmt.execute();

		} catch (SQLException ex) {
			// Handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} finally {

		}
	}

```
# 4. Base Pipeline
-----------------------

![Base Pipeline Design](https://bitbucket.org/mei-isep/odsoft-19-20-nlm-g111/raw/master/images/PipelineDesign.jpg)

Our pipeline consists of **6 main stages**: (1) **Checkout**, (2) **Build**, (3) **Code Quality and Tests**, (4) **Smoke Tests**, (5) **User Validation** and (6) **Deployment**.  
The first stage - **Checkout**, as its name implies, is to **checkout the repository** where all project developments are located.  
The second stage is called **Build**, because this is where the project build is done. First the **Application Build** is done, then **javadoc and war file** are generated and published. Finally, the **database is built** and **Report.pdf is generated** based on the repository readme.  As was the project requirement the database is executed inside a docker container.
Since the application build is independent of the database build these two steps can be performed in **parallel**.  Also javadoc and war file generation are independent and so these 4 steps are executed in parallel pipeline.  
The **Code Quality and Tests** stage is also composed of several tasks that **run in parallel** as they do not depend on each other (See section 5).

# 5. Virtualization - Virtual Machine vs. Container
-----------------------

Resource virtualization has already proven to be a trend that is definitely here to stay. Having a disk or virtual environment can be a cheaper, more flexible, democratic and scalable option for companies, so many of them are interested in betting on this technology.
Many programmers know how long the virtualization process often takes, making software development and maintenance inefficient. However, with the advancement of technology, a new concept of virtualization emerged, called containers.

## 5.1. Virtual Machines (VM's)

Having a **Virtual Machine** means that a virtual hard disk is created within a physical environment through specific software. Having hardware inside another makes it possible to run operating systems and all kinds of programs.  
This is one of the great advantages of having a Virtual Machine, as you can use the applications and programs you need without depending on the operating system you have on the physical disk. Because you can create a different virtual environment within one that already exists, you are free to use all the programs you need on the same machine.  
  
  
For example, if you need to use software that only runs on system X and one that is made for operating system Y, the Virtual Machine will allow both machines to work simultaneously in the same environment. If your PC has system X, simply create a VM with system Y, or the other way around.  
  
  
Another benefit of this model is that resource isolation allows the Virtual Machine to "fool" software, which does not detect that it is active because it is not running on a physical system. Since there is no interaction between physical and virtual performance, this dynamic allows the original resources, which do not belong to the virtual machine, to be spared. In other words, what happens in the Virtual Machine stays in the Virtual Machine.  
Without having to partition hard drives to run multiple systems, the physical hard drive remains intact and you can test and create in the virtual environment without worrying about running out of resources on your device.  
When you need to move your Virtual Machine between devices or want to back up, the process is also simple and fast, which saves staff work and speeds up work.


## 5.2. Containers


**Containers** are types of environments in which applications and their elements can be grouped together to improve the work of IT staff who can work focused on an application-specific environment, as well as the functioning of software and the server, as the Resources used by each Container are fully isolated.  
  
What unites the Container model with the Virtual Machine is mainly that both use virtualization. What changes here is that virtualization is at the operating system level and Containers do not use hypervisor like Virtual Machines, but system resources and kernel processes to create the environments. This is what prevents the Container from having an overview of the physical environment outside its space, as the Virtual Machine has.  
  
In practice, Container promotes hardware communication to the operating system and directly to containers, which take care of isolation and application startup. In the Virtual Machine the hardware communicates with the hypervisor, and then with the machine. Already inside the VM will boot the operating system and only then the applications will be activated.
This means that both Container and VM work with environment virtualization and isolation to drive application-independent processing, but Containers create isolated environments where different applications can run simultaneously because the division is done at the level of available resources such as memory and processing. The Virtual Machine, on the other hand, allows a physical machine to house others with different operating systems, hard disks and hardware independent of the originals.  
  
One of the main advantages of Container is the ability to create independent services and codes that can be moved seamlessly between different machines and environments without data loss.  
When considering security, the Container is slightly less robust than the Virtual Machine, which still provides more user protection, but, on the other hand, tends to be faster since only applications need to be started, not the entire operating system.  
  
  
One thing that can promote wide availability of your machines is to use both methods. You can create Containers within your Virtual Machine by extending resource management and leveraging results without necessarily spending more on it.


The following image represents the main differences between conventional virtualization (using virtual machines) and virtualization using containers.

![Virtualization](https://bitbucket.org/mei-isep/odsoft-19-20-nlm-g111/raw/master/images/Containers_VMs.jpg)


### 5.1.1. Docker 

In short, Docker is an open source platform, developed in the Go language and created by Google itself. Because of its high performance, the software ensures easier creation and administration of isolated environments, ensuring the rapid availability of programs to the end user.

Docker aims to create, test and deploy applications in a separate environment from the original machine, called a container. This way, the developer can package the software in a standardized way. This is because the platform provides basic functions for its execution, such as: code, libraries, runtime and system tools.

**Advantages:  **

* How quickly software can be made available - up to 7 times faster than conventional virtualization;  
* Possibility of configuring different environments quickly, as well as reducing the number of incompatibilities between available systems;  
* Modularity;  
* Layers and version control of images;  
* Reversal;  
* Rapid implementation.  

### 5.1.2. Container Database

As previously mentioned, one of the project requirements was the creation of a **docker image to run the database.**  
For this and since our database is implemented in MySQL and being widely used and known MySQL there is already a docker image already published in [**Docker Hub**](https://hub.docker.com/) (`docker pull mysql`). However there are some changes that we need to make for our container to work properly, so we have created a **Dockerfile**.  
A **Dockerfile** is a text file that describes the steps that Docker needs to prepare an image, including package installation, directory creation, and setting environment variables, among other things.  

This is the Dockerfile created for creating the database container.

```
FROM mysql:latest

# Environment variables
ENV MYSQL_DATABASE=cms_odsoft
ENV MYSQL_USER=cms_user
ENV MYSQL_PASSWORD=password
ENV MYSQL_ROOT_PASSWORD=root

VOLUME /etc/mysql

ADD database.sql /etc/mysql

RUN sed -i 's/cms_odsoft/cms_odsoft/g' /etc/mysql/database.sql
RUN cp /etc/mysql/database.sql /docker-entrypoint-initdb.d

EXPOSE 3306

```

The first line (`FROM mysql:latest`) shows the indication that this container image is based on the `mysql image`. Subsequently, some environment variables are defined that are used for database creation and access.  
Whenever a database container is launched we want to keep the existing data, so a .sql file (`database.sql`) is added which is a backup of the database. Only then can we ensure that the data in this .sql file is imported as soon as the container is running.

### 5.1.3. Container Application 

**The application should work in a separate container from the database**. As mentioned above, a Dockerfile is also created that will serve as the base for the container. In this case the Docker image is based on **tomcat**. The application war file is copied to the Tomcat folder and the container is created.

```
FROM tomcat:9.0

MAINTAINER test

WORKDIR /usr/local/tomcat/webapps

COPY ./build/libs/*.war /usr/local/tomcat/webapps

```

### 5.1.4. Useful commands

```  

docker ps // Shows the containers that are running  
docker ps -a // Shows all containers and their state  
docker build -t database . // Create a docker image called database
docker run -p 3306:3306 --name cont_database database // Create a container called cont_database using the docker image database
docker rmi $docker_id // Remove container with id $docker_id


```

# 6. Code Quality
-----------------------

**Writing good code is extremely important** because over time badly structured code can lead to excessive complexity in new module development and maintenance, creating a project where developers don't want to work or are afraid to change something and make functionality unrelated stop working.  
In our pipeline we use two plugins that help in structuring and writing good code: **Checkstyle Plugin** and **Findbugs Plugin**.  

## 6.1. Checkstyle Plugin

The **Checkstyle Plugin** performs quality checks on your project’s Java source files using Checkstyle and generates reports from these checks.  

**Checkstyle plugin — project layout**

By default, the **Checkstyle Plugin** expects the following project layout, but this can be changed:  
`config/checkstyle/checkstyle.xml`- Checkstyle configuration file

The configuration used is common file found on GitHub but with some alterations in order to make the scan a little bit less strict..

**Enabling Checkstyle plugin**

In order to enable the **Checkstyle Plugin**, the `build.gradle` file needed to be changed. The following lines were added:

```
apply plugin: 'checkstyle'

checkstyle { 
  toolVersion '7.8.1' 
  configFile file("config/checkstyle/checkstyle.xml")
}
checkstyleMain {
  ignoreFailures = true 
  source ='src/main/java'
}
checkstyleTest {
  ignoreFailures = true 
  source ='src/test/java'
}
```

`toolVersion`— The version of the code quality tool to be used;  
`configFile` — The Checkstyle configuration file to use. The path to the configuration file needs to be defined. For this, the file phrase is needed, in order to define the path to file;  
`ignoreFailures` — Allows the build to not fail when it finds warnings.

**The Checkstyle plugin adds the following tasks to the project:**  

`checkstyleMain`: Runs Checkstyle against the production Java source files;  
`checkstyleTest`: Runs Checkstyle against the test Java source files.


## 6.2. Findbugs Plugin

The **FindBugs Plugin** performs quality checks on your project’s Java source files using FindBugs and generates reports from these checks.  

**Findbugs plugin — project layout**

By default, the **Findbugs Plugin** expects the following project layout, but this can be changed:  
`config/findbugs` — Other FindBugs configuration files  

The configuration used is a file named `excludeFilter.xml` that exclude some bugs that are not necessary.

**Enabling FindBugs plugin**

In order to enable the **Findbugs Plugin**, the `build.gradle` file needed to be changed:

```
apply plugin: 'findbugs'

findbugs {
    ignoreFailures = false
    toolVersion = "3.0.1"
    sourceSets=[sourceSets.main]
    excludeFilter = file("config/findbugs/excludeFilter.xml")
    reportsDir = file("$project.buildDir/reports/findbugs")
    effort = "max"
}

tasks.withType(FindBugs) {
    reports {
        xml.enabled true
        html.enabled false
    }
}

```

`ignoreFailures` — Whether to allow the build to continue if there are warnings;  
`toolVersion` — The version of the code quality tool to be used;  
`sourceSets` — The source sets to be analyzed as part of the check and build tasks;  
`excludeFilter` — The filename of a filter specifying bugs to exclude from being reported. For this, the file phrase is needed, in order to define the path to file;  
`reportDir` — The directory where reports will be generated;  
`effort` — The analysis effort level. The value specified should be one of min, default, or max. Higher levels increase precision and find more bugs at the expense of running time and memory consumption.  

**The FindBugs plugin adds the following tasks to the project:**

`findbugsMain`: Runs FindBugs against the production Java source files;  
`findbugsTest`: Runs FindBugs against the test Java source files.


In order to use the plugin in the jenkinsfile script, the `findbugsMain`, `findbugsTest`, `checkstyleMain` and the `checkstyleTest` task needs to be executed. Also in order to ensure that the script is executed either on Windows or Linux operating systems.

To do this the jenkinsfile contains the following lines.

```
if (isUnix()) {

  sh './gradlew findbugsMain -b build.gradle'
  sh './gradlew findbugsTest -b build.gradle'
  sh './gradlew checkstyleMain -b build.gradle'
  sh './gradlew checkstyleTest -b build.gradle'
  
} else {

  bat './gradlew.bat findbugsMain -b build.gradle'
  bat './gradlew.bat findbugsTest -b build.gradle'
  bat './gradlew.bat checkstyleMain -b build.gradle'
  bat './gradlew.bat checkstyleTest -b build.gradle'
  
}
```

**Quality gate configuration**

Several quality gates can be defined that will be checked after the issues have been reported. These quality gates allow to modify a Jenkins' build status so that the user immediately sees if the desired quality of your product is met. A build can be set to unstable or failed for each of these quality gates. All quality gates use a simple metric: the number of issues that will fail a given quality gate.

For this project the tools Findbugs and Checktyle were used. For this the next two lines were added to the script:

```
recordIssues qualityGates: [[threshold: 350, type: 'TOTAL', unstable: true]], 
   tools: [checkStyle(pattern: '**/reports/checkstyle/*.xml')]

recordIssues qualityGates: [[threshold: 60, type: 'TOTAL', unstable: true]], 
   tools: [spotBugs(pattern: '**/reports/findbugs/*.xml', useRankAsPriority: true)]
```

The threshold defined for the checktyle tool is 350 warnings. Inicially the tool identified around 1000.

The threshold defined for the checktyle tool is 60 warnings. Most of these come from the class that establishes the connection to the database.

If these values are passed the build changes its state to **UNSTABLE**.



# 7. Integration Tests
-----------------------

The Integration Tests will use **JaCoCo** to get the test results and the coverage reports will also be published through publishHTML. In order to ensure that the script is executed either on Windows or Linux operating systems.
To do this the jenkinsfile contains the following lines.
  
  
```
if (isUnix()) {
  sh './gradlew integrationTest jacocoIntegrationReport -b ./build.gradle'
}else{
  bat 'gradlew.bat integrationTest jacocoIntegrationReport -b build.gradle'
}
```

The jacoco and publishHTML plugin are used in order to publish the Testing and Coverage Reports. For this project, a thresholds was stablished to check the build health. The builds fails if the coverage degrades more than the delta threshold stablished. For this job the next lines of code were added to the jenkinsfile:
  
  
```
jacoco buildOverBuild: true, changeBuildStatus: true, classPattern: '**/classes/**/server', deltaBranchCoverage: '60', deltaClassCoverage: '60',
  deltaComplexityCoverage: '60', deltaInstructionCoverage: '60', deltaLineCoverage: '60', deltaMethodCoverage: '60', maximumInstructionCoverage: '80',
  minimumInstructionCoverage: '06'

publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: './build/reports/integrationTest',
  reportFiles: 'index.html', reportName: 'HTML Integration Test Report', reportTitles: ''])
```

The value of the threshold for the coverage of the Integration Tests is 60%, so the build fails if the value is lower than 60. The threshold for a healthy build is 80%.


# 8. Functional Testing
-----------------------

In order to develop the Functional Testing part of the project, Cucumber and Selenium will be used. So, it is necessary to create the test scenary and for each part that is being tested, a ".feature" file should be created. The test file will be written in a language that a user with no previous techical knowledge understands.

After defining the scenary, it is necessary to configure the Browser WebDriver where the tests will be executed, so the DriverUtil.java file should be changed acordingly. Then the ".feature" files should start being developed and they will include the scecific tests developed. In order to distinguish elements on the browser to test the features, checking the element-id or class would be the best option. But due to the fact that the element-id was not available and in some cases there was the same class for a lot of different elements, xpath had to be used.

For the reports to be published on Jenkins, it is necessary to use the Publish HTML plugin, and the file used would be index.html, located on the build/reports/tests/acceptance folder.

For the application being executed on a Docker container, it is mandatory to configure a container using a tomcat image, for that a Dockerfile is developed and it is possible to send both the application and the database, as it is required.
After creating the Docker Image, it is still possible to publish it on Docker Hub, by pushing it.

# 9. Smoke Test
-----------------------

The last objetive in the Functional and Smoke test part is to execute a Smoke Test on a Docker Container. In order to fulfill this requirement, the .war file of the application, using a tomcat image, should be sent to the container. When it is running correctly, it must have a exposed port, so it is possible to connect into the container. In order to prove that the connection is proper the smoke test is applied, using a CURL command whose success is directly dependent on the connection. 




# 10. Deployment
-----------------------
To simulate live deployment to production, a final docker container was created. This was the last stage of the pipeline, designed to finalize the build of the project when every other stage passes and change the tag of the git repository.

## 10.1. Pulling the containers

At this stage of the pipeline, both the application and the database containers have already been created and pushed to the Docker Hub, so during this phase they are simply pulled from the hub.
Before the database is pulled, the pipelines first checks if the current version is the newest, using the following code in the project's jenkinsfile:

```groovy
if (isUnix()) {	
	string output = sh(returnStdout: true, script: 'docker images 1182161/database:${BUILD_TAG} | wc -l').trim()
	numLines = output.reverse().take(1)
	numLinesInt = numLines.toInteger()

	if(numLinesInt > 1){
		echo 'You have the newest database version!'
	} else {
		echo 'Your database is outdated!'
		sh 'docker pull 1182161/database:${BUILD_TAG}'
	}				

	sh 'docker build -t database .'
}
```

This code is wrapped in a isUnix() conditional statement in order to verify the operating system it is being ran on.
When on Windows, the comands used in the pipeline are slightly different, but since the core code is the same and for the sake of simplicity, that code will not be shown here.  

During this stage it is first checked how many images of the current build (represented by the BUILD_TAG) are present in the production machine.
If there is at least one (meaning the docker images command will produce at least 2 lines of output, counting the header), the database in the machine contains the newest data. If not, the new database version is pulled from the hub.
Lastly, the image is built in the production machine to be used in the next session.  

The application's container is also pulled from the hub, as just like with the database's container, the image is built. The docker-compose up command initiates the next step of the deployment.

```groovy
sh 'docker pull 1182161/cms:${BUILD_TAG}'
sh 'docker build -t cms .'
					
sh 'docker-compose up -d'
```

## 10.2. Production Container

Once both container images have been built, the docker-compose up command is executed. The -d tag allows it to run in the background so that the pipeline does not stay busy.
This command uses the content of the docker-compose.yml file, which is represented in the snippet below:

```
services:

  db-production:
    image: database
    ports:
      - "3308:3306"
  cms-production:
    image: cms
    ports:
      - "8081:8080"
```

This file creates containers using the database and cms images built in the previous steps and runs them.
For example, the service db-production creates a container equivalent to the one created by the 'docker run -p 3308:3306 -d database' instruction.
This allows both containers to be running and interacting with each other, available on the ports specificed.


## 10.3. Git Tag

Each build must be accompanied by a tag in the git repository that indicates its result - success or failure.
Since any stage of the pipeline can change the build's status, this result must be checked during the execution of the whole pipeline.
For this, every stage is encased in a try-catch statement, as shown in the following snippet:

```
try{
	//stage code
}catch (e) {
	currentBuild.result = 'FAILURE'
	throw e
} 
```

By default the currentBuild.result variable is set to SUCCESS, but if a stage fails the 'try' statement, the build is set to FAILURE.
This variable is later using during the very last stage of the pipeline where the tag is committed to the git repository.

```
sh "git tag -a Build#${env.BUILD_NUMBER}-${currentBuild.currentResult} -m \"Tagged autommatically by Jenkins\""
sh "git push https://a_branco@bitbucket.org/mei-isep/odsoft-19-20-nlm-g111.git --tags"
```


