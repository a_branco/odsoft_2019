package com.twu.calculator;

public class Calculator {

    private double accumulator;

    public double doOperation(String operation, double operand) {
        switch (operation) {
            case "add":
                accumulator += operand;
                break;
            case "subtract":
                accumulator -= operand;
                break;
            case "multiply":
                accumulator *= operand;
                break;
            case "divide":
                accumulator /= operand;
                break;
            case "abs":
                accumulator = Math.abs(accumulator);
                break;
            case "neg":
                accumulator = -accumulator;
                break;
            case "sqrt":
                accumulator = Math.sqrt(accumulator);
                break;
            case "sqr":
                accumulator = Math.pow(accumulator, 2);
                break;
            case "cube":
                accumulator = Math.pow(accumulator, 3);
                break;
            case "cubert":
                accumulator = Math.cbrt(accumulator);
                break;
            case "cancel":
                accumulator = 0;
                break;
            case "factorial":
                accumulator = computeFactorial(accumulator);
                break;
            case "factorials":
                accumulator = computeFactorials(accumulator, 1.0);
                break;
            case "third":
                accumulator = accumulator/3;
                break;
            case "double":
                accumulator = accumulator + accumulator;
                break;
            case "exponential":
                accumulator = Math.pow(accumulator, operand);
                break;
            case "exit":
                System.exit(0);
        }
        return accumulator;
    }

    private double computeFactorial(double n) {
        if (n <= 1.0) {
            return 1.0;
        }
        return n * computeFactorial(n - 1.0);
    }

    private double computeFactorials(double n, double t) {
        if (n <= 1.0) {
            return 1.0;
        }
        for(int i = 1 ; i <= n ; i++) {
            t = t * i;
        }
        return t;
    }
}
