package pt.isep.cms.classes.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface AddClassEventHandler extends EventHandler {
  void onAddClass(AddClassEvent event);
}
