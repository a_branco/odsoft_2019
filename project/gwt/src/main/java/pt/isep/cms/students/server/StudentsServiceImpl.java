package pt.isep.cms.students.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import pt.isep.cms.Database.DatabaseConnection;
import pt.isep.cms.students.client.StudentsService;
import pt.isep.cms.students.shared.Student;
import pt.isep.cms.students.shared.StudentDetails;

@SuppressWarnings("serial")
public class StudentsServiceImpl extends RemoteServiceServlet implements StudentsService {

  DatabaseConnection _DatabaseConnection = new DatabaseConnection();

  private HashMap<String, Student> students = new HashMap<String, Student>();

  public StudentsServiceImpl() {
    initStudents();
  }

  private void initStudents() {
    students = _DatabaseConnection.getStudents();
  }

  /**
   * Adds student to database.
   *
   * @param student student.
   * @return returns the added student.
   */
  public Student addStudent(Student student) {

    String firstName;
    firstName = student.getFirstName();

    String lastName;
    lastName = student.getLastName();

    String gender;
    gender = student.getGender();

    Date birthday;
    birthday = student.getBirthday();

    String email;
    email = student.getEmail();

    String classe;
    classe = student.getIdClasse();

    // Add student to database. The ID of student is auto increment
    _DatabaseConnection.addStudent(firstName, lastName, gender, birthday, email, classe);

    // After adding the student to the database we need to know what ID has been
    // assigned to add to the hash map
    String id = _DatabaseConnection.lastStudentId();

    student.setId(id);
    students.put(student.getId(), student);

    return student;
  }

  public HashMap<String, String> getClassesDropDown() {
    HashMap<String, String> classes = _DatabaseConnection.getClassesDropDown();
    return classes;
  }

  /**
   * updates student.
   *
   * @param student student to update.
   * @return returns updated student.
   */
  public Student updateStudent(Student student) {

    String lid = student.getId();
    int idStudent = Integer.parseInt(lid);

    students.remove(student.getId());
    students.put(student.getId(), student);

    _DatabaseConnection.updateStudent(idStudent, student.getFirstName(), student.getLastName(),
        student.getGender(), student.getBirthday(), student.getEmail(), student.getIdClasse());

    return student;
  }

  /**
   * deletes a student.
   *
   * @param id id of student to delete.
   * @return if deleted, returns true.
   */
  public Boolean deleteStudent(String id) {
    students.remove(id);

    _DatabaseConnection.deleteStudent(id);

    return true;

  }

  /**
   * deletes students.
   *
   * @param ids ids of students to delete.
   * @return List of students without the deleted ones.
   */
  public ArrayList<StudentDetails> deleteStudents(ArrayList<String> ids) {

    for (int i = 0; i < ids.size(); ++i) {
      deleteStudent(ids.get(i));
    }

    return getStudentDetails();
  }

  /**
   * gets all student details.
   *
   * @return Arraylist with all student details.
   */
  public ArrayList<StudentDetails> getStudentDetails() {
    /*
     * ArrayList<StudentDetails> studentDetails = new ArrayList<StudentDetails>();
     *
     * Iterator<String> it = students.keySet().iterator(); while (it.hasNext()) {
     * Student student = students.get(it.next());
     * studentDetails.add(student.getLightWeightStudent()); }
     *
     * return studentDetails;
     */

    ArrayList<StudentDetails> studentDetails = new ArrayList<StudentDetails>();

    Iterator<String> it = students.keySet().iterator();
    while (it.hasNext()) {
      Student student = students.get(it.next());
      studentDetails.add(student.getLightWeightStudent());
    }

    return studentDetails;

  }

  public Student getStudent(String id) {
    return students.get(id);
  }

}
