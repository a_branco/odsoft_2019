package pt.isep.cms.subjects.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import pt.isep.cms.Database.DatabaseConnection;
import pt.isep.cms.subjects.client.SubjectsService;
import pt.isep.cms.subjects.shared.Subject;
import pt.isep.cms.subjects.shared.SubjectDetails;

@SuppressWarnings("serial")
public class SubjectsServiceImpl extends RemoteServiceServlet implements SubjectsService {

  DatabaseConnection _DatabaseConnection = new DatabaseConnection();

  private HashMap<String, Subject> subjects = new HashMap<String, Subject>();

  public SubjectsServiceImpl() {
    initSubjects();
  }

  private void initSubjects() {
    subjects = _DatabaseConnection.getSubjects();
  }

  /**
 * Adds subject to database.
 * @param subject subject.
 * @return returns the added subject.
 */
  public Subject addSubject(Subject subject) {
   
    String name;
    name = subject.getName();

    String designation;
    designation = subject.getDesignation();

    String infos;
    infos = subject.getInfos();

    String teacher;
    teacher = subject.getIdTeacher();
    
    // Add subject to database. The ID of subject is auto increment
    _DatabaseConnection.addSubject(name, designation, infos, teacher);

    // After adding the subject to the database we need to know what ID has been assigned to add to the hash map
    String id = _DatabaseConnection.lastSubjectId();
    
    subject.setId(id);
    subjects.put(subject.getId(), subject);
    
    return subject;
  }

  public HashMap<String, String> getTeachersDropDown() {
    HashMap<String, String> subjects = _DatabaseConnection.getTeachersDropDown();
    return subjects;
  }

  /**
 * updates subject.
 * @param subject subject to update.
 * @return returns updated subject.
 */
  public Subject updateSubject(Subject subject) {
    
    String lid = subject.getId();
    int idSubject = Integer.parseInt(lid);

    subjects.remove(subject.getId());
    subjects.put(subject.getId(), subject);
    
    _DatabaseConnection.updateSubject(idSubject, subject.getName(), subject.getDesignation(), subject.getInfos(), subject.getIdTeacher());

    return subject;
    
  }

  /**
 * deletes a subject.
 * @param id id of subject to delete.
 * @return if deleted, returns true.
 */
  public Boolean deleteSubject(String id) {
    subjects.remove(id);
    
    _DatabaseConnection.deleteSubject(id);    

    return true;
  }

  /**
 * deletes subjects.
 * @param ids ids of subjects to delete.
 * @return List of subjects without the deleted ones.
 */
  public ArrayList<SubjectDetails> deleteSubjects(ArrayList<String> ids) {

    for (int i = 0; i < ids.size(); ++i) {
      deleteSubject(ids.get(i));
    }

    return getSubjectDetails();
  }

  /**
 * gets all subject details.
 * @return Arraylist with all subject details.
 */
  public ArrayList<SubjectDetails> getSubjectDetails() {
    /*ArrayList<SubjectDetails> subjectDetails = new ArrayList<SubjectDetails>();

    Iterator<String> it = subjects.keySet().iterator();
    while (it.hasNext()) {
      Subject subject = subjects.get(it.next());
      subjectDetails.add(subject.getLightWeightSubject());
    }

    return subjectDetails;*/

    ArrayList<SubjectDetails> subjectDetails = new ArrayList<SubjectDetails>();

    Iterator<String> it = subjects.keySet().iterator();
    while (it.hasNext()) {
      Subject subject = subjects.get(it.next());
      subjectDetails.add(subject.getLightWeightSubject());
    }

    return subjectDetails;

  }

  public Subject getSubject(String id) {
    return subjects.get(id);
  }

}
