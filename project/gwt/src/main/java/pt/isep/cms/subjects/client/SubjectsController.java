package pt.isep.cms.subjects.client;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.i18n.client.Constants;
import com.google.gwt.user.client.ui.HasWidgets;

import pt.isep.cms.client.ShowcaseConstants;

import pt.isep.cms.subjects.client.event.AddSubjectEvent;
import pt.isep.cms.subjects.client.event.AddSubjectEventHandler;
import pt.isep.cms.subjects.client.event.EditSubjectCancelledEvent;
import pt.isep.cms.subjects.client.event.EditSubjectCancelledEventHandler;
import pt.isep.cms.subjects.client.event.EditSubjectEvent;
import pt.isep.cms.subjects.client.event.EditSubjectEventHandler;
import pt.isep.cms.subjects.client.event.SubjectUpdatedEvent;
import pt.isep.cms.subjects.client.event.SubjectUpdatedEventHandler;
import pt.isep.cms.subjects.client.presenter.EditSubjectPresenter;
import pt.isep.cms.subjects.client.presenter.Presenter;
import pt.isep.cms.subjects.client.presenter.SubjectsPresenter;
import pt.isep.cms.subjects.client.view.SubjectsDialog;
import pt.isep.cms.subjects.client.view.SubjectsView;

public class SubjectsController implements Presenter {
  //(ATB) No history at this level, ValueChangeHandler<String> {
  private final HandlerManager eventBus;
  private final SubjectsServiceAsync rpcService;
  private HasWidgets container;

  public static interface CwConstants extends Constants {
  }

  /**
   * An instance of the constants.
   */
  private final CwConstants constants;
  private final ShowcaseConstants globalConstants;

  /**
 * Constructor for SubjectsController Class.
 * @param rpcService service.
 * @param eventBus event.
 * @param constants constants.
 */
  public SubjectsController(SubjectsServiceAsync rpcService,
        HandlerManager eventBus, ShowcaseConstants constants) {
    this.eventBus = eventBus;
    this.rpcService = rpcService;
    this.constants = constants;
    this.globalConstants = constants;

    bind();
  }

  private void bind() {
    // (ATB) No History at this level
    // History.addValueChangeHandler(this);

    eventBus.addHandler(AddSubjectEvent.TYPE, new AddSubjectEventHandler() {
      public void onAddSubject(AddSubjectEvent event) {
        doAddNewSubject();
      }
    });

    eventBus.addHandler(EditSubjectEvent.TYPE, new EditSubjectEventHandler() {
      public void onEditSubject(EditSubjectEvent event) {
        doEditSubject(event.getId());
      }
    });

    eventBus.addHandler(EditSubjectCancelledEvent.TYPE, new EditSubjectCancelledEventHandler() {
      public void onEditSubjectCancelled(EditSubjectCancelledEvent event) {
        doEditSubjectCancelled();
      }
    });

    eventBus.addHandler(SubjectUpdatedEvent.TYPE, new SubjectUpdatedEventHandler() {
      public void onSubjectUpdated(SubjectUpdatedEvent event) {
        doSubjectUpdated();
      }
    });
  }

  private void doAddNewSubject() {
    // Lets use the presenter to display a dialog...
    Presenter presenter = new EditSubjectPresenter(rpcService, eventBus,
        new SubjectsDialog(rpcService, globalConstants, SubjectsDialog.Type.ADD));
    presenter.go(container);

  }

  private void doEditSubject(String id) {
    Presenter presenter = new EditSubjectPresenter(rpcService, eventBus,
        new SubjectsDialog(rpcService, globalConstants, SubjectsDialog.Type.UPDATE), id);
    presenter.go(container);
  }

  private void doEditSubjectCancelled() {
    // Nothing to update...
  }

  private void doSubjectUpdated() {
    // (ATB) Update the list of Subjects...
    Presenter presenter = new SubjectsPresenter(rpcService, eventBus, new SubjectsView());
    presenter.go(container);
  }

  /**
 * go.
 * @param container container.
 */
  public void go(final HasWidgets container) {
    this.container = container;

    Presenter presenter = new SubjectsPresenter(rpcService, eventBus, new SubjectsView());
    presenter.go(container);
  }

}
