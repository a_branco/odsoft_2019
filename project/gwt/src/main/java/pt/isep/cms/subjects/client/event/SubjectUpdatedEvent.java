package pt.isep.cms.subjects.client.event;

import com.google.gwt.event.shared.GwtEvent;
import pt.isep.cms.subjects.shared.Subject;

public class SubjectUpdatedEvent extends GwtEvent<SubjectUpdatedEventHandler> {
  public final static Type<SubjectUpdatedEventHandler> TYPE =
      new Type<SubjectUpdatedEventHandler>();
  private final Subject updatedSubject;

  public SubjectUpdatedEvent(Subject updatedSubject) {
    this.updatedSubject = updatedSubject;
  }

  public Subject getUpdatedSubject() {
    return updatedSubject;
  }


  @Override
  public Type<SubjectUpdatedEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(SubjectUpdatedEventHandler handler) {
    handler.onSubjectUpdated(this);
  }
}
