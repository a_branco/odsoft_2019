package pt.isep.cms.teachers.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditTeacherEventHandler extends EventHandler {
  void onEditTeacher(EditTeacherEvent event);
}
