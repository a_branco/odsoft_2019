package pt.isep.cms.classes.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditClassCancelledEventHandler extends EventHandler {
  void onEditClassCancelled(EditClassCancelledEvent event);
}
