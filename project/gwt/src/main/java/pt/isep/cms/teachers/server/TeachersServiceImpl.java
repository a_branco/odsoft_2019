package pt.isep.cms.teachers.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import pt.isep.cms.Database.DatabaseConnection;
import pt.isep.cms.teachers.client.TeachersService;
import pt.isep.cms.teachers.shared.Teacher;
import pt.isep.cms.teachers.shared.TeacherDetails;

@SuppressWarnings("serial")
public class TeachersServiceImpl extends RemoteServiceServlet implements TeachersService {

  DatabaseConnection _DatabaseConnection = new DatabaseConnection();

  private HashMap<String, Teacher> teachers = new HashMap<String, Teacher>();

  public TeachersServiceImpl() {
    initTeachers();
  }

  private void initTeachers() {
    teachers = _DatabaseConnection.getTeachers();
  }

  /**
   * Adds Teacher.
   *
   * @param teacher teacher to add.
   * @return added teacher.
   */
  public Teacher addTeacher(Teacher teacher) {

    String firstName;
    firstName = teacher.getFirstName();

    String lastName;
    lastName = teacher.getLastName();

    String gender;
    gender = teacher.getGender();

    Date birthday;
    birthday = teacher.getBirthday();

    // Add teacher to database. The ID of teacher is auto increment
    _DatabaseConnection.addTeacher(firstName, lastName, gender, birthday);

    // After adding the teacher to the database we need to
    // know what ID has been assigned to add to the hash map
    String id = _DatabaseConnection.lastTeacherID();

    teacher.setId(id);
    teachers.put(teacher.getId(), teacher);

    return teacher;
  }

  /**
   * Updates a Teacher.
   *
   * @param teacher teacher to update.
   * @return updated teacher.
   */
  public Teacher updateTeacher(Teacher teacher) {
    String lid = teacher.getId();
    int idTeacher = Integer.parseInt(lid);

    teachers.remove(teacher.getId());
    teachers.put(teacher.getId(), teacher);
    _DatabaseConnection.updateTeacher(idTeacher, teacher.getFirstName(),
        teacher.getLastName(), teacher.getGender(), teacher.getBirthday());

    return teacher;
  }

  /**
   * deletes a teacher.
   *
   * @param id id of teacher to delete.
   * @return if deleted, returns true.
   */
  public Boolean deleteTeacher(String id) {
    teachers.remove(id);

    _DatabaseConnection.deleteTeacher(id);

    return true;
  }

  /**
   * deletes teachers.
   *
   * @param ids id of teachers to delete.
   * @return List of Teacher without the deleted ones.
   */
  public ArrayList<TeacherDetails> deleteTeachers(ArrayList<String> ids) {

    for (int i = 0; i < ids.size(); ++i) {
      deleteTeacher(ids.get(i));
    }

    return getTeacherDetails();
  }

  /**
   * gets list of details of all teachers.
   *
   * @return List of all teachers details.
   */
  public ArrayList<TeacherDetails> getTeacherDetails() {

    ArrayList<TeacherDetails> teacherDetails = new ArrayList<TeacherDetails>();

    Iterator<String> it = teachers.keySet().iterator();
    while (it.hasNext()) {
      Teacher teacher = teachers.get(it.next());
      teacherDetails.add(teacher.getLightWeightTeacher());
    }

    return teacherDetails;

  }

  public Teacher getTeacher(String id) {
    return teachers.get(id);
  }

}
