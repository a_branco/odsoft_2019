package pt.isep.cms.teachers.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditTeacherCancelledEventHandler extends EventHandler {
  void onEditTeacherCancelled(EditTeacherCancelledEvent event);
}
