package pt.isep.cms.subjects.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class EditSubjectEvent extends GwtEvent<EditSubjectEventHandler> {
  public final static Type<EditSubjectEventHandler> TYPE =
      new Type<EditSubjectEventHandler>();
  private final String id;

  public EditSubjectEvent(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  @Override
  public Type<EditSubjectEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(EditSubjectEventHandler handler) {
    handler.onEditSubject(this);
  }
}
