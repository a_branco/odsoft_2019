package pt.isep.cms.students.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@SuppressWarnings("serial")
public class Student implements Serializable, IsSerializable {
  public String id;
  public String firstName;
  public String lastName;
  public Date birthday;
  public String gender;
  public String email;
  public String idClasse;

  public Student() {
  }

  /**
   * Constructor for Student Class.
   *
   * @param id        id.
   * @param firstName firstName.
   * @param lastName  lastName.
   * @param birthday  birthday.
   * @param gender    gender.
   * @param email     email.
   */
  public Student(String id, String firstName, String lastName,
                 Date birthday, String gender, String email, String idClasse) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = birthday;
    this.gender = gender;
    this.email = email;
    this.idClasse = idClasse;
  }

  public StudentDetails getLightWeightStudent() {
    return new StudentDetails(id, getFullName());
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setIdClasse(String idClasse) {
    this.idClasse = idClasse;
  }

  public String getIdClasse() {
    return idClasse;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFullName() {
    return firstName + " " + lastName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Student student = (Student) o;
    return id.equals(student.id)
      && firstName.equals(student.firstName)
      && lastName.equals(student.lastName)
      && birthday.equals(student.birthday)
      && gender.equals(student.gender) && email.equals(student.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, firstName, lastName, birthday, gender);
  }
}
