package pt.isep.cms.teachers.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class EditTeacherEvent extends GwtEvent<EditTeacherEventHandler> {
  public final static Type<EditTeacherEventHandler> TYPE =
      new Type<EditTeacherEventHandler>();
  private final String id;

  public EditTeacherEvent(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  @Override
  public Type<EditTeacherEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(EditTeacherEventHandler handler) {
    handler.onEditTeacher(this);
  }
}
