package pt.isep.cms.classes.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

import java.util.ArrayList;
import java.util.List;

import pt.isep.cms.classes.client.ClassesServiceAsync;
import pt.isep.cms.classes.client.event.AddClassEvent;
import pt.isep.cms.classes.client.event.EditClassEvent;
import pt.isep.cms.classes.shared.ClassDetails;

public class ClassesPresenter implements Presenter {

  private List<ClassDetails> classDetails;

  public interface Display {
    HasClickHandlers getAddButton();

    HasClickHandlers getDeleteButton();

    HasClickHandlers getList();

    void setData(List<String> data);

    int getClickedRow(ClickEvent event);

    List<Integer> getSelectedRows();

    Widget asWidget();
  }

  private final ClassesServiceAsync rpcService;
  private final HandlerManager eventBus;
  private final Display display;

  /**
 * Constructor for ClassPresenter Class.
 *
 * @param rpcService service.
 * @param eventBus eventBus.
 * @param view view.
 */
  public ClassesPresenter(ClassesServiceAsync rpcService, HandlerManager eventBus, Display view) {
    this.rpcService = rpcService;
    this.eventBus = eventBus;
    this.display = view;
  }

  /**
  * bind.
  */
  public void bind() {
    display.getAddButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        eventBus.fireEvent(new AddClassEvent());
      }
    });

    display.getDeleteButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        deleteSelectedClasses();
      }
    });

    display.getList().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        int selectedRow = display.getClickedRow(event);

        if (selectedRow >= 0) {
          String id = classDetails.get(selectedRow).getId();
          eventBus.fireEvent(new EditClassEvent(id));
        }
      }
    });
  }

  /**
  * go.
  *
  * @param container container
  */
  public void go(final HasWidgets container) {
    bind();
    container.clear();
    container.add(display.asWidget());

    fetchClassDetails();
  }

  /**
  * Sorts Class Details.
  */
  public void sortClassDetails() {

    // Yes, we could use a more optimized method of sorting, but the
    // point is to create a test case that helps illustrate the higher
    // level concepts used when creating MVP-based applications.
    //
    for (int i = 0; i < classDetails.size(); ++i) {
      for (int j = 0; j < classDetails.size() - 1; ++j) {
        if (classDetails.get(j).getDisplayName()
            .compareToIgnoreCase(classDetails.get(j + 1).getDisplayName()) >= 0) {
          ClassDetails tmp = classDetails.get(j);
          classDetails.set(j, classDetails.get(j + 1));
          classDetails.set(j + 1, tmp);
        }
      }
    }
  }

  public void setClassDetails(List<ClassDetails> classDetails) {
    this.classDetails = classDetails;
  }

  public ClassDetails getClassDetail(int index) {
    return classDetails.get(index);
  }

  private void fetchClassDetails() {
    rpcService.getClassDetails(new AsyncCallback<ArrayList<ClassDetails>>() {
      public void onSuccess(ArrayList<ClassDetails> result) {
        classDetails = result;
        sortClassDetails();
        List<String> data = new ArrayList<String>();

        for (int i = 0; i < result.size(); ++i) {
          data.add(classDetails.get(i).getDisplayName());
        }

        display.setData(data);
      }

      public void onFailure(Throwable caught) {
        Window.alert(caught.getMessage());
        Window.alert("Error fetching class details");
      }
    });
  }

  private void deleteSelectedClasses() {
    List<Integer> selectedRows = display.getSelectedRows();
    ArrayList<String> ids = new ArrayList<String>();

    for (int i = 0; i < selectedRows.size(); ++i) {
      ids.add(classDetails.get(selectedRows.get(i)).getId());
    }

    rpcService.deleteClasses(ids, new AsyncCallback<ArrayList<ClassDetails>>() {
      public void onSuccess(ArrayList<ClassDetails> result) {
        classDetails = result;
        sortClassDetails();
        List<String> data = new ArrayList<String>();

        for (int i = 0; i < result.size(); ++i) {
          data.add(classDetails.get(i).getDisplayName());
        }

        display.setData(data);

      }

      public void onFailure(Throwable caught) {
        Window.alert("Error deleting selected classes");
      }
    });
  }
}
