package pt.isep.cms.subjects.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@SuppressWarnings("serial")
public class Subject implements Serializable, IsSerializable {
  public String idSubject;
  public String name;
  public String designation;
  public String infos;
  public String idTeacher;

  public Subject() {
  }

  /**
 * Constructor for Subject Class.
 * @param idSubject idSubject.
 * @param name name.
 * @param designation designation.
 * @param infos infos.
 * @param idTeacher idTeacher.
 */
  public Subject(String idSubject, String name, String designation,
      String infos, String idTeacher) {
    this.idSubject = idSubject;
    this.name = name;
    this.designation = designation;
    this.infos = infos;
    this.idTeacher = idTeacher;
  }

  public SubjectDetails getLightWeightSubject() {
    return new SubjectDetails(idSubject, getDesignation());
  }

  public String getId() {
    return idSubject;
  }

  public void setId(String idSubject) {
    this.idSubject = idSubject;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDesignation() {
    return designation;
  }

  public void setDesignation(String designation) {
    this.designation = designation;
  }

  public String getInfos() {
    return infos;
  }

  public void setInfos(String infos) {
    this.infos = infos;
  }

  public String getIdTeacher() {
    return idTeacher;
  }

  public void setIdTeacher(String idTeacher) {
    this.idTeacher = idTeacher;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Subject subject = (Subject) o;
    return idSubject.equals(subject.idSubject)
      && name.equals(subject.name)
      && designation.equals(subject.designation)
      && infos.equals(subject.infos);
      
  }

  @Override
  public int hashCode() {
    return Objects.hash(idSubject, name, designation, infos, idTeacher);
  }
}
