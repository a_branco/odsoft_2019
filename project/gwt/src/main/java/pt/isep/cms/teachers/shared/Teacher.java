package pt.isep.cms.teachers.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@SuppressWarnings("serial")
public class Teacher implements Serializable, IsSerializable {
  public String id;
  public String firstName;
  public String lastName;
  public Date birthday;
  public String gender;

  public Teacher() {
  }

  /**
  * Constructor for Teacher Class.
  * @param id id.
  * @param firstName firstName.
  * @param lastName lastName.
  * @param birthday birthday.
  * @param gender gender.
  */
  public Teacher(String id, String firstName, String lastName, Date birthday, String gender) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = birthday;
    this.gender = gender;
  }

  public TeacherDetails getLightWeightTeacher() {
    return new TeacherDetails(id, getFullName());
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getFullName() {
    return firstName + " " + lastName;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Teacher teacher = (Teacher) o;
    return id.equals(teacher.id)
      && firstName.equals(teacher.firstName)
      && lastName.equals(teacher.lastName)
      && birthday.equals(teacher.birthday)
      && gender.equals(teacher.gender);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, firstName, lastName, birthday, gender);
  }
}
