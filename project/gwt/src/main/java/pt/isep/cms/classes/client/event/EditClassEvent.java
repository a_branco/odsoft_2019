package pt.isep.cms.classes.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class EditClassEvent extends GwtEvent<EditClassEventHandler> {
  public final static Type<EditClassEventHandler> TYPE =
      new Type<EditClassEventHandler>();
  private final String id;

  public EditClassEvent(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  @Override
  public Type<EditClassEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(EditClassEventHandler handler) {
    handler.onEditClass(this);
  }
}
