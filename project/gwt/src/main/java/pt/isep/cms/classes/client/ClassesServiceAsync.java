package pt.isep.cms.classes.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

import pt.isep.cms.classes.shared.ClassDetails;
import pt.isep.cms.classes.shared.Classe;

public interface ClassesServiceAsync {

  public void addClass(Classe classe, AsyncCallback<Classe> callback);

  public void deleteClass(String id, AsyncCallback<Boolean> callback);

  public void deleteClasses(ArrayList<String> ids, AsyncCallback<ArrayList<ClassDetails>> callback);

  public void getClassDetails(AsyncCallback<ArrayList<ClassDetails>> callback);

  public void getClass(String id, AsyncCallback<Classe> callback);

  public void updateClass(Classe classe, AsyncCallback<Classe> callback);
}

