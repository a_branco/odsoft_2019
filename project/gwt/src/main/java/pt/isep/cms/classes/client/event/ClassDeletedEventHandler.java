package pt.isep.cms.classes.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ClassDeletedEventHandler extends EventHandler {
  void onClassDeleted(ClassDeletedEvent event);
}
