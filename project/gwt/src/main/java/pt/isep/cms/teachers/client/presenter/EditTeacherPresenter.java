package pt.isep.cms.teachers.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.datepicker.client.DateBox;
import pt.isep.cms.teachers.client.TeachersServiceAsync;
import pt.isep.cms.teachers.client.event.EditTeacherCancelledEvent;
import pt.isep.cms.teachers.client.event.TeacherUpdatedEvent;
import pt.isep.cms.teachers.shared.Teacher;

public class EditTeacherPresenter implements Presenter {
  public interface Display {
    HasClickHandlers getSaveButton();

    HasClickHandlers getCancelButton();

    HasValue<String> getFirstName();

    HasValue<String> getLastName();

    HasValue<String> getGender();

    DateBox getBirthday();

    void show();

    void hide();
  }

  private Teacher teacher;
  private final TeachersServiceAsync rpcService;
  private final HandlerManager eventBus;
  private final Display display;

  /**
 * Constructor for EditTeacherPresenter Class.
 * @param rpcService service.
 * @param eventBus event.
 * @param display display.
 */
  public EditTeacherPresenter(TeachersServiceAsync rpcService,
        HandlerManager eventBus, Display display) {
    this.rpcService = rpcService;
    this.eventBus = eventBus;
    this.teacher = new Teacher();
    this.display = display;
    bind();
  }

  /**
 * Constructor for EditTeacherPresenter Class.
 * @param rpcService service.
 * @param eventBus event.
 * @param display display.
 * @param id id.
 */
  public EditTeacherPresenter(TeachersServiceAsync rpcService,
        HandlerManager eventBus, Display display, String id) {
    this.rpcService = rpcService;
    this.eventBus = eventBus;
    this.display = display;
    bind();

    rpcService.getTeacher(id, new AsyncCallback<Teacher>() {
      public void onSuccess(Teacher result) {
        teacher = result;
        EditTeacherPresenter.this.display.getFirstName().setValue(teacher.getFirstName());
        EditTeacherPresenter.this.display.getLastName().setValue(teacher.getLastName());
        EditTeacherPresenter.this.display.getGender().setValue(teacher.getGender());
        EditTeacherPresenter.this.display.getBirthday().setValue(teacher.getBirthday());
      }

      public void onFailure(Throwable caught) {
        Window.alert("Error retrieving teacher");
      }
    });

  }

  /**
 * bind.
 */
  public void bind() {
    this.display.getSaveButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        doSave();
        display.hide();
      }
    });

    this.display.getCancelButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        display.hide();
        eventBus.fireEvent(new EditTeacherCancelledEvent());
      }
    });
  }

  public void go(final HasWidgets container) {
    display.show();
  }

  private void doSave() {
    teacher.setFirstName(display.getFirstName().getValue());
    teacher.setLastName(display.getLastName().getValue());
    teacher.setGender(display.getGender().getValue());
    teacher.setBirthday(display.getBirthday().getValue());

    if (teacher.getId() == null) {
      // Adding new teacher
      rpcService.addTeacher(teacher, new AsyncCallback<Teacher>() {
        public void onSuccess(Teacher result) {
          eventBus.fireEvent(new TeacherUpdatedEvent(result));
        }

        public void onFailure(Throwable caught) {
          Window.alert("Error adding teacher");
        }
      });
    } else {
      // updating existing teacher
      rpcService.updateTeacher(teacher, new AsyncCallback<Teacher>() {
        public void onSuccess(Teacher result) {
          eventBus.fireEvent(new TeacherUpdatedEvent(result));
        }

        public void onFailure(Throwable caught) {
          Window.alert("Error updating teacher");
        }
      });
    }
  }

}
