package pt.isep.cms.classes.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import pt.isep.cms.classes.client.ClassesServiceAsync;
import pt.isep.cms.classes.client.event.ClassUpdatedEvent;
import pt.isep.cms.classes.client.event.EditClassCancelledEvent;
import pt.isep.cms.classes.shared.Classe;

public class EditClassPresenter implements Presenter {
  public interface Display {
    HasClickHandlers getSaveButton();

    HasClickHandlers getCancelButton();

    HasValue<String> getDesignation();

    HasValue<String> getName();

    void show();

    void hide();
  }

  private Classe classe;
  private final ClassesServiceAsync rpcService;
  private final HandlerManager eventBus;
  private final Display display;

  /**
 * Constructor for EditClassPresenter Class.
 * @param rpcService service.
 * @param eventBus event.
 * @param display display.
 */
  public EditClassPresenter(ClassesServiceAsync rpcService,
        HandlerManager eventBus, Display display) {
    this.rpcService = rpcService;
    this.eventBus = eventBus;
    this.classe = new Classe();
    this.display = display;
    bind();
  }

  /**
 * Constructor for EditClassPresenter Class.
 * @param rpcService service.
 * @param eventBus event.
 * @param display display.
 * @param id id.
 */
  public EditClassPresenter(ClassesServiceAsync rpcService,
        HandlerManager eventBus, Display display, String id) {
    this.rpcService = rpcService;
    this.eventBus = eventBus;
    this.display = display;
    bind();

    rpcService.getClass(id, new AsyncCallback<Classe>() {
      public void onSuccess(Classe result) {
        classe = result;
        EditClassPresenter.this.display.getDesignation().setValue(classe.getDesignation());
        EditClassPresenter.this.display.getName().setValue(classe.getName());
      }

      public void onFailure(Throwable caught) {
        Window.alert("Error retrieving class");
      }
    });

  }

  /**
 * bind.
 */
  public void bind() {
    this.display.getSaveButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        doSave();
        display.hide();
      }
    });

    this.display.getCancelButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        display.hide();
        eventBus.fireEvent(new EditClassCancelledEvent());
      }
    });
  }

  public void go(final HasWidgets container) {
    display.show();
  }

  private void doSave() {
    classe.setDesignation(display.getDesignation().getValue());
    classe.setName(display.getName().getValue());

    if (classe.getId() == null) {
      // Adding new class
      rpcService.addClass(classe, new AsyncCallback<Classe>() {
        public void onSuccess(Classe result) {
          eventBus.fireEvent(new ClassUpdatedEvent(result));
        }

        public void onFailure(Throwable caught) {
          Window.alert("Error adding class");
        }
      });
    } else {
      // updating existing class
      rpcService.updateClass(classe, new AsyncCallback<Classe>() {
        public void onSuccess(Classe result) {
          eventBus.fireEvent(new ClassUpdatedEvent(result));
        }

        public void onFailure(Throwable caught) {
          Window.alert("Error updating class");
        }
      });
    }
  }

}
