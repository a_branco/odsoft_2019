package pt.isep.cms.classes.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import pt.isep.cms.Database.DatabaseConnection;
import pt.isep.cms.classes.client.ClassesService;
import pt.isep.cms.classes.shared.ClassDetails;
import pt.isep.cms.classes.shared.Classe;


@SuppressWarnings("serial")
public class ClassesServiceImpl extends RemoteServiceServlet implements ClassesService {

  DatabaseConnection _DatabaseConnection = new DatabaseConnection();

  private HashMap<String, Classe> classes = new HashMap<String, Classe>();

  public ClassesServiceImpl() {
    initClasses();
  }

  private void initClasses() {
    classes = _DatabaseConnection.getClasses();
  }

  /**
  * Add Class method.
  * @param classe classe to add.
  * @return returns the added class.
  */
  public Classe addClass(Classe classe) {
   

    String designation;
    String name;

    designation = classe.getDesignation();
    name = classe.getName();

    _DatabaseConnection.addClass(designation, name);

    // After adding the class to the database we need to know
    // what ID has been assigned to add to the hash map
    String id = _DatabaseConnection.lastClassID();
    
    classe.setId(id);
    
    classes.put(classe.getId(), classe);
    
    return classe;
  }

  /**
 * Update Classe method.
 * @param classe classe to update
 * @return returns updated classe.
 */
  public Classe updateClass(Classe classe) {
    String lid = classe.getId();
    int idClass = Integer.parseInt(lid);

    classes.remove(classe.getId());
    classes.put(classe.getId(), classe);
    _DatabaseConnection.updateClass(idClass, classe.getDesignation(), classe.getName());

    return classe;

  }

  /**
 * Delete Classe method.
 * @param id id.
 * @return if deleted then returns true.
 */
  public Boolean deleteClass(String id) {
    classes.remove(id);
    _DatabaseConnection.deleteClass(id);
    return true;
  }

  /**
 * Delete Classes method.
 * @param ids ids.
 * @return returns new List without the deleted Classes.
 */
  public ArrayList<ClassDetails> deleteClasses(ArrayList<String> ids) {

    for (int i = 0; i < ids.size(); ++i) {
      deleteClass(ids.get(i));
    }

    return getClassDetails();
  }

  /**
 * Gets List with classes details.
 * @return List with details of all Classes
 */
  public ArrayList<ClassDetails> getClassDetails() {

    ArrayList<ClassDetails> classDetails = new ArrayList<ClassDetails>();

    Iterator<String> it = classes.keySet().iterator();
    while (it.hasNext()) {
      Classe classe = classes.get(it.next());
      classDetails.add(classe.getLightWeightClass());
    }

    return classDetails;

  }

  public Classe getClass(String id) {
    return classes.get(id);
  }

}
