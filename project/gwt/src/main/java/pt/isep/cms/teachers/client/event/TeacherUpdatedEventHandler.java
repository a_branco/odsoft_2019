package pt.isep.cms.teachers.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface TeacherUpdatedEventHandler extends EventHandler {
  void onTeacherUpdated(TeacherUpdatedEvent event);
}
