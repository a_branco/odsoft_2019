package pt.isep.cms.subjects.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

import java.util.ArrayList;
import java.util.List;

import pt.isep.cms.subjects.client.SubjectsServiceAsync;
import pt.isep.cms.subjects.client.event.AddSubjectEvent;
import pt.isep.cms.subjects.client.event.EditSubjectEvent;
import pt.isep.cms.subjects.shared.SubjectDetails;

public class SubjectsPresenter implements Presenter {

  private List<SubjectDetails> subjectDetails;

  public interface Display {
    HasClickHandlers getAddButton();

    HasClickHandlers getDeleteButton();

    HasClickHandlers getList();

    void setData(List<String> data);

    int getClickedRow(ClickEvent event);

    List<Integer> getSelectedRows();

    Widget asWidget();
  }

  private final SubjectsServiceAsync rpcService;
  private final HandlerManager eventBus;
  private final Display display;

  /**
 * Constructor for SubjectsPresenter Class.
 * @param rpcService service.
 * @param eventBus event.
 * @param view view.
 */
  public SubjectsPresenter(SubjectsServiceAsync rpcService,
        HandlerManager eventBus, Display view) {
    this.rpcService = rpcService;
    this.eventBus = eventBus;
    this.display = view;
  }

  /**
 * bind.
 */
  public void bind() {
    display.getAddButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        eventBus.fireEvent(new AddSubjectEvent());
      }
    });

    display.getDeleteButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        deleteSelectedSubjects();
      }
    });

    display.getList().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        int selectedRow = display.getClickedRow(event);

        if (selectedRow >= 0) {
          String id = subjectDetails.get(selectedRow).getId();
          eventBus.fireEvent(new EditSubjectEvent(id));
        }
      }
    });
  }

  /**
 * go.
 * @param container container.
 */
  public void go(final HasWidgets container) {
    bind();
    container.clear();
    container.add(display.asWidget());

    fetchSubjectDetails();
  }

  /**
 * Sorts Subjects Details.
 */
  public void sortSubjectDetails() {

    // Yes, we could use a more optimized method of sorting, but the
    // point is to create a test case that helps illustrate the higher
    // level concepts used when creating MVP-based applications.
    //
    for (int i = 0; i < subjectDetails.size(); ++i) {
      for (int j = 0; j < subjectDetails.size() - 1; ++j) {
        if (subjectDetails.get(j).getDisplayName()
            .compareToIgnoreCase(subjectDetails.get(j + 1).getDisplayName()) >= 0) {
          SubjectDetails tmp = subjectDetails.get(j);
          subjectDetails.set(j, subjectDetails.get(j + 1));
          subjectDetails.set(j + 1, tmp);
        }
      }
    }
  }

  public void setSubjectDetails(List<SubjectDetails> subjectDetails) {
    this.subjectDetails = subjectDetails;
  }

  public SubjectDetails getSubjectDetail(int index) {
    return subjectDetails.get(index);
  }

  private void fetchSubjectDetails() {
    rpcService.getSubjectDetails(new AsyncCallback<ArrayList<SubjectDetails>>() {
      public void onSuccess(ArrayList<SubjectDetails> result) {
        subjectDetails = result;
        sortSubjectDetails();
        List<String> data = new ArrayList<String>();

        for (int i = 0; i < result.size(); ++i) {
          data.add(subjectDetails.get(i).getDisplayName());
        }

        display.setData(data);
      }

      public void onFailure(Throwable caught) {
        Window.alert(caught.getMessage());
        Window.alert("Error fetching subject details");
      }
    });
  }

  private void deleteSelectedSubjects() {
    List<Integer> selectedRows = display.getSelectedRows();
    ArrayList<String> ids = new ArrayList<String>();

    for (int i = 0; i < selectedRows.size(); ++i) {
      ids.add(subjectDetails.get(selectedRows.get(i)).getId());
    }

    rpcService.deleteSubjects(ids, new AsyncCallback<ArrayList<SubjectDetails>>() {
      public void onSuccess(ArrayList<SubjectDetails> result) {
        subjectDetails = result;
        sortSubjectDetails();
        List<String> data = new ArrayList<String>();

        for (int i = 0; i < result.size(); ++i) {
          data.add(subjectDetails.get(i).getDisplayName());
        }

        display.setData(data);

      }

      public void onFailure(Throwable caught) {
        Window.alert("Error deleting selected subjects");
      }
    });
  }
}
