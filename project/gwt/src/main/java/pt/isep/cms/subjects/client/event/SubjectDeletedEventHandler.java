package pt.isep.cms.subjects.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface SubjectDeletedEventHandler extends EventHandler {
  void onSubjectDeleted(SubjectDeletedEvent event);
}
