package pt.isep.cms.teachers.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

import pt.isep.cms.teachers.shared.Teacher;
import pt.isep.cms.teachers.shared.TeacherDetails;

public interface TeachersServiceAsync {

  public void addTeacher(Teacher teacher, AsyncCallback<Teacher> callback);

  public void deleteTeacher(String id, AsyncCallback<Boolean> callback);

  public void deleteTeachers(ArrayList<String> ids,
      AsyncCallback<ArrayList<TeacherDetails>> callback);

  public void getTeacherDetails(AsyncCallback<ArrayList<TeacherDetails>> callback);

  public void getTeacher(String id, AsyncCallback<Teacher> callback);

  public void updateTeacher(Teacher teacher, AsyncCallback<Teacher> callback);
}

