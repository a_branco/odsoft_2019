package pt.isep.cms.teachers.client.event;

import com.google.gwt.event.shared.GwtEvent;
import pt.isep.cms.teachers.shared.Teacher;

public class TeacherUpdatedEvent extends GwtEvent<TeacherUpdatedEventHandler> {
  public final static Type<TeacherUpdatedEventHandler> TYPE =
      new Type<TeacherUpdatedEventHandler>();
  private final Teacher updatedTeacher;

  public TeacherUpdatedEvent(Teacher updatedTeacher) {
    this.updatedTeacher = updatedTeacher;
  }

  public Teacher getUpdatedTeacher() {
    return updatedTeacher;
  }


  @Override
  public Type<TeacherUpdatedEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(TeacherUpdatedEventHandler handler) {
    handler.onTeacherUpdated(this);
  }
}
