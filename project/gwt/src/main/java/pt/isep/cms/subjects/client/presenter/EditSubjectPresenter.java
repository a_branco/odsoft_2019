package pt.isep.cms.subjects.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.datepicker.client.DateBox;
import pt.isep.cms.subjects.client.SubjectsServiceAsync;
import pt.isep.cms.subjects.client.event.EditSubjectCancelledEvent;
import pt.isep.cms.subjects.client.event.SubjectUpdatedEvent;
import pt.isep.cms.subjects.shared.Subject;

public class EditSubjectPresenter implements Presenter {
  public interface Display {
    HasClickHandlers getSaveButton();

    HasClickHandlers getCancelButton();

    HasValue<String> getName();

    HasValue<String> getDesignation();

    HasValue<String> getInfos();

    String getTeacher();

    void show();

    void hide();
  }

  private Subject subject;
  private final SubjectsServiceAsync rpcService;
  private final HandlerManager eventBus;
  private final Display display;

  /**
 * Constructor for EditSubjectPresenter Class.
 * @param rpcService service.
 * @param eventBus event.
 * @param display dislpay.
 */
  public EditSubjectPresenter(SubjectsServiceAsync rpcService,
        HandlerManager eventBus, Display display) {
    this.rpcService = rpcService;
    this.eventBus = eventBus;
    this.subject = new Subject();
    this.display = display;
    bind();
  }

  /**
 * Constructor for EditSubjectPresenter Class.
 * @param rpcService service.
 * @param eventBus event.
 * @param display display.
 * @param id id.
 */
  public EditSubjectPresenter(SubjectsServiceAsync rpcService,
        HandlerManager eventBus, Display display, String id) {
    this.rpcService = rpcService;
    this.eventBus = eventBus;
    this.display = display;
    bind();

    rpcService.getSubject(id, new AsyncCallback<Subject>() {
      public void onSuccess(Subject result) {
        subject = result;
        EditSubjectPresenter.this.display.getName().setValue(subject.getName());
        EditSubjectPresenter.this.display.getDesignation().setValue(subject.getDesignation());
        EditSubjectPresenter.this.display.getInfos().setValue(subject.getInfos());
        EditSubjectPresenter.this.display.getTeacher();
    
      }

      public void onFailure(Throwable caught) {
        Window.alert("Error retrieving subject");
      }
    });

  }

  /**
 * bind.
 */
  public void bind() {
    this.display.getSaveButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        doSave();
        display.hide();
      }
    });

    this.display.getCancelButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        display.hide();
        eventBus.fireEvent(new EditSubjectCancelledEvent());
      }
    });
  }

  public void go(final HasWidgets container) {
    display.show();
  }

  private void doSave() {
    subject.setName(display.getName().getValue());
    subject.setDesignation(display.getDesignation().getValue());
    subject.setInfos(display.getInfos().getValue());
    subject.setIdTeacher(display.getTeacher());

    if (subject.getId() == null) {
      // Adding new subject
      rpcService.addSubject(subject, new AsyncCallback<Subject>() {
        public void onSuccess(Subject result) {
          eventBus.fireEvent(new SubjectUpdatedEvent(result));
        }

        public void onFailure(Throwable caught) {
          Window.alert("Error adding subject");
        }
      });
    } else {
      // updating existing subject
      rpcService.updateSubject(subject, new AsyncCallback<Subject>() {
        public void onSuccess(Subject result) {
          eventBus.fireEvent(new SubjectUpdatedEvent(result));
        }

        public void onFailure(Throwable caught) {
          Window.alert("Error updating subject");
        }
      });
    }
  }

}
