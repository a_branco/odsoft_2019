package pt.isep.cms.teachers.client;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.i18n.client.Constants;
import com.google.gwt.user.client.ui.HasWidgets;

import pt.isep.cms.client.ShowcaseConstants;

import pt.isep.cms.teachers.client.event.AddTeacherEvent;
import pt.isep.cms.teachers.client.event.AddTeacherEventHandler;
import pt.isep.cms.teachers.client.event.EditTeacherCancelledEvent;
import pt.isep.cms.teachers.client.event.EditTeacherCancelledEventHandler;
import pt.isep.cms.teachers.client.event.EditTeacherEvent;
import pt.isep.cms.teachers.client.event.EditTeacherEventHandler;
import pt.isep.cms.teachers.client.event.TeacherUpdatedEvent;
import pt.isep.cms.teachers.client.event.TeacherUpdatedEventHandler;
import pt.isep.cms.teachers.client.presenter.EditTeacherPresenter;
import pt.isep.cms.teachers.client.presenter.Presenter;
import pt.isep.cms.teachers.client.presenter.TeachersPresenter;
import pt.isep.cms.teachers.client.view.TeachersDialog;
import pt.isep.cms.teachers.client.view.TeachersView;

public class TeachersController implements Presenter {
  //(ATB) No history at this level, ValueChangeHandler<String> {
  private final HandlerManager eventBus;
  private final TeachersServiceAsync rpcService;
  private HasWidgets container;

  public static interface CwConstants extends Constants {
  }

  /**
   * An instance of the constants.
   */
  private final CwConstants constants;
  private final ShowcaseConstants globalConstants;

  /**
 * Construtor for TeachersController Class.
 * @param rpcService service.
 * @param eventBus event.
 * @param constants constants.
 */
  public TeachersController(TeachersServiceAsync rpcService,
        HandlerManager eventBus, ShowcaseConstants constants) {
    this.eventBus = eventBus;
    this.rpcService = rpcService;
    this.constants = constants;
    this.globalConstants = constants;

    bind();
  }

  private void bind() {
    // (ATB) No History at this level
    // History.addValueChangeHandler(this);

    eventBus.addHandler(AddTeacherEvent.TYPE, new AddTeacherEventHandler() {
      public void onAddTeacher(AddTeacherEvent event) {
        doAddNewTeacher();
      }
    });

    eventBus.addHandler(EditTeacherEvent.TYPE, new EditTeacherEventHandler() {
      public void onEditTeacher(EditTeacherEvent event) {
        doEditTeacher(event.getId());
      }
    });

    eventBus.addHandler(EditTeacherCancelledEvent.TYPE, new EditTeacherCancelledEventHandler() {
      public void onEditTeacherCancelled(EditTeacherCancelledEvent event) {
        doEditTeacherCancelled();
      }
    });

    eventBus.addHandler(TeacherUpdatedEvent.TYPE, new TeacherUpdatedEventHandler() {
      public void onTeacherUpdated(TeacherUpdatedEvent event) {
        doTeacherUpdated();
      }
    });
  }

  private void doAddNewTeacher() {
    // Lets use the presenter to display a dialog...
    Presenter presenter = new EditTeacherPresenter(rpcService, eventBus,
        new TeachersDialog(globalConstants, TeachersDialog.Type.ADD));
    presenter.go(container);

  }

  private void doEditTeacher(String id) {
    Presenter presenter = new EditTeacherPresenter(rpcService, eventBus,
        new TeachersDialog(globalConstants, TeachersDialog.Type.UPDATE), id);
    presenter.go(container);
  }

  private void doEditTeacherCancelled() {
    // Nothing to update...
  }

  private void doTeacherUpdated() {
    // (ATB) Update the list of teachers...
    Presenter presenter = new TeachersPresenter(rpcService, eventBus, new TeachersView());
    presenter.go(container);
  }

  /**
 * go.
 * @param container container.
 */
  public void go(final HasWidgets container) {
    this.container = container;

    Presenter presenter = new TeachersPresenter(rpcService, eventBus, new TeachersView());
    presenter.go(container);
  }

}
