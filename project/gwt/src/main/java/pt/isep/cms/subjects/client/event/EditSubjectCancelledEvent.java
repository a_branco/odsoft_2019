package pt.isep.cms.subjects.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class EditSubjectCancelledEvent extends GwtEvent<EditSubjectCancelledEventHandler> {
  public final static Type<EditSubjectCancelledEventHandler> TYPE =
       new Type<EditSubjectCancelledEventHandler>();

  @Override
  public Type<EditSubjectCancelledEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(EditSubjectCancelledEventHandler handler) {
    handler.onEditSubjectCancelled(this);
  }
}
