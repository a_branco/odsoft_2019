package pt.isep.cms.teachers.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class EditTeacherCancelledEvent extends GwtEvent<EditTeacherCancelledEventHandler> {
  public final static Type<EditTeacherCancelledEventHandler> TYPE =
      new Type<EditTeacherCancelledEventHandler>();

  @Override
  public Type<EditTeacherCancelledEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(EditTeacherCancelledEventHandler handler) {
    handler.onEditTeacherCancelled(this);
  }
}
