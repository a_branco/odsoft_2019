package pt.isep.cms.teachers.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.ArrayList;

import pt.isep.cms.teachers.shared.Teacher;
import pt.isep.cms.teachers.shared.TeacherDetails;

@RemoteServiceRelativePath("teachersService")
public interface TeachersService extends RemoteService {

  Teacher addTeacher(Teacher teacher);

  Boolean deleteTeacher(String id);

  ArrayList<TeacherDetails> deleteTeachers(ArrayList<String> ids);

  ArrayList<TeacherDetails> getTeacherDetails();

  Teacher getTeacher(String id);

  Teacher updateTeacher(Teacher teacher);
}
