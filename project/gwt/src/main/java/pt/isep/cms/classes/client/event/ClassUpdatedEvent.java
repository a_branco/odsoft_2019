package pt.isep.cms.classes.client.event;

import com.google.gwt.event.shared.GwtEvent;
import pt.isep.cms.classes.shared.Classe;

public class ClassUpdatedEvent extends GwtEvent<ClassUpdatedEventHandler> {
  public final static Type<ClassUpdatedEventHandler> TYPE = new Type<ClassUpdatedEventHandler>();
  private final Classe updatedClass;
  
  public ClassUpdatedEvent(Classe updatedClass) {
    this.updatedClass = updatedClass;
  }
  
  public Classe getUpdatedClass() {
    return updatedClass;
  }
  

  @Override
  public Type<ClassUpdatedEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(ClassUpdatedEventHandler handler) {
    handler.onClassUpdated(this);
  }
}
