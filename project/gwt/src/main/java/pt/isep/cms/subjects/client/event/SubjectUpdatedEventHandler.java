package pt.isep.cms.subjects.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface SubjectUpdatedEventHandler extends EventHandler {
  void onSubjectUpdated(SubjectUpdatedEvent event);
}
