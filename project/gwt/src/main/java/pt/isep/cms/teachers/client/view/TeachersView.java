package pt.isep.cms.teachers.client.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.ArrayList;
import java.util.List;

import pt.isep.cms.teachers.client.presenter.TeachersPresenter;

public class TeachersView extends Composite implements TeachersPresenter.Display {
  private final Button addButton;
  private final Button deleteButton;
  private FlexTable teachersTable;
  private final FlexTable contentTable;
  // private final VerticalPanel vPanel ;

  /**
 * Constructor for TeachersView Class.
 */
  public TeachersView() {
    DecoratorPanel contentTableDecorator = new DecoratorPanel();
    initWidget(contentTableDecorator);
    contentTableDecorator.setWidth("100%");
    contentTableDecorator.setWidth("18em");

    contentTable = new FlexTable();
    contentTable.setWidth("100%");
    contentTable.getCellFormatter().addStyleName(0, 0, "teachers-ListContainer");
    contentTable.getCellFormatter().setWidth(0, 0, "100%");
    contentTable.getFlexCellFormatter().setVerticalAlignment(0, 0, DockPanel.ALIGN_TOP);

    // vPanel = new VerticalPanel();
    // initWidget(vPanel);

    // Create the menu
    //
    HorizontalPanel hPanel = new HorizontalPanel();
    hPanel.setBorderWidth(0);
    hPanel.setSpacing(0);
    hPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
    addButton = new Button("Add");
    hPanel.add(addButton);

    deleteButton = new Button("Delete");
    hPanel.add(deleteButton);

    // vPanel.add(hPanel);

    contentTable.getCellFormatter().addStyleName(0, 0, "teachers-ListMenu");
    contentTable.setWidget(0, 0, hPanel);

    // Create the teachers list
    //
    teachersTable = new FlexTable();
    teachersTable.setCellSpacing(0);
    teachersTable.setCellPadding(0);
    teachersTable.setWidth("100%");
    teachersTable.addStyleName("teachers-ListContents");
    teachersTable.getColumnFormatter().setWidth(0, "15px");

    // vPanel.add(teachersTable);

    contentTable.setWidget(1, 0, teachersTable);

    contentTableDecorator.add(contentTable);
  }

  public HasClickHandlers getAddButton() {
    return addButton;
  }

  public HasClickHandlers getDeleteButton() {
    return deleteButton;
  }

  public HasClickHandlers getList() {
    return teachersTable;
  }

  /**
 * sets data.
 * @param data data to set.
 */
  public void setData(List<String> data) {
    teachersTable.removeAllRows();

    for (int i = 0; i < data.size(); ++i) {
      teachersTable.setWidget(i, 0, new CheckBox());
      teachersTable.setText(i, 1, data.get(i));
    }
  }

  /**
 * fetches number of clicked row.
 * @param event event.
 * @return number of row clicked.
 */
  public int getClickedRow(ClickEvent event) {
    int selectedRow = -1;
    HTMLTable.Cell cell = teachersTable.getCellForEvent(event);

    if (cell != null) {
      // Suppress clicks if the user is actually selecting the
      // check box
      //
      if (cell.getCellIndex() > 0) {
        selectedRow = cell.getRowIndex();
      }
    }

    return selectedRow;
  }

  /**
 * Fetches the numbers of all selected rows.
 * @return List with all number of the selected rows.
 */
  public List<Integer> getSelectedRows() {
    List<Integer> selectedRows = new ArrayList<Integer>();

    for (int i = 0; i < teachersTable.getRowCount(); ++i) {
      CheckBox checkBox = (CheckBox) teachersTable.getWidget(i, 0);
      if (checkBox.getValue()) {
        selectedRows.add(i);
      }
    }

    return selectedRows;
  }

  public Widget asWidget() {
    return this;
  }
}
