package pt.isep.cms.subjects.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;

@SuppressWarnings("serial")
public class SubjectDetails implements Serializable, IsSerializable {
  private String id;
  private String displayName;

  public SubjectDetails() {
    new SubjectDetails("0", "");
  }

  public SubjectDetails(String id, String displayName) {
    this.id = id;
    this.displayName = displayName;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
}
