package pt.isep.cms.students.client.view;
/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.Constants;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;

import java.util.HashMap;

import pt.isep.cms.client.ShowcaseConstants;
import pt.isep.cms.students.client.StudentsServiceAsync;
import pt.isep.cms.students.client.presenter.EditStudentPresenter;

/**
 * Dialog Box for Adding and Updating Students.
 */
public class StudentsDialog implements EditStudentPresenter.Display {

  private final StudentsServiceAsync rpcService;

  public enum Type {
    ADD,
    UPDATE
  }

  /**
   * The constants used in this Content Widget.
   */
  public static interface CwConstants extends Constants {

    String cwAddStudentDialogCaption();

    String cwUpdateStudentDialogCaption();

    //String cwDialogBoxClose();
    //
    //String cwDialogBoxDescription();
    //
    //String cwDialogBoxDetails();
    //
    //String cwDialogBoxItem();
    //
    //String cwDialogBoxListBoxInfo();
    //
    //String cwDialogBoxMakeTransparent();
    //
    //String cwDialogBoxName();
    //
    //String cwDialogBoxShowButton();
  }

  /**
   * An instance of the constants.
   */
  private final CwConstants constants;
  private final ShowcaseConstants globalConstants;

  // Widgets
  private final TextBox firstName;
  private final TextBox lastName;
  private final DateBox birthday;
  private final TextBox gender;
  private final TextBox email;
  private final ListBox classe;
  private final FlexTable detailsTable;
  private final Button saveButton;
  private final Button cancelButton;

  private void initDetailsTable() {
    detailsTable.setWidget(0, 0, new Label("Firstname"));
    detailsTable.setWidget(0, 1, firstName);
    detailsTable.setWidget(1, 0, new Label("Lastname"));
    detailsTable.setWidget(1, 1, lastName);
    detailsTable.setWidget(2, 0, new Label("Gender"));
    detailsTable.setWidget(2, 1, gender);
    detailsTable.setWidget(3, 0, new Label("Birthday"));
    detailsTable.setWidget(3, 1, birthday);
    detailsTable.setWidget(4, 0, new Label("E-mail"));
    detailsTable.setWidget(4, 1, email);
    detailsTable.setWidget(5, 0, new Label("Class"));
    detailsTable.setWidget(5, 1, classe);
    firstName.setFocus(true);
  }

  DecoratorPanel contentDetailsDecorator;
  final DialogBox dialogBox;

  /**
   * Constructor.
   *
   * @param constants the constants
   * @param rpcService rpcService
   * @param type type
   */
  public StudentsDialog(StudentsServiceAsync rpcService, ShowcaseConstants constants, Type type) {
    // super(constants.cwDialogBoxName(), constants.cwDialogBoxDescription());

    this.rpcService = rpcService;
    this.constants = constants;
    this.globalConstants = constants;

    // Init the widgets of the dialog
    contentDetailsDecorator = new DecoratorPanel();
    contentDetailsDecorator.setWidth("30em"); // em = size of current font
    // initWidget(contentDetailsDecorator);

    VerticalPanel contentDetailsPanel = new VerticalPanel();
    contentDetailsPanel.setWidth("100%");

    // Create the students list
    //
    detailsTable = new FlexTable();
    detailsTable.setCellSpacing(0);
    detailsTable.setWidth("100%");
    detailsTable.addStyleName("students-ListContainer");
    detailsTable.getColumnFormatter().addStyleName(1, "add-student-input");
    firstName = new TextBox();
    lastName = new TextBox();
    gender = new TextBox();
    email = new TextBox();
    birthday = new DateBox();
    classe = new ListBox();
    
    rpcService.getClassesDropDown(new AsyncCallback<HashMap<String, String>>() {

      public void onSuccess(HashMap<String, String> result) {
        for (String val : result.values()) {
          classe.addItem(val);
        }
      }

      public void onFailure(Throwable caught) {
        //Window.alert("Error retrieving student");
      }
    });

    initDetailsTable();
    contentDetailsPanel.add(detailsTable);

    HorizontalPanel menuPanel = new HorizontalPanel();
    saveButton = new Button("Save");
    cancelButton = new Button("Cancel");
    menuPanel.add(saveButton);
    menuPanel.add(cancelButton);
    contentDetailsPanel.add(menuPanel);
    contentDetailsDecorator.add(contentDetailsPanel);

    dialogBox = new DialogBox();
    dialogBox.ensureDebugId("cwDialogBox");
    if (type == Type.ADD) {
      dialogBox.setText(constants.cwAddStudentDialogCaption());
    } else {
      dialogBox.setText(constants.cwUpdateStudentDialogCaption());
    }

    dialogBox.add(contentDetailsDecorator);

    dialogBox.setGlassEnabled(true);
    dialogBox.setAnimationEnabled(true);
  }

  /**
 * shows dialog box.
 */
  public void displayDialog() {
    // Create the dialog box
    // final DialogBox dialogBox = createDialogBox();

    dialogBox.center();
    dialogBox.show();
  }

  @Override
  public HasClickHandlers getSaveButton() {
    // TODO Auto-generated method stub
    return saveButton;
    // return null;
  }

  @Override
  public HasClickHandlers getCancelButton() {
    // TODO Auto-generated method stub
    return cancelButton;
    // return null;
  }

  @Override
  public HasValue<String> getFirstName() {
    // TODO Auto-generated method stub
    return firstName;
    // return null;
  }

  @Override
  public HasValue<String> getLastName() {
    // TODO Auto-generated method stub
    return lastName;
    // return null;
  }

  @Override
  public DateBox getBirthday() {
    // TODO Auto-generated method stub
    return birthday;
    // return null;
  }

  /**
 * Returns Classes in a list box.
 * @return ListBox with Classes
 */
  @Override
  public String getStringClasse() {
    // TODO Auto-generated method stub
    int indexSelected = classe.getSelectedIndex();
	String classeSelected = classe.getItemText(indexSelected);	
	return classeSelected;
  }

  /**
 * gets email.
 * @return email value
 */
  @Override
  public HasValue<String> getEmail() {
    // TODO Auto-generated method stub
    return email;
    // return null;
  }

  @Override
  public HasValue<String> getGender() {
    // TODO Auto-generated method stub
    return gender;
    // return null;
  }

  @Override
  public void show() {
    // TODO Auto-generated method stub
    // return null;
    displayDialog();
  }

  @Override
  public void hide() {
    // TODO Auto-generated method stub
    // return null;
    dialogBox.hide();
  }

}
