package pt.isep.cms.classes.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.ArrayList;

import pt.isep.cms.classes.shared.ClassDetails;
import pt.isep.cms.classes.shared.Classe;

@RemoteServiceRelativePath("classesService")
public interface ClassesService extends RemoteService {

  Classe addClass(Classe classe);

  Boolean deleteClass(String id);

  ArrayList<ClassDetails> deleteClasses(ArrayList<String> ids);

  ArrayList<ClassDetails> getClassDetails();

  Classe getClass(String id);

  Classe updateClass(Classe classe);
}
