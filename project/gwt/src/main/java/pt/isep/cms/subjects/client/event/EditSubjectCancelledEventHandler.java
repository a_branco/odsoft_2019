package pt.isep.cms.subjects.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditSubjectCancelledEventHandler extends EventHandler {
  void onEditSubjectCancelled(EditSubjectCancelledEvent event);
}
