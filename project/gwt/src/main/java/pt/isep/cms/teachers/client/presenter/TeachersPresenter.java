package pt.isep.cms.teachers.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;

import java.util.ArrayList;
import java.util.List;

import pt.isep.cms.teachers.client.TeachersServiceAsync;
import pt.isep.cms.teachers.client.event.AddTeacherEvent;
import pt.isep.cms.teachers.client.event.EditTeacherEvent;
import pt.isep.cms.teachers.shared.TeacherDetails;

public class TeachersPresenter implements Presenter {

  private List<TeacherDetails> teacherDetails;

  public interface Display {
    HasClickHandlers getAddButton();

    HasClickHandlers getDeleteButton();

    HasClickHandlers getList();

    void setData(List<String> data);

    int getClickedRow(ClickEvent event);

    List<Integer> getSelectedRows();

    Widget asWidget();
  }

  private final TeachersServiceAsync rpcService;
  private final HandlerManager eventBus;
  private final Display display;

  /**
 * Constructor for TeachersPresenter Class.
 * @param rpcService service.
 * @param eventBus event.
 * @param view view.
 */
  public TeachersPresenter(TeachersServiceAsync rpcService,
        HandlerManager eventBus, Display view) {
    this.rpcService = rpcService;
    this.eventBus = eventBus;
    this.display = view;
  }

  /**
 * bind.
 */
  public void bind() {
    display.getAddButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        eventBus.fireEvent(new AddTeacherEvent());
      }
    });

    display.getDeleteButton().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        deleteSelectedTeachers();
      }
    });

    display.getList().addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        int selectedRow = display.getClickedRow(event);

        if (selectedRow >= 0) {
          String id = teacherDetails.get(selectedRow).getId();
          eventBus.fireEvent(new EditTeacherEvent(id));
        }
      }
    });
  }

  /**
 * go.
 * @param container container.
 */
  public void go(final HasWidgets container) {
    bind();
    container.clear();
    container.add(display.asWidget());

    fetchTeacherDetails();
  }

  /**
 * sorts the list of Teachers Details.
 */
  public void sortTeacherDetails() {

    // Yes, we could use a more optimized method of sorting, but the
    // point is to create a test case that helps illustrate the higher
    // level concepts used when creating MVP-based applications.
    //
    for (int i = 0; i < teacherDetails.size(); ++i) {
      for (int j = 0; j < teacherDetails.size() - 1; ++j) {
        if (teacherDetails.get(j).getDisplayName()
            .compareToIgnoreCase(teacherDetails.get(j + 1).getDisplayName()) >= 0) {
          TeacherDetails tmp = teacherDetails.get(j);
          teacherDetails.set(j, teacherDetails.get(j + 1));
          teacherDetails.set(j + 1, tmp);
        }
      }
    }
  }

  public void setTeacherDetails(List<TeacherDetails> teacherDetails) {
    this.teacherDetails = teacherDetails;
  }

  public TeacherDetails getTeacherDetail(int index) {
    return teacherDetails.get(index);
  }

  private void fetchTeacherDetails() {
    rpcService.getTeacherDetails(new AsyncCallback<ArrayList<TeacherDetails>>() {
      public void onSuccess(ArrayList<TeacherDetails> result) {
        teacherDetails = result;
        sortTeacherDetails();
        List<String> data = new ArrayList<String>();

        for (int i = 0; i < result.size(); ++i) {
          data.add(teacherDetails.get(i).getDisplayName());
        }

        display.setData(data);
      }

      public void onFailure(Throwable caught) {
        Window.alert(caught.getMessage());
        Window.alert("Error fetching teacher details");
      }
    });
  }

  private void deleteSelectedTeachers() {
    List<Integer> selectedRows = display.getSelectedRows();
    ArrayList<String> ids = new ArrayList<String>();

    for (int i = 0; i < selectedRows.size(); ++i) {
      ids.add(teacherDetails.get(selectedRows.get(i)).getId());
    }

    rpcService.deleteTeachers(ids, new AsyncCallback<ArrayList<TeacherDetails>>() {
      public void onSuccess(ArrayList<TeacherDetails> result) {
        teacherDetails = result;
        sortTeacherDetails();
        List<String> data = new ArrayList<String>();

        for (int i = 0; i < result.size(); ++i) {
          data.add(teacherDetails.get(i).getDisplayName());
        }

        display.setData(data);

      }

      public void onFailure(Throwable caught) {
        Window.alert("Error deleting selected teachers");
      }
    });
  }
}
