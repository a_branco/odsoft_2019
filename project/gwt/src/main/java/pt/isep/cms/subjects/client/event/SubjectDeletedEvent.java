package pt.isep.cms.subjects.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class SubjectDeletedEvent extends GwtEvent<SubjectDeletedEventHandler> {
  public final static Type<SubjectDeletedEventHandler> TYPE =
      new Type<SubjectDeletedEventHandler>();

  @Override
  public Type<SubjectDeletedEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(SubjectDeletedEventHandler handler) {
    handler.onSubjectDeleted(this);
  }
}
