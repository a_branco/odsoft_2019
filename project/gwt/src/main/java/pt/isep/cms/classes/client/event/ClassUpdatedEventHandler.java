package pt.isep.cms.classes.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ClassUpdatedEventHandler extends EventHandler {
  void onClassUpdated(ClassUpdatedEvent event);
}
