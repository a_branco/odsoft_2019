package pt.isep.cms.classes.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditClassEventHandler extends EventHandler {
  void onEditClass(EditClassEvent event);
}
