package pt.isep.cms.classes.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class AddClassEvent extends GwtEvent<AddClassEventHandler> {
  public final static Type<AddClassEventHandler> TYPE = new Type<AddClassEventHandler>();
  
  @Override
  public Type<AddClassEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(AddClassEventHandler handler) {
    handler.onAddClass(this);
  }
}
