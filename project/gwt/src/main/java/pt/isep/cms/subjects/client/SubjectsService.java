package pt.isep.cms.subjects.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.ArrayList;
import java.util.HashMap;

import pt.isep.cms.subjects.shared.Subject;
import pt.isep.cms.subjects.shared.SubjectDetails;

@RemoteServiceRelativePath("subjectsService")
public interface SubjectsService extends RemoteService {

  Subject addSubject(Subject subject);

  Boolean deleteSubject(String id);

  ArrayList<SubjectDetails> deleteSubjects(ArrayList<String> ids);

  ArrayList<SubjectDetails> getSubjectDetails();

  HashMap<String, String> getTeachersDropDown();

  Subject getSubject(String id);

  Subject updateSubject(Subject subject);
}
