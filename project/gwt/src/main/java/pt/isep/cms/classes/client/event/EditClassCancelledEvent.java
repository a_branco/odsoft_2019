package pt.isep.cms.classes.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class EditClassCancelledEvent extends GwtEvent<EditClassCancelledEventHandler> {
  public final static Type<EditClassCancelledEventHandler> TYPE =
      new Type<EditClassCancelledEventHandler>();

  @Override
  public Type<EditClassCancelledEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(EditClassCancelledEventHandler handler) {
    handler.onEditClassCancelled(this);
  }
}
