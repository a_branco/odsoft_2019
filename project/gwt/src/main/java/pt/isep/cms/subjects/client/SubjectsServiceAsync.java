package pt.isep.cms.subjects.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;
import java.util.HashMap;

import pt.isep.cms.subjects.shared.Subject;
import pt.isep.cms.subjects.shared.SubjectDetails;

public interface SubjectsServiceAsync {

  public void addSubject(Subject subject, AsyncCallback<Subject> callback);

  public void deleteSubject(String id, AsyncCallback<Boolean> callback);

  public void deleteSubjects(ArrayList<String> ids,
      AsyncCallback<ArrayList<SubjectDetails>> callback);

  public void getSubjectDetails(AsyncCallback<ArrayList<SubjectDetails>> callback);

  public void getSubject(String id, AsyncCallback<Subject> callback);

  public void updateSubject(Subject subject, AsyncCallback<Subject> callback);

  public void getTeachersDropDown(AsyncCallback<HashMap<String, String>> callback);
}

