package pt.isep.cms.classes.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;

@SuppressWarnings("serial")
public class ClassDetails implements Serializable, IsSerializable {
  private String id;
  private String displayName;

  public ClassDetails() {
    new ClassDetails("0", "");
  }


  public ClassDetails(String id, String displayName) {
    this.id = id;
    this.displayName = displayName;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
}
