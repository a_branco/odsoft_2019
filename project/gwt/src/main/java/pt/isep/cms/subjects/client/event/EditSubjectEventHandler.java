package pt.isep.cms.subjects.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditSubjectEventHandler extends EventHandler {
  void onEditSubject(EditSubjectEvent event);
}
