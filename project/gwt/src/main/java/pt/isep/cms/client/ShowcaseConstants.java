package pt.isep.cms.client;
/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

import pt.isep.cms.classes.client.ClassesController;
import pt.isep.cms.classes.client.CwClasses;
import pt.isep.cms.classes.client.view.ClassesDialog;

import pt.isep.cms.client.MainMenuTreeViewModel.MenuConstants;

import pt.isep.cms.contacts.client.ContactsController;
import pt.isep.cms.contacts.client.CwContacts;
import pt.isep.cms.contacts.client.view.ContactsDialog;

import pt.isep.cms.students.client.CwStudents;
import pt.isep.cms.students.client.StudentsController;
import pt.isep.cms.students.client.view.StudentsDialog;

import pt.isep.cms.subjects.client.CwSubjects;
import pt.isep.cms.subjects.client.SubjectsController;
import pt.isep.cms.subjects.client.view.SubjectsDialog;

import pt.isep.cms.teachers.client.CwTeachers;
import pt.isep.cms.teachers.client.TeachersController;
import pt.isep.cms.teachers.client.view.TeachersDialog;

/**
 * Constants used throughout the showcase.
 */
public interface ShowcaseConstants extends MenuConstants,
    CwContacts.CwConstants, CwTeachers.CwConstants, CwStudents.CwConstants,
    CwClasses.CwConstants, CwSubjects.CwConstants, ContactsDialog.CwConstants,
    TeachersDialog.CwConstants, StudentsDialog.CwConstants,
    ClassesDialog.CwConstants, SubjectsDialog.CwConstants,
    ContactsController.CwConstants, TeachersController.CwConstants,
    StudentsController.CwConstants, ClassesController.CwConstants, SubjectsController.CwConstants {

  /**
   * The path to source code for examples, raw files, and style definitions.
   */
  //String DST_SOURCE = "gwtShowcaseSource/";

  /**
   * The destination folder for parsed source code from Showcase examples.
   */
  //String DST_SOURCE_EXAMPLE = DST_SOURCE + "java/";

  /**
   * The destination folder for raw files that are included in entirety.
   */
  //String DST_SOURCE_RAW = DST_SOURCE + "raw/";

  /**
   * The destination folder for parsed CSS styles used in Showcase examples.
   */
  //String DST_SOURCE_STYLE = DST_SOURCE + "css/";
}
