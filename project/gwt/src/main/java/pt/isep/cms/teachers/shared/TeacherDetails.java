package pt.isep.cms.teachers.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;

@SuppressWarnings("serial")
public class TeacherDetails implements Serializable, IsSerializable {
  private String id;
  private String displayName;

  public TeacherDetails() {
    new TeacherDetails("0", "");
  }

  public TeacherDetails(String id, String displayName) {
    this.id = id;
    this.displayName = displayName;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
}
