package pt.isep.cms.classes.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.io.Serializable;
import java.util.Objects;


@SuppressWarnings("serial")
public class Classe implements Serializable, IsSerializable {
  public String id;
  public String designation;
  public String name;


  /**
  * Constructor for Classe classe.
  * @param id id.
  * @param designation designation.
  * @param name name.
  */
  public Classe(String id, String designation, String name) {
    this.id = id;
    this.designation = designation;
    this.name = name;
  }

  public Classe() {
  }

  public ClassDetails getLightWeightClass() {
    return new ClassDetails(id, getName());
    //return "asd";
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDesignation() {
    return designation;
  }

  public void setDesignation(String designation) {
    this.designation = designation;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  //public String getFullName() { return firstName + " " + lastName; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Classe classe = (Classe) o;
    return id.equals(classe.id)
      && designation.equals(classe.designation)
      && name.equals(classe.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, designation, name);
  }
}