package pt.isep.cms.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import pt.isep.cms.classes.shared.Classe;
import pt.isep.cms.students.shared.Student;
import pt.isep.cms.subjects.shared.Subject;
import pt.isep.cms.teachers.shared.Teacher;

@SuppressWarnings("serial")
public class DatabaseConnection {

  /**
   * Constructor for DatabaseConnection Class.
   *
   * @return Connection
   */
  // This method establishes the connection to the database.
  public Connection connectDatabase() {

    Connection conn = null;

    try {
      conn = DriverManager.getConnection("jdbc:mysql://172.18.155.9/cms_odsoft",
       "root", "root"); // Address, user,

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Successful connection established!");
    }
    return conn;
  }

  /**
   * return last teacher id in the database.
   *
   * @return String
   */
  public String lastTeacherID() {

    Connection conn = connectDatabase(); // Connect to database
    String lastIdTeacher = "";

    try {
      String query = "SELECT idTeacher FROM teacher ORDER BY idTeacher DESC LIMIT 1";

      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(query);

      // Iterate through the java ResultSet
      while (rs.next()) {
        try {
          lastIdTeacher = rs.getString("idTeacher");
        } catch (Exception e) {
          System.out.println(e);
        }
      }

      st.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get last teacher id with success!");
    }

    return lastIdTeacher;
  }

  /**
   * updates teacher in the database.
   *
   * @param id        id.
   * @param firstName firstName.
   * @param lastName  lastName.
   * @param gender    gender.
   * @param birthday  birthday.
   */
  public void updateTeacher(int id, String firstName, String lastName,
      String gender, Date birthday) {

    Connection conn = connectDatabase(); // Connect to database

    try {
      String query = "update teacher set firstName = ?, lastName = ?, "
          + "gender = ?, birthDate = ? where idTeacher = ?";
      PreparedStatement preparedStmt = conn.prepareStatement(query);
      preparedStmt.setString(1, firstName);
      preparedStmt.setString(2, lastName);
      preparedStmt.setString(3, gender);

      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
      String strDate = formatter.format(birthday);

      preparedStmt.setString(4, strDate);
      preparedStmt.setInt(5, id);

      preparedStmt.executeUpdate();

      preparedStmt.close();
    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Teacher updated!");
    }
  }

  /**
   * deletes teacher in the database.
   *
   * @param id id of teacher.
   */
  public void deleteTeacher(String id) {

    int idTeacher = Integer.parseInt(id); // Convert string id to integer

    Connection conn = connectDatabase(); // Connect to database

    try {
      String query = "delete from teacher where idTeacher = ?";
      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setInt(1, idTeacher);

      preparedStmt.execute();

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Teacher deleted!");
    }
  }

  /**
   * adds teacher to database.
   *
   * @param firstName firstName.
   * @param lastName  lastName.
   * @param gender    gender.
   * @param birthday  birthday.
   */
  public void addTeacher(String firstName, String lastName, String gender, Date birthday) {

    Connection conn = connectDatabase(); // Connect to database

    try {
      String query = " insert into teacher (firstName, lastName, " + "gender, birthDate)"
          + " values (?, ?, ?, ?)";

      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setString(1, firstName);
      preparedStmt.setString(2, lastName);
      preparedStmt.setString(3, gender);

      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
      String strDate = formatter.format(birthday);
      preparedStmt.setString(4, strDate);

      preparedStmt.execute();

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Teacher added!");
    }
  }

  /**
   * Fetches teachers from the database.
   *
   * @return Hashmap with the teachers.
   */
  public HashMap<String, Teacher> getTeachers() {

    HashMap<String, Teacher> teachers = new HashMap<String, Teacher>();
    Connection conn = connectDatabase(); // Connect to database

    try {
      String query = "SELECT * FROM teacher";

      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(query);

      // Iterate through the java ResultSet
      while (rs.next()) {

        try {
          // Handle date
          String dateString = rs.getString("birthDate");
          SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
          Date date = formatter.parse(dateString);
          String id = rs.getString("idTeacher");
          String firstName = rs.getString("firstName");
          String lastName = rs.getString("lastName");
          String gender = rs.getString("gender");

          // Each query result corresponds to a teacher type object
          Teacher teacher = new Teacher(id, firstName, lastName, date, gender);
          teacher.setBirthday(date);
          teachers.put(teacher.getId(), teacher);
        } catch (Exception e) {
          System.out.println(e);
        }
      }

      st.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get Teachers!");
    }

    return teachers;

  }

  /**
   * Fetches classes from the database.
   *
   * @return Hashmap with the classes.
   */
  public HashMap<String, Classe> getClasses() {

    HashMap<String, Classe> classes = new HashMap<String, Classe>();
    Connection conn = connectDatabase(); // Connect to database

    try {
      String query = "SELECT * FROM class";

      Statement st = conn.createStatement();

      ResultSet rs = st.executeQuery(query);

      while (rs.next()) {

        try {

          String id = rs.getString("idClass");
          String designation = rs.getString("designation");
          String name = rs.getString("name");

          Classe classe = new Classe(id, designation, name);
          classes.put(classe.getId(), classe);
        } catch (Exception e) {
          System.out.println(e);
        }
      }

      st.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get all classes!");
    }

    return classes;

  }

  /**
   * return last class id in the database.
   *
   * @return String
   */
  public String lastClassID() {

    Connection conn = connectDatabase(); // Connect to database
    String lastClassID = "";

    try {
      String query = "SELECT idClass FROM class ORDER BY idClass DESC LIMIT 1";

      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(query);

      // Iterate through the java ResultSet
      while (rs.next()) {
        try {
          lastClassID = rs.getString("idClass");
        } catch (Exception e) {
          System.out.println(e);
        }
      }

      st.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get last class id with success!");
    }

    return lastClassID;
  }


  /**
   * deletes Class in the database.
   *
   * @param id id.
   */
  public void deleteClass(String id) {

    int idClass = Integer.parseInt(id); // Convert string id to integer

    Connection conn = connectDatabase(); // Connect to database

    try {
      // MySQL Delete Statement
      String query = "delete from class where idClass = ?";
      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setInt(1, idClass);

      // Execute the PreparedStatement
      preparedStmt.execute();

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Class deleted!");
    }
  }

  /**
   * adds class to the database table.
   *
   * @param designation designation.
   * @param name        name.
   */
  public void addClass(String designation, String name) {

    Connection conn = connectDatabase(); // Connect to database

    try {
      String query = " insert into class (designation, name)" + " values (?, ?)";

      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setString(1, designation);
      preparedStmt.setString(2, name);

      preparedStmt.execute();

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Class added!");
    }
  }

  /**
   * updates class in the database.
   *
   * @param id          id.
   * @param designation designation.
   * @param name        name.
   */
  public void updateClass(int id, String designation, String name) {
    Connection conn = connectDatabase(); // Connect to database

    try {
      // Create the java MySQL Update PreparedStatement
      String query = "update class set designation = ?, name = ? where idClass = ?";
      PreparedStatement preparedStmt = conn.prepareStatement(query);
      preparedStmt.setString(1, designation);
      preparedStmt.setString(2, name);
      preparedStmt.setInt(3, id);

      // execute the java preparedstatement
      preparedStmt.executeUpdate();

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Class updated!");
    }
  }

  /**
   * gets all classes info in a string.
   *
   * @return get hashmap with all classes info.
   */
  public HashMap<String, String> getClassesDropDown() {

    HashMap<String, String> classes = new HashMap<String, String>();

    Connection conn = connectDatabase(); // Connect to database

    try {

      String query = "SELECT * FROM class";

      Statement st = conn.createStatement();

      ResultSet rs = st.executeQuery(query);

      while (rs.next()) {
        try {
          String id = rs.getString("idClass");
          String name = rs.getString("name");
          String designation = rs.getString("designation");
          classes.put(id, designation);
        } catch (Exception e) {
          System.out.println(e);
        }
      }

      st.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get all classes!");
    }

    return classes;

  }

  /**
 * Fetches Id of Class object.
 * @param classe classe to get id from
 * @return desired Id.
 */
  public int getIdClass(String classe) {
    Connection conn = connectDatabase(); // Connect to database
    int idClass = 0;
    try {
      String query = "SELECT idClass FROM class WHERE designation LIKE ?";

      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setString(1, "%" + classe + "%");

      ResultSet rs = preparedStmt.executeQuery();

      while (rs.next()) {
        try {
          idClass = rs.getInt("idClass");

          System.out.println(idClass);

        } catch (Exception e) {
          System.out.println(e);
        }
      }

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get Class ID!");
    }

    return idClass;
  }


  /**
   * Adds Student to Database.
   *
   * @param firstName firstName.
   * @param lastName  lastName.
   * @param gender    gender.
   * @param birthday  birthday.
   * @param email     email.
   */
  public void addStudent(String firstName, String lastName,
      String gender, Date birthday, String email, String classe) {


    Connection conn = connectDatabase(); // Connect to database

    System.out.println("= " + classe);
    int idClasse = getIdClass(classe);

    System.out.println("??? " + idClasse);

    try {

      if (numberStudensPerClass(idClasse) < 20) {
        try {
          String query = " insert into student (firstName, lastName, "
              + "gender, birthDate, email, class_idClass)"
              + " values (?, ?, ?, ?, ?, ?)";

          PreparedStatement preparedStmt = conn.prepareStatement(query);

          preparedStmt.setString(1, firstName);
          preparedStmt.setString(2, lastName);
          preparedStmt.setString(3, gender);

          SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
          String strDate = formatter.format(birthday);
          preparedStmt.setString(4, strDate);

          preparedStmt.setString(5, email);
          preparedStmt.setInt(6, idClasse);

          preparedStmt.execute();

        } catch (SQLException ex) {
          // Handle any errors
          System.out.println("SQLException: " + ex.getMessage());
          System.out.println("SQLState: " + ex.getSQLState());
          System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
          System.out.println("Statement Done!");
        }
      } else {
        throw new Throwable("Error!");
      }

    } catch (Throwable t) {
      System.out.println(t.getMessage());
    } finally {
      System.out.println("Student added!");
    }

  }


  /**
   * deletes student in the database.
   *
   * @param id id.
   */
  public void deleteStudent(String id) {

    int idStudent = Integer.parseInt(id); // Convert string id to integer

    Connection conn = connectDatabase(); // Connect to database

    try {
      // MySQL Delete Statement
      String query = "DELETE FROM student where idStudent = ?";
      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setInt(1, idStudent);

      // Execute the PreparedStatement
      preparedStmt.execute();

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Student deleted!");
    }
  }


  /**
   * return last student id in the database.
   *
   * @return String
   */
  public String lastStudentId() {

    Connection conn = connectDatabase(); // Connect to database
    String lastIdStudent = "";

    try {
      String query = "SELECT idStudent FROM student ORDER BY idStudent DESC LIMIT 1";

      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(query);

      // Iterate through the java ResultSet
      while (rs.next()) {
        try {
          lastIdStudent = rs.getString("idStudent");
        } catch (Exception e) {
          System.out.println(e);
        }
      }

      st.close();
    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get last student id!");
    }

    return lastIdStudent;
  }

  /**
   * return number students per class.
   *
   * @return int
   */
  public int numberStudensPerClass(int idClasse) {

    Connection conn = connectDatabase(); // Connect to database
    int number = 0;

    try {

      String query = "SELECT COUNT(idStudent) AS countStudent "
          + "FROM student WHERE class_idClass LIKE ?";

      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setString(1, "%" + idClasse + "%");

      ResultSet rs = preparedStmt.executeQuery();

      while (rs.next()) {
        try {
          number = rs.getInt("countStudent");

          System.out.println(number);

        } catch (Exception e) {
          System.out.println(e);
        }
      }

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Number students per class!");
    }

    return number;
  }


  /**
   * updates student in the database.
   *
   * @param idStudent idStudent
   * @param firstName firstName
   * @param lastName  lastName
   * @param gender    gender
   * @param birthday  birthday
   * @param email     email
   * @param classe    classe
   */
  public void updateStudent(int idStudent, String firstName, String lastName,
      String gender, Date birthday, String email, String classe) {
    Connection conn = connectDatabase(); // Connect to database

    try {
      // Create the java MySQL Update PreparedStatement
      String query = "UPDATE student set firstName = ?, lastName = ?, gender = ?, "
          + "birthDate = ?, email = ?, class_idClass = ? where idStudent = ?";

      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setString(1, firstName);
      preparedStmt.setString(2, lastName);
      preparedStmt.setString(3, gender);

      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
      String strDate = formatter.format(birthday);

      preparedStmt.setString(4, strDate);

      preparedStmt.setString(5, email);

      int idClass = getIdClass(classe);
      preparedStmt.setInt(6, idClass);

      preparedStmt.setInt(7, idStudent);

      // execute the java preparedstatement
      preparedStmt.executeUpdate();

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Student updated!");
    }
  }


  /**
   * Fetches all students from database.
   *
   * @return Hashmap with all students
   */
  public HashMap<String, Student> getStudents() {

    HashMap<String, Student> students = new HashMap<String, Student>();
    Connection conn = connectDatabase(); // Connect to database

    try {
      String query = "SELECT * FROM student";
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(query);

      while (rs.next()) {

        try {
          // Handle date
          String dateString = rs.getString("birthDate");
          SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
          Date date = formatter.parse(dateString);
          String id = rs.getString("idStudent");
          String firstName = rs.getString("firstName");
          String lastName = rs.getString("lastName");
          String gender = rs.getString("gender");
          String email = rs.getString("email");
          String idClass = rs.getString("class_idClass");

          // Each query result corresponds to a student type object
          Student student = new Student(id, firstName, lastName, date, gender, email, idClass);
          student.setBirthday(date);
          students.put(student.getId(), student);
        } catch (Exception e) {
          System.out.println(e);
        }
      }

      st.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get all students!");
    }

    return students;

  }

  /**
   * Fetches all subjects from database.
   *
   * @return Hashmap with all subjects
   */
  public HashMap<String, Subject> getSubjects() {

    HashMap<String, Subject> subjects = new HashMap<String, Subject>();
    Connection conn = connectDatabase(); // Connect to database

    try {
      String query = "SELECT * FROM subject";
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(query);

      while (rs.next()) {

        try {
          String idSubject = rs.getString("idsubject");
          String name = rs.getString("name");
          String designation = rs.getString("designation");
          String infos = rs.getString("infos");
          String idTeacher = rs.getString("teacher_idTeacher");

          // Each query result corresponds to a subject type object
          Subject subject = new Subject(idSubject, name, designation, infos, idTeacher);

          subjects.put(idSubject, subject);
        } catch (Exception e) {
          System.out.println(e);
        }
      }

      st.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get all subjects!");
    }

    return subjects;

  }


  /**
   * gets all teachers info in a string.
   *
   * @return get hashmap with all teachers info.
   */
  public HashMap<String, String> getTeachersDropDown() {

    HashMap<String, String> teachers = new HashMap<String, String>();

    Connection conn = connectDatabase(); // Connect to database

    try {

      String query = "SELECT * FROM teacher";

      Statement st = conn.createStatement();

      ResultSet rs = st.executeQuery(query);

      while (rs.next()) {
        try {
          String idTeacher = rs.getString("idTeacher");
          String name = rs.getString("firstName");
          teachers.put(idTeacher, name);
        } catch (Exception e) {
          System.out.println(e);
        }
      }

      st.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get all teachers!");
    }

    return teachers;

  }

  /**
 * Fetches Id from a teacher name.
 * @param name name of teacher.
 * @return Desired Id.
 */
  public int getIdTeacher(String name) {
    Connection conn = connectDatabase(); // Connect to database
    int idTeacher = 0;
    try {
      String query = "SELECT idTeacher FROM teacher WHERE firstName LIKE ?";

      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setString(1, "%" + name + "%");

      ResultSet rs = preparedStmt.executeQuery();

      while (rs.next()) {
        try {
          idTeacher = rs.getInt("idTeacher");

          System.out.println(idTeacher);

        } catch (Exception e) {
          System.out.println(e);
        }
      }

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get id teacher!");
    }

    return idTeacher;
  }


  /**
 * Adds Subject.
 * @param name name.
 * @param designation designation.
 * @param infos infos.
 * @param teacher teacher.
 */
  public void addSubject(String name, String designation, String infos, String teacher) {

    Connection conn = connectDatabase(); // Connect to database

    int idTeacher = getIdTeacher(teacher);

    try {
      String query = "INSERT INTO subject (name, designation, "
          + "infos, teacher_idTeacher) values (?, ?, ?, ?)";

      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setString(1, name);
      preparedStmt.setString(2, designation);
      preparedStmt.setString(3, infos);
      preparedStmt.setInt(4, idTeacher);

      preparedStmt.execute();

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Subject added!");
    }
  }

  /**
   * updates subject in the database.
   *
   * @param idSubject   idSubject
   * @param designation designation
   * @param name        name
   * @param infos       infos
   * @param teacher     teacher
   */
  public void updateSubject(int idSubject, String name, String designation,
      String infos, String teacher) {
    Connection conn = connectDatabase(); // Connect to database

    try {
      // Create the java MySQL Update PreparedStatement
      String query = "UPDATE subject set name = ?, designation = ?, "
          + "infos = ?, teacher_idTeacher = ? where idsubject = ?";

      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setString(1, name);
      preparedStmt.setString(2, designation);
      preparedStmt.setString(3, infos);

      int idTeacher = getIdTeacher(teacher);
      preparedStmt.setInt(4, idTeacher);

      preparedStmt.setInt(5, idSubject);

      // execute the java preparedstatement
      preparedStmt.executeUpdate();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Subject updated!");
    }
  }


  /**
   * return last subject id in the database.
   *
   * @return String
   */
  public String lastSubjectId() {

    Connection conn = connectDatabase(); // Connect to database
    String lastIdSubject = "";

    try {
      String query = "SELECT idsubject FROM subject ORDER BY idsubject DESC LIMIT 1";

      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(query);

      // Iterate through the java ResultSet
      while (rs.next()) {
        try {
          lastIdSubject = rs.getString("idsubject");
        } catch (Exception e) {
          System.out.println(e);
        }
      }

      st.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Get last subject id!");
    }

    return lastIdSubject;
  }


  /**
   * deletes subject in the database.
   *
   * @param id id.
   */
  public void deleteSubject(String id) {

    int idSubject = Integer.parseInt(id); // Convert string id to integer

    Connection conn = connectDatabase(); // Connect to database

    try {
      // MySQL Delete Statement
      String query = "DELETE FROM subject where idsubject = ?";
      PreparedStatement preparedStmt = conn.prepareStatement(query);

      preparedStmt.setInt(1, idSubject);

      // Execute the PreparedStatement
      preparedStmt.execute();

      preparedStmt.close();

    } catch (SQLException ex) {
      // Handle any errors
      System.out.println("SQLException: " + ex.getMessage());
      System.out.println("SQLState: " + ex.getSQLState());
      System.out.println("VendorError: " + ex.getErrorCode());
    } finally {
      System.out.println("Subject deleted!");
    }
  }


}