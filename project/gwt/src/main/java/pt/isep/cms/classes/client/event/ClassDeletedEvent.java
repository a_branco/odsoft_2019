package pt.isep.cms.classes.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ClassDeletedEvent extends GwtEvent<ClassDeletedEventHandler> {
  public final static Type<ClassDeletedEventHandler> TYPE = new Type<ClassDeletedEventHandler>();
  
  @Override
  public Type<ClassDeletedEventHandler> getAssociatedType() {
    return TYPE;
  }

  @Override
  protected void dispatch(ClassDeletedEventHandler handler) {
    handler.onClassDeleted(this);
  }
}
