package pt.isep.cms.classes.client;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.i18n.client.Constants;
import com.google.gwt.user.client.ui.HasWidgets;

import pt.isep.cms.classes.client.event.AddClassEvent;
import pt.isep.cms.classes.client.event.AddClassEventHandler;
import pt.isep.cms.classes.client.event.ClassUpdatedEvent;
import pt.isep.cms.classes.client.event.ClassUpdatedEventHandler;
import pt.isep.cms.classes.client.event.EditClassCancelledEvent;
import pt.isep.cms.classes.client.event.EditClassCancelledEventHandler;
import pt.isep.cms.classes.client.event.EditClassEvent;
import pt.isep.cms.classes.client.event.EditClassEventHandler;
import pt.isep.cms.classes.client.presenter.ClassesPresenter;
import pt.isep.cms.classes.client.presenter.EditClassPresenter;
import pt.isep.cms.classes.client.presenter.Presenter;
import pt.isep.cms.classes.client.view.ClassesDialog;
import pt.isep.cms.classes.client.view.ClassesView;
import pt.isep.cms.client.ShowcaseConstants;


public class ClassesController implements Presenter {
  // (ATB) No history at this level, ValueChangeHandler<String> {
  private final HandlerManager eventBus;
  private final ClassesServiceAsync rpcService;
  private HasWidgets container;

  public static interface CwConstants extends Constants {
  }

  /**
   * An instance of the constants.
   */
  private final CwConstants constants;
  private final ShowcaseConstants globalConstants;


  /**
  * Constructor for ClassesController Class.
  * @param rpcService service.
  * @param eventBus eventBus.
  * @param constants constants.
  */
  public ClassesController(ClassesServiceAsync rpcService,
      HandlerManager eventBus, ShowcaseConstants constants) {
    this.eventBus = eventBus;
    this.rpcService = rpcService;
    this.constants = constants;
    this.globalConstants = constants;

    bind();
  }

  private void bind() {
    // (ATB) No History at this level
    // History.addValueChangeHandler(this);

    eventBus.addHandler(AddClassEvent.TYPE, new AddClassEventHandler() {
      public void onAddClass(AddClassEvent event) {
        doAddNewClass();
      }
    });

    eventBus.addHandler(EditClassEvent.TYPE, new EditClassEventHandler() {
      public void onEditClass(EditClassEvent event) {
        doEditClass(event.getId());
      }
    });

    eventBus.addHandler(EditClassCancelledEvent.TYPE, new EditClassCancelledEventHandler() {
      public void onEditClassCancelled(EditClassCancelledEvent event) {
        doEditClassCancelled();
      }
    });

    eventBus.addHandler(ClassUpdatedEvent.TYPE, new ClassUpdatedEventHandler() {
      public void onClassUpdated(ClassUpdatedEvent event) {
        doClassUpdated();
      }
    });
  }

  private void doAddNewClass() {
    // Lets use the presenter to display a dialog...
    Presenter presenter = new EditClassPresenter(rpcService, eventBus,
        new ClassesDialog(globalConstants, ClassesDialog.Type.ADD));
    presenter.go(container);

  }

  private void doEditClass(String id) {
    Presenter presenter = new EditClassPresenter(rpcService, eventBus,
         new ClassesDialog(globalConstants, ClassesDialog.Type.UPDATE), id);
    presenter.go(container);
  }

  private void doEditClassCancelled() {
    // Nothing to update...
  }

  /**
  * check if Class was Updated.
  */
  private void doClassUpdated() {
    // (ATB) Update the list of Classes...
    Presenter presenter = new ClassesPresenter(rpcService, eventBus, new ClassesView());
    presenter.go(container);
  }

  /**
  * go somewhere.
  * @param container container.
  */
  public void go(final HasWidgets container) {
    this.container = container;

    Presenter presenter = new ClassesPresenter(rpcService, eventBus, new ClassesView());
    presenter.go(container);
  }

}
