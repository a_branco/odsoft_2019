package pt.isep.cms.classes.client.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.ArrayList;
import java.util.List;

import pt.isep.cms.classes.client.presenter.ClassesPresenter;

public class ClassesView extends Composite implements ClassesPresenter.Display {
  private final Button addButton;
  private final Button deleteButton;
  private FlexTable classesTable;
  private final FlexTable contentTable;
  // private final VerticalPanel vPanel ;

  /**
* Generates Classes View.
*/
  public ClassesView() {
    DecoratorPanel contentTableDecorator = new DecoratorPanel();
    initWidget(contentTableDecorator);
    contentTableDecorator.setWidth("100%");
    contentTableDecorator.setWidth("18em");

    contentTable = new FlexTable();
    contentTable.setWidth("100%");
    contentTable.getCellFormatter().addStyleName(0, 0, "classes-ListContainer");
    contentTable.getCellFormatter().setWidth(0, 0, "100%");
    contentTable.getFlexCellFormatter().setVerticalAlignment(0, 0, DockPanel.ALIGN_TOP);

    // vPanel = new VerticalPanel();
    // initWidget(vPanel);

    // Create the menu
    //
    HorizontalPanel hPanel = new HorizontalPanel();
    hPanel.setBorderWidth(0);
    hPanel.setSpacing(0);
    hPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_LEFT);
    addButton = new Button("Add");
    hPanel.add(addButton);

    deleteButton = new Button("Delete");
    hPanel.add(deleteButton);

    // vPanel.add(hPanel);

    contentTable.getCellFormatter().addStyleName(0, 0, "classes-ListMenu");
    contentTable.setWidget(0, 0, hPanel);

    // Create the classes list
    //
    classesTable = new FlexTable();
    classesTable.setCellSpacing(0);
    classesTable.setCellPadding(0);
    classesTable.setWidth("100%");
    classesTable.addStyleName("classes-ListContents");
    classesTable.getColumnFormatter().setWidth(0, "15px");

    // vPanel.add(classesTable);

    contentTable.setWidget(1, 0, classesTable);

    contentTableDecorator.add(contentTable);
  }

  public HasClickHandlers getAddButton() {
    return addButton;
  }

  public HasClickHandlers getDeleteButton() {
    return deleteButton;
  }

  public HasClickHandlers getList() {
    return classesTable;
  }

  /**
 * sets data.
 * @param data data.
 */
  public void setData(List<String> data) {
    classesTable.removeAllRows();

    for (int i = 0; i < data.size(); ++i) {
      classesTable.setWidget(i, 0, new CheckBox());
      classesTable.setText(i, 1, data.get(i));
    }
  }

  /**
 * recognizes the clicked row number.
 * @param event event.
 * @return returns number of row clicked.
 */
  public int getClickedRow(ClickEvent event) {
    int selectedRow = -1;
    HTMLTable.Cell cell = classesTable.getCellForEvent(event);

    if (cell != null) {
      // Suppress clicks if the user is actually selecting the
      // check box
      //
      if (cell.getCellIndex() > 0) {
        selectedRow = cell.getRowIndex();
      }
    }

    return selectedRow;
  }

  /**
 * gets selected rows.
 * @return List of selected rows.
 */
  public List<Integer> getSelectedRows() {
    List<Integer> selectedRows = new ArrayList<Integer>();

    for (int i = 0; i < classesTable.getRowCount(); ++i) {
      CheckBox checkBox = (CheckBox) classesTable.getWidget(i, 0);
      if (checkBox.getValue()) {
        selectedRows.add(i);
      }
    }

    return selectedRows;
  }

  public Widget asWidget() {
    return this;
  }
}
