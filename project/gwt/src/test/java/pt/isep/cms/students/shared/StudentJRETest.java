package pt.isep.cms.students.shared;

import junit.framework.TestCase;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;


public class StudentJRETest extends TestCase {

  public void testGet_SetId_Student() {
    Student tInitial = new Student("3", "Ze", "Manel", new Date(), "Masculino", "email","1");
    assertEquals("3", tInitial.getId());

    tInitial.setId("100");
    assertEquals("100", tInitial.getId());
  }

  public void testGet_SetFirstName_Student() {
    Student tInitial = new Student("3", "Ze", "Manel", new Date(), "Masculino", "email","1");
    assertEquals("Ze", tInitial.getFirstName());

    tInitial.setFirstName("Antonio");
    assertEquals("Antonio", tInitial.getFirstName());
  }

  public void testGet_SetLastName_Student() {
    Student tInitial = new Student("3", "Ze", "Manel", new Date(), "Masculino", "email","1");
    assertEquals("Manel", tInitial.getLastName());

    tInitial.setLastName("Rogeroni");
    assertEquals("Rogeroni", tInitial.getLastName());
  }

  public void testGet_SetGender_Student() {
    Student tInitial = new Student("3", "Ze", "Manel", new Date(), "Masculino", "email","1");
    assertEquals("Masculino", tInitial.getGender());

    tInitial.setGender("helicopter");
    assertEquals("helicopter", tInitial.getGender());
  }

  public void testGet_SetEmail_Student() {
    Student tInitial = new Student("3", "Ze", "Manel", new Date(), "Masculino", "email","1");
    assertEquals("email", tInitial.getEmail());

    tInitial.setEmail("helicopter");
    assertEquals("helicopter", tInitial.getEmail());
  }

  public void testGet_SetIdClass_Student() {
    Student tInitial = new Student("3", "Ze", "Manel", new Date(), "Masculino", "email","1");
    assertEquals("1", tInitial.getIdClasse());

    tInitial.setIdClasse("2");
    assertEquals("2", tInitial.getIdClasse());
  }

  public void testGet_SetBirthDay_Student() {
    Date nova = Date.from(LocalDate.parse("1974-04-25").atStartOfDay().toInstant(ZoneOffset.UTC));
    Student tInitial = new Student("3", "Ze", "Manel", nova, "Masculino", "email","1");
    assertEquals(nova, tInitial.getBirthday());

    Date novaData = Date.from(LocalDate.parse("1999-04-25").atStartOfDay().toInstant(ZoneOffset.UTC));

    tInitial.setBirthday(novaData);
    assertEquals(novaData, tInitial.getBirthday());
  }

  public void testGetFullName_Student() {
    Student tInitial = new Student("3", "Ze", "Manel", new Date(), "Masculino", "email","1");
    assertEquals("Ze Manel", tInitial.getFullName());
  }

  public void testGetLightWeight_Student() {
    Student tInitial = new Student("3", "Ze", "Manel", new Date(), "Masculino", "email","1");
    StudentDetails studentDetails = new StudentDetails("3", "Ze Manel");

    assertEquals(studentDetails.getDisplayName(), tInitial.getLightWeightStudent().getDisplayName());
  }

  public void testEquals_Student() {
    Student tInitial = new Student("3", "Ze", "Manel", new Date(), "Masculino", "email","1");

    assertFalse(tInitial.equals(null));
    assertTrue(tInitial.equals(tInitial));

    StudentDetails studentDetails = new StudentDetails();
    assertFalse(tInitial.equals(studentDetails));

    Student tInitial_IdDiff = new Student("4", "Ze", "Manel", new Date(), "Masculino", "email","1");
    assertFalse(tInitial.equals(tInitial_IdDiff));

    Student tInitial_FirstNameDiff = new Student("3", "Antonio", "Manel", new Date(), "Masculino", "email","1");
    assertFalse(tInitial.equals(tInitial_FirstNameDiff));

    Student tInitial_LastNameDiff = new Student("3", "Ze", "Jorge", new Date(), "Masculino", "email","1");
    assertFalse(tInitial.equals(tInitial_LastNameDiff));

    Student tInitial_GenderDiff = new Student("3", "Ze", "Manel", new Date(), "Helipcopter", "email","1");
    assertFalse(tInitial.equals(tInitial_GenderDiff));

    Student tInitial_EmailDiff = new Student("3", "Ze", "Manel", new Date(), "Helipcopter", "email123","1");
    assertFalse(tInitial.equals(tInitial_EmailDiff));

    Student tInitial_ClassDiff = new Student("3", "Ze", "Manel", new Date(), "Helipcopter", "email","3");
    assertFalse(tInitial.equals(tInitial_ClassDiff));

    Date nova = Date.from(LocalDate.parse("1999-04-25").atStartOfDay().toInstant(ZoneOffset.UTC));
    Student tInitial_BirthdayDiff = new Student("3", "Ze", "Manel", nova, "Masculino", "email","1");
    assertFalse(tInitial.equals(tInitial_BirthdayDiff));
  }


}

