package pt.isep.cms.students.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import java.util.ArrayList;
import java.util.Date;

import pt.isep.cms.students.client.presenter.StudentsPresenter;
import pt.isep.cms.students.client.view.StudentsView;
import pt.isep.cms.students.shared.Student;
import pt.isep.cms.students.shared.StudentDetails;

// Nao se pode usar o easymock com testes GWT pois este usar reflection e o GWT não consegue "transpile"....
//import static org.easymock.EasyMock.createStrictMock;

public class StudentServiceGWTTest extends GWTTestCase {
  private StudentsPresenter studentsPresenter;
  private StudentsServiceAsync rpcService;
  private HandlerManager eventBus;
  private StudentsPresenter.Display mockDisplay;
  boolean flagDeleteStudent;
  String LastId = "0";

  public String getModuleName() {
    return "pt.isep.cms.students.TestCMSJUnit";
  }

  public void gwtSetUp() {
    rpcService = GWT.create(StudentsService.class);
    mockDisplay = new StudentsView();
    studentsPresenter = new StudentsPresenter(rpcService, eventBus, mockDisplay);
  }

  public void testStudentSort() {
    ArrayList<StudentDetails> studentDetails = new ArrayList<StudentDetails>();
    studentDetails.add(new StudentDetails("0", "c_student"));
    studentDetails.add(new StudentDetails("1", "b_student"));
    studentDetails.add(new StudentDetails("2", "a_student"));
    studentsPresenter.setStudentDetails(studentDetails);
    studentsPresenter.sortStudentDetails();
    assertTrue(studentsPresenter.getStudentDetail(0).getDisplayName().equals("a_student"));
    assertTrue(studentsPresenter.getStudentDetail(1).getDisplayName().equals("b_student"));
    assertTrue(studentsPresenter.getStudentDetail(2).getDisplayName().equals("c_student"));
  }

//	public void testStudentsService() {
//		StudentsServiceAsync studentsService = GWT.create(StudentsService.class);
//		ServiceDefTarget target = (ServiceDefTarget) studentsService;
//		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "students/studentsService");
//
//		delayTestFinish(10000);
//
//		studentsService.getStudent("2", new AsyncCallback<Student>() {
//			public void onFailure(Throwable caught) {
//				fail("Request failure: " + caught.getMessage());
//			}
//
//			public void onSuccess(Student result) {
//				assertTrue(result != null);
//				finishTest();
//			}
//		});
//	}

  public void testUpdateStudentService() {

    StudentsServiceAsync studentsService = GWT.create(StudentsService.class);
    ServiceDefTarget target = (ServiceDefTarget) studentsService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "students/studentsService");

    delayTestFinish(10000);


    Date bday = new Date(System.currentTimeMillis());

    Student student = new Student("1", "Deolinda", "Pinto", bday, "Feminino", "email", "1");

    studentsService.updateStudent(student, new AsyncCallback<Student>() {

      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Student result) {


        assertEquals(result.getId(), "1");
        assertEquals(result.getFirstName(), "Deolinda");
        assertEquals(result.getLastName(), "Pinto");
        assertEquals(result.getGender(), "Feminino");
        assertEquals(result.getEmail(), "email");
        assertEquals(result.getIdClasse(), "1");
        assertEquals(result.getBirthday(), bday);

        finishTest();
      }
    });
  }

  public void testAddStudentsService() {
    StudentsServiceAsync studentsService = GWT.create(StudentsService.class);
    ServiceDefTarget target = (ServiceDefTarget) studentsService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "students/studentsService");

    delayTestFinish(10000);


    Student student = new Student("1", "Deolinda", "Pinto", new Date(), "Feminino", "email","1");

    studentsService.addStudent(student, new AsyncCallback<Student>() {
      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Student result) {

        studentsService.getStudentDetails(new AsyncCallback<ArrayList<StudentDetails>>() {

          @Override
          public void onFailure(Throwable caught) {
            fail("Request failure: " + caught.getMessage());
          }

          @Override
          public void onSuccess(ArrayList<StudentDetails> result) {
            boolean studentInList = false;
            boolean studentNotInList = false;

            for (StudentDetails studentDetails : result) {
              if (studentDetails.getDisplayName().equals(student.getFullName())) {
                studentInList = true;
              }
              if (studentDetails.getDisplayName().equals("test")) {
                studentNotInList = true;
              }
            }
            assertTrue(studentInList);
            assertFalse(studentNotInList);
            finishTest();
          }
        });
      }
    });
  }

  public void assertStudent() {
    assertTrue(true);
  }

	/*public void testDeleteStudentService() {
		StudentsServiceAsync studentsService = GWT.create(StudentsService.class);
		ServiceDefTarget target = (ServiceDefTarget) studentsService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "students/studentsService");

		delayTestFinish(10000);

		Student student1 = new Student("1", "Jorge", "Potato", new Date(), "Feminino","email","1");

		studentsService.addStudent(student1, new AsyncCallback<Student>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Student result) {

				studentsService.getStudentDetails(new AsyncCallback<ArrayList<StudentDetails>>() {
					@Override
					public void onFailure(Throwable caught) {
						fail("Request failure: " + caught.getMessage());
					}

					@Override
					public void onSuccess(ArrayList<StudentDetails> result) {
						String LastId = "0";

						for (StudentDetails studentDetails : result) {
							if (Float.valueOf(studentDetails.getId()) > Float.valueOf(LastId)) {
								LastId = studentDetails.getId();
							}
						}

						int ultimoId = (int) (Float.valueOf(LastId) + 4);
						LastId = String.valueOf(ultimoId);

						//student1.setId(LastId);
						//assertEquals(2213,result.size());

						studentsService.deleteStudent(LastId, new AsyncCallback<Boolean>() {
							@Override
							public void onFailure(Throwable caught) {
								fail("Request failure: " + caught.getMessage());
							}

							@Override
							public void onSuccess(Boolean result) {

								assertTrue(result);

								studentsService.getStudentDetails(new AsyncCallback<ArrayList<StudentDetails>>() {
									@Override
									public void onFailure(Throwable caught) {
										fail("Request failure: " + caught.getMessage());
									}

									@Override
									public void onSuccess(ArrayList<StudentDetails> result) {
										boolean studentInList = false;

										for (StudentDetails studentDetails : result) {
											if (studentDetails.getDisplayName().equals("Jorge Potato")) {
												studentInList = true;
											}
										}
										//assertFalse(studentInList);
										finishTest();
									}
								});
							}
						});
					}
				});
			}
		});
	}

	public void testDeleteStudentsService() {
		StudentsServiceAsync studentsService = GWT.create(StudentsService.class);
		ServiceDefTarget target = (ServiceDefTarget) studentsService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "students/studentsService");

		delayTestFinish(10000);


		Student student1 = new Student("1", "JorgeMultiple", "PotatoMultiple", new Date(), "Feminino", "email","1");

		studentsService.addStudent(student1, new AsyncCallback<Student>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Student result) {
				studentsService.getStudentDetails(new AsyncCallback<ArrayList<StudentDetails>>() {
					@Override
					public void onFailure(Throwable caught) {
						fail("Request failure: " + caught.getMessage());
					}

					@Override
					public void onSuccess(ArrayList<StudentDetails> result) {
						ArrayList<String> strings = new ArrayList<String>();



						for (StudentDetails studentDetails : result) {
							if (Float.valueOf(studentDetails.getId()) > Float.valueOf(LastId)) {
								LastId = studentDetails.getId();
							}
						}

						int ultimoId = (int) (Float.valueOf(LastId) + 4);
						LastId = String.valueOf(ultimoId);

						strings.add("2");
						strings.add(LastId);

						studentsService.deleteStudents(strings, new AsyncCallback<ArrayList<StudentDetails>>() {
							@Override
							public void onFailure(Throwable caught) {
								fail("Request failure: " + caught.getMessage());
							}

							@Override
							public void onSuccess(ArrayList<StudentDetails> result) {

								studentsService.getStudentDetails(new AsyncCallback<ArrayList<StudentDetails>>() {
									@Override
									public void onFailure(Throwable caught) {
										fail("Request failure: " + caught.getMessage());
									}

									@Override
									public void onSuccess(ArrayList<StudentDetails> result) {

										String LastIdnovo = "0";

										for (StudentDetails studentDetails : result) {
											if (Float.valueOf(studentDetails.getId()) > Float.valueOf(LastIdnovo)) {
												LastIdnovo = studentDetails.getId();
											}
										}

										int ultimoId = (int) (Float.valueOf(LastIdnovo) + 4);
										LastIdnovo = String.valueOf(ultimoId);

										assertNotSame(LastId,LastIdnovo);

										boolean studentInList = false;

										for (StudentDetails studentDetails : result) {
											if (studentDetails.getDisplayName().equals("JorgeMultiple PotatoMultiple")) {
												studentInList = true;
											}
										}
										assertFalse(studentInList);
										finishTest();
									}
								});
							}
						});
					}
				});
			}
		});
	}*/

  //tests the View student functionality, not used since it is already being tested in testStudentsService()
	/*public void testViewStudentsService() {
		StudentsServiceAsync studentsService = GWT.create(StudentsService.class);
		ServiceDefTarget target = (ServiceDefTarget) studentsService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "students/studentsService");

		delayTestFinish(10000);


		Student student = new Student();
		Student student1 = new Student("1", "firstName", "lastName",  new Date(), "gender", "email");

		studentsService.updateStudent(student1, new AsyncCallback<Student>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Student result) {

						String id = String.valueOf(result.size() - 1);

						studentsService.getStudent(id, new AsyncCallback<Student>() {
							@Override
							public void onFailure(Throwable caught) {
								fail("Request failure: " + caught.getMessage());
							}

							@Override
							public void onSuccess(Student result) {
								student = result;
								EditStudentPresenter.this.display.getFirstName().setValue(student.getFirstName());
								EditStudentPresenter.this.display.getLastName().setValue(student.getLastName());
								EditStudentPresenter.this.display.getGender().setValue(student.getGender());
								EditStudentPresenter.this.display.getBirthday().setValue(student.getBirthday());
								EditStudentPresenter.this.display.getEmail().setValue(student.getEmail());
								assertEquals(student, result);
								finishTest();
							}
						});

			}
		});

	}*/
}
