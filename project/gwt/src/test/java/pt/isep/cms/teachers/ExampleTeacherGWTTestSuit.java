package pt.isep.cms.teachers;

import com.google.gwt.junit.tools.GWTTestSuite;

import junit.framework.Test;
import junit.framework.TestSuite;
import pt.isep.cms.teachers.client.TeacherServiceGWTTest;

public class ExampleTeacherGWTTestSuit extends GWTTestSuite {
  public static Test suite() {
    TestSuite suite = new TestSuite("Test for the Teachers Application");
    suite.addTestSuite(TeacherServiceGWTTest.class);
    return suite;
  }
}