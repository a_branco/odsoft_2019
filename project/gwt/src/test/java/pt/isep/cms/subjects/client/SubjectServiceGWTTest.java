package pt.isep.cms.subjects.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import java.util.ArrayList;
import java.util.Date;

import pt.isep.cms.subjects.client.presenter.SubjectsPresenter;
import pt.isep.cms.subjects.client.view.SubjectsView;
import pt.isep.cms.subjects.shared.Subject;
import pt.isep.cms.subjects.shared.SubjectDetails;

// Nao se pode usar o easymock com testes GWT pois este usar reflection e o GWT não consegue "transpile"....
//import static org.easymock.EasyMock.createStrictMock;

public class SubjectServiceGWTTest extends GWTTestCase {
  private SubjectsPresenter subjectsPresenter;
  private SubjectsServiceAsync rpcService;
  private HandlerManager eventBus;
  private SubjectsPresenter.Display mockDisplay;
  boolean flagDeleteSubject;
  String LastId = "0";

  public String getModuleName() {
    return "pt.isep.cms.subjects.TestCMSJUnit";
  }

  public void gwtSetUp() {
    rpcService = GWT.create(SubjectsService.class);
    mockDisplay = new SubjectsView();
    subjectsPresenter = new SubjectsPresenter(rpcService, eventBus, mockDisplay);
  }

  public void testSubjectSort() {
    ArrayList<SubjectDetails> subjectDetails = new ArrayList<SubjectDetails>();
    subjectDetails.add(new SubjectDetails("0", "c_subject"));
    subjectDetails.add(new SubjectDetails("1", "b_subject"));
    subjectDetails.add(new SubjectDetails("2", "a_subject"));
    subjectsPresenter.setSubjectDetails(subjectDetails);
    subjectsPresenter.sortSubjectDetails();
    assertTrue(subjectsPresenter.getSubjectDetail(0).getDisplayName().equals("a_subject"));
    assertTrue(subjectsPresenter.getSubjectDetail(1).getDisplayName().equals("b_subject"));
    assertTrue(subjectsPresenter.getSubjectDetail(2).getDisplayName().equals("c_subject"));
  }

//	public void testSubjectsService() {
//		SubjectsServiceAsync subjectsService = GWT.create(SubjectsService.class);
//		ServiceDefTarget target = (ServiceDefTarget) subjectsService;
//		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "subjects/subjectsService");
//
//		delayTestFinish(10000);
//
//		subjectsService.getSubject("2", new AsyncCallback<Subject>() {
//			public void onFailure(Throwable caught) {
//				fail("Request failure: " + caught.getMessage());
//			}
//
//			public void onSuccess(Subject result) {
//				assertTrue(result != null);
//				finishTest();
//			}
//		});
//	}

  public void testUpdateSubjectService() {

    SubjectsServiceAsync subjectsService = GWT.create(SubjectsService.class);
    ServiceDefTarget target = (ServiceDefTarget) subjectsService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "subjects/subjectsService");

    delayTestFinish(10000);


    Date bday = new Date(System.currentTimeMillis());

    Subject subject = new Subject("1", "name", "designation", "infos", "idTeacher");

    subjectsService.updateSubject(subject, new AsyncCallback<Subject>() {

      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Subject result) {


        assertEquals(result.getId(), "1");
        assertEquals(result.getName(), "name");
        assertEquals(result.getDesignation(), "designation");
        assertEquals(result.getInfos(), "infos");
        assertEquals(result.getIdTeacher(), "idTeacher");

        finishTest();
      }
    });
  }

  public void testAddSubjectsService() {
    SubjectsServiceAsync subjectsService = GWT.create(SubjectsService.class);
    ServiceDefTarget target = (ServiceDefTarget) subjectsService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "subjects/subjectsService");

    delayTestFinish(10000);


    Subject subject = new Subject("1", "name", "designation", "infos", "idTeacher");

    subjectsService.addSubject(subject, new AsyncCallback<Subject>() {
      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Subject result) {

        subjectsService.getSubjectDetails(new AsyncCallback<ArrayList<SubjectDetails>>() {

          @Override
          public void onFailure(Throwable caught) {
            fail("Request failure: " + caught.getMessage());
          }

          @Override
          public void onSuccess(ArrayList<SubjectDetails> result) {
            boolean subjectInList = false;
            boolean subjectNotInList = false;

            for (SubjectDetails subjectDetails : result) {
              if (subjectDetails.getDisplayName().equals(subject.getDesignation())) {
                subjectInList = true;
              }
              if (subjectDetails.getDisplayName().equals("test")) {
                subjectNotInList = true;
              }
            }
            assertTrue(subjectInList);
            assertFalse(subjectNotInList);
            finishTest();
          }
        });
      }
    });
  }

  public void assertSubject() {
    assertTrue(true);
  }

    /*public void testDeleteSubjectService() {
        SubjectsServiceAsync subjectsService = GWT.create(SubjectsService.class);
        ServiceDefTarget target = (ServiceDefTarget) subjectsService;
        target.setServiceEntryPoint(GWT.getModuleBaseURL() + "subjects/subjectsService");

        delayTestFinish(10000);

        Subject subject1 = new Subject("1", "name", "Jorge Potato", "infos", "idTeacher");

        subjectsService.addSubject(subject1, new AsyncCallback<Subject>() {
            @Override
            public void onFailure(Throwable caught) {
                fail("Request failure: " + caught.getMessage());
            }

            @Override
            public void onSuccess(Subject result) {

                subjectsService.getSubjectDetails(new AsyncCallback<ArrayList<SubjectDetails>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        fail("Request failure: " + caught.getMessage());
                    }

                    @Override
                    public void onSuccess(ArrayList<SubjectDetails> result) {
                        String LastId = "0";

                        for (SubjectDetails subjectDetails : result) {
                            if (Float.valueOf(subjectDetails.getId()) > Float.valueOf(LastId)) {
                                LastId = subjectDetails.getId();
                            }
                        }

                        int ultimoId = (int) (Float.valueOf(LastId) + 4);
                        LastId = String.valueOf(ultimoId);

                        //subject1.setId(LastId);
                        //assertEquals(2213,result.size());

                        subjectsService.deleteSubject(LastId, new AsyncCallback<Boolean>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                fail("Request failure: " + caught.getMessage());
                            }

                            @Override
                            public void onSuccess(Boolean result) {

                                assertTrue(result);

                                subjectsService.getSubjectDetails(new AsyncCallback<ArrayList<SubjectDetails>>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        fail("Request failure: " + caught.getMessage());
                                    }

                                    @Override
                                    public void onSuccess(ArrayList<SubjectDetails> result) {
                                        boolean subjectInList = false;

                                        for (SubjectDetails subjectDetails : result) {
                                            if (subjectDetails.getDisplayName().equals("Jorge Potato")) {
                                                subjectInList = true;
                                            }
                                        }
                                        //assertFalse(subjectInList);
                                        finishTest();
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    public void testDeleteSubjectsService() {
        SubjectsServiceAsync subjectsService = GWT.create(SubjectsService.class);
        ServiceDefTarget target = (ServiceDefTarget) subjectsService;
        target.setServiceEntryPoint(GWT.getModuleBaseURL() + "subjects/subjectsService");

        delayTestFinish(10000);


      Subject subject1 = new Subject("1", "name", "Jorgemultiple", "infos", "idTeacher");

        subjectsService.addSubject(subject1, new AsyncCallback<Subject>() {
            @Override
            public void onFailure(Throwable caught) {
                fail("Request failure: " + caught.getMessage());
            }

            @Override
            public void onSuccess(Subject result) {
                subjectsService.getSubjectDetails(new AsyncCallback<ArrayList<SubjectDetails>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        fail("Request failure: " + caught.getMessage());
                    }

                    @Override
                    public void onSuccess(ArrayList<SubjectDetails> result) {
                        ArrayList<String> strings = new ArrayList<String>();



                        for (SubjectDetails subjectDetails : result) {
                            if (Float.valueOf(subjectDetails.getId()) > Float.valueOf(LastId)) {
                                LastId = subjectDetails.getId();
                            }
                        }

                        int ultimoId = (int) (Float.valueOf(LastId) + 4);
                        LastId = String.valueOf(ultimoId);

                        strings.add("2");
                        strings.add(LastId);

                        subjectsService.deleteSubjects(strings, new AsyncCallback<ArrayList<SubjectDetails>>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                fail("Request failure: " + caught.getMessage());
                            }

                            @Override
                            public void onSuccess(ArrayList<SubjectDetails> result) {

                                subjectsService.getSubjectDetails(new AsyncCallback<ArrayList<SubjectDetails>>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        fail("Request failure: " + caught.getMessage());
                                    }

                                    @Override
                                    public void onSuccess(ArrayList<SubjectDetails> result) {

										String LastIdnovo = "0";

										for (SubjectDetails subjectDetails : result) {
											if (Float.valueOf(subjectDetails.getId()) > Float.valueOf(LastIdnovo)) {
												LastIdnovo = subjectDetails.getId();
											}
										}

										int ultimoId = (int) (Float.valueOf(LastIdnovo) + 4);
										LastIdnovo = String.valueOf(ultimoId);

										assertNotSame(LastId,LastIdnovo);

                                        boolean subjectInList = false;

                                        for (SubjectDetails subjectDetails : result) {
                                            if (subjectDetails.getDisplayName().equals("Jorgemultiple")) {
                                                subjectInList = true;
                                            }
                                        }
                                        assertFalse(subjectInList);
                                        finishTest();
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }*/

  //tests the View subject functionality, not used since it is already being tested in testSubjectsService()
	/*public void testViewSubjectsService() {
		SubjectsServiceAsync subjectsService = GWT.create(SubjectsService.class);
		ServiceDefTarget target = (ServiceDefTarget) subjectsService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "subjects/subjectsService");

		delayTestFinish(10000);


		Subject subject = new Subject();
		Subject subject1 = new Subject("1", "firstName", "lastName",  new Date(), "gender");

		subjectsService.updateSubject(subject1, new AsyncCallback<Subject>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Subject result) {

						String id = String.valueOf(result.size() - 1);

						subjectsService.getSubject(id, new AsyncCallback<Subject>() {
							@Override
							public void onFailure(Throwable caught) {
								fail("Request failure: " + caught.getMessage());
							}

							@Override
							public void onSuccess(Subject result) {
								subject = result;
								EditSubjectPresenter.this.display.getFirstName().setValue(subject.getFirstName());
								EditSubjectPresenter.this.display.getLastName().setValue(subject.getLastName());
								EditSubjectPresenter.this.display.getGender().setValue(subject.getGender());
								EditSubjectPresenter.this.display.getBirthday().setValue(subject.getBirthday());
								assertEquals(subject, result);
								finishTest();
							}
						});

			}
		});

	}*/
}
