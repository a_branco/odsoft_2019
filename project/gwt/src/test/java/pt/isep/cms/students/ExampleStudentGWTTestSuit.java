package pt.isep.cms.students;

import com.google.gwt.junit.tools.GWTTestSuite;

import junit.framework.Test;
import junit.framework.TestSuite;
import pt.isep.cms.students.client.StudentServiceGWTTest;

public class ExampleStudentGWTTestSuit extends GWTTestSuite {
  public static Test suite() {
    TestSuite suite = new TestSuite("Test for the Teachers Application");
    suite.addTestSuite(StudentServiceGWTTest.class);
    return suite;
  }
}