package pt.isep.cms.subjects;

import com.google.gwt.junit.tools.GWTTestSuite;

import junit.framework.Test;
import junit.framework.TestSuite;
import pt.isep.cms.subjects.client.SubjectServiceGWTTest;

public class ExampleSubjectGWTTestSuit extends GWTTestSuite {
  public static Test suite() {
    TestSuite suite = new TestSuite("Test for the Subjects Application");
    suite.addTestSuite(SubjectServiceGWTTest.class);
    return suite;
  }
}