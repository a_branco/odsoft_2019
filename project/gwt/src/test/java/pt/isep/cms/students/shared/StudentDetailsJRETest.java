package pt.isep.cms.students.shared;

import junit.framework.TestCase;

public class StudentDetailsJRETest extends TestCase {

  public void testGet_SetId_StudentDetails() {
    StudentDetails tInitial = new StudentDetails("3", "Ze Manel");
    assertEquals("3", tInitial.getId());

    tInitial.setId("100");
    assertEquals("100", tInitial.getId());
  }

  public void testGet_SetDisplayName_StudentDetails() {
    StudentDetails tInitial = new StudentDetails("3", "Ze Manel");
    assertEquals("Ze Manel", tInitial.getDisplayName());

    tInitial.setDisplayName("Antonio Rogeroni");
    assertEquals("Antonio Rogeroni", tInitial.getDisplayName());
  }
}

