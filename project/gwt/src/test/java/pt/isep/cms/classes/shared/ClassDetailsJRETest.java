package pt.isep.cms.classes.shared;

import junit.framework.TestCase;

public class ClassDetailsJRETest extends TestCase {

  public void testGet_SetId_ClassDetails() {
    ClassDetails tInitial = new ClassDetails("3", "Ze Manel");
    assertEquals("3", tInitial.getId());

    tInitial.setId("100");
    assertEquals("100", tInitial.getId());
  }

  public void testGet_SetDisplayName_ClassDetails() {
    ClassDetails tInitial = new ClassDetails("3", "Ze Manel");
    assertEquals("Ze Manel", tInitial.getDisplayName());

    tInitial.setDisplayName("Antonio Rogeroni");
    assertEquals("Antonio Rogeroni", tInitial.getDisplayName());
  }
}

