package pt.isep.cms.contacts.shared;

import junit.framework.TestCase;

public class ContactJRETest extends TestCase {

  public void testGet_SetId_Contact() {
    Contact tInitial = new Contact("3", "Ze", "Manel", "Masculino@gmail.com");
    assertEquals("3", tInitial.getId());

    tInitial.setId("100");
    assertEquals("100", tInitial.getId());
  }

  public void testGet_SetFirstName_Contact() {
    Contact tInitial = new Contact("3", "Ze", "Manel", "Masculino@gmail.com");
    assertEquals("Ze", tInitial.getFirstName());

    tInitial.setFirstName("Antonio");
    assertEquals("Antonio", tInitial.getFirstName());
  }

  public void testGet_SetLastName_Contact() {
    Contact tInitial = new Contact("3", "Ze", "Manel", "Masculino@gmail.com");
    assertEquals("Manel", tInitial.getLastName());

    tInitial.setLastName("Rogeroni");
    assertEquals("Rogeroni", tInitial.getLastName());
  }

  public void testGet_SetEmailAddress_Contact() {
    Contact tInitial = new Contact("3", "Ze", "Manel", "Masculino@gmail.com");
    assertEquals("Masculino@gmail.com", tInitial.getEmailAddress());

    tInitial.setEmailAddress("helicopter@gmail.com");
    assertEquals("helicopter@gmail.com", tInitial.getEmailAddress());
  }

  public void testGetFullName_Teacher() {
    Contact tInitial = new Contact("3", "Ze", "Manel", "Masculino@gmail.com");
    assertEquals("Ze Manel", tInitial.getFullName());
  }

  public void testGetLightWeight_Contact() {
    Contact tInitial = new Contact("3", "Ze", "Manel", "Masculino@gmail.com");
    ContactDetails contactDetails = new ContactDetails("3", "Ze Manel");

    assertEquals(contactDetails.getDisplayName(), tInitial.getLightWeightContact().getDisplayName());
  }

}

