package pt.isep.cms.subjects.shared;

import junit.framework.TestCase;

public class SubjectDetailsJRETest extends TestCase {

  public void testGet_SetId_SubjectDetails() {
    SubjectDetails tInitial = new SubjectDetails("3", "Ze Manel");
    assertEquals("3", tInitial.getId());

    tInitial.setId("100");
    assertEquals("100", tInitial.getId());
  }

  public void testGet_SetDisplayName_SubjectDetails() {
    SubjectDetails tInitial = new SubjectDetails("3", "Ze Manel");
    assertEquals("Ze Manel", tInitial.getDisplayName());

    tInitial.setDisplayName("Antonio Rogeroni");
    assertEquals("Antonio Rogeroni", tInitial.getDisplayName());
  }
}

