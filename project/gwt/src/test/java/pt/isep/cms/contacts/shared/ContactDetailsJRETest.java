package pt.isep.cms.contacts.shared;

import junit.framework.TestCase;

public class ContactDetailsJRETest extends TestCase {

  public void testGet_SetId_ContactDetails() {
    ContactDetails tInitial = new ContactDetails("3", "Ze Manel");
    assertEquals("3", tInitial.getId());

    tInitial.setId("100");
    assertEquals("100", tInitial.getId());
  }

  public void testGet_SetDisplayName_ContactDetails() {
    ContactDetails tInitial = new ContactDetails("3", "Ze Manel");
    assertEquals("Ze Manel", tInitial.getDisplayName());

    tInitial.setDisplayName("Antonio Rogeroni");
    assertEquals("Antonio Rogeroni", tInitial.getDisplayName());
  }
}

