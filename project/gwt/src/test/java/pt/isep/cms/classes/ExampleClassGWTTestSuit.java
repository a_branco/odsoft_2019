package pt.isep.cms.classes;

import com.google.gwt.junit.tools.GWTTestSuite;

import junit.framework.Test;
import junit.framework.TestSuite;
import pt.isep.cms.classes.client.ClassServiceGWTTest;

public class ExampleClassGWTTestSuit extends GWTTestSuite {
  public static Test suite() {
    TestSuite suite = new TestSuite("Test for the Classes Application");
    suite.addTestSuite(ClassServiceGWTTest.class);
    return suite;
  }
}