package pt.isep.cms.teachers.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import java.util.ArrayList;
import java.util.Date;

import pt.isep.cms.teachers.client.presenter.TeachersPresenter;
import pt.isep.cms.teachers.client.view.TeachersView;
import pt.isep.cms.teachers.shared.Teacher;
import pt.isep.cms.teachers.shared.TeacherDetails;

// Nao se pode usar o easymock com testes GWT pois este usar reflection e o GWT não consegue "transpile"....
//import static org.easymock.EasyMock.createStrictMock;

public class TeacherServiceGWTTest extends GWTTestCase {
  private TeachersPresenter teachersPresenter;
  private TeachersServiceAsync rpcService;
  private HandlerManager eventBus;
  private TeachersPresenter.Display mockDisplay;
  boolean flagDeleteTeacher;
  String LastId = "0";

  public String getModuleName() {
    return "pt.isep.cms.teachers.TestCMSJUnit";
  }

  public void gwtSetUp() {
    rpcService = GWT.create(TeachersService.class);
    mockDisplay = new TeachersView();
    teachersPresenter = new TeachersPresenter(rpcService, eventBus, mockDisplay);
  }

  public void testTeacherSort() {
    ArrayList<TeacherDetails> teacherDetails = new ArrayList<TeacherDetails>();
    teacherDetails.add(new TeacherDetails("0", "c_teacher"));
    teacherDetails.add(new TeacherDetails("1", "b_teacher"));
    teacherDetails.add(new TeacherDetails("2", "a_teacher"));
    teachersPresenter.setTeacherDetails(teacherDetails);
    teachersPresenter.sortTeacherDetails();
    assertTrue(teachersPresenter.getTeacherDetail(0).getDisplayName().equals("a_teacher"));
    assertTrue(teachersPresenter.getTeacherDetail(1).getDisplayName().equals("b_teacher"));
    assertTrue(teachersPresenter.getTeacherDetail(2).getDisplayName().equals("c_teacher"));
  }

//	public void testTeachersService() {
//		TeachersServiceAsync teachersService = GWT.create(TeachersService.class);
//		ServiceDefTarget target = (ServiceDefTarget) teachersService;
//		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "teachers/teachersService");
//
//		delayTestFinish(10000);
//
//		teachersService.getTeacher("2", new AsyncCallback<Teacher>() {
//			public void onFailure(Throwable caught) {
//				fail("Request failure: " + caught.getMessage());
//			}
//
//			public void onSuccess(Teacher result) {
//				assertTrue(result != null);
//				finishTest();
//			}
//		});
//	}

  public void testUpdateTeacherService() {

    TeachersServiceAsync teachersService = GWT.create(TeachersService.class);
    ServiceDefTarget target = (ServiceDefTarget) teachersService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "teachers/teachersService");

    delayTestFinish(10000);


    Date bday = new Date(System.currentTimeMillis());

    Teacher teacher = new Teacher("1", "Deolinda", "Pinto", bday, "Mulher");

    teachersService.updateTeacher(teacher, new AsyncCallback<Teacher>() {

      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Teacher result) {


        assertEquals(result.getId(), "1");
        assertEquals(result.getFirstName(), "Deolinda");
        assertEquals(result.getLastName(), "Pinto");
        assertEquals(result.getGender(), "Mulher");
        assertEquals(result.getBirthday(), bday);

        finishTest();
      }
    });
  }

  public void testAddTeachersService() {
    TeachersServiceAsync teachersService = GWT.create(TeachersService.class);
    ServiceDefTarget target = (ServiceDefTarget) teachersService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "teachers/teachersService");

    delayTestFinish(10000);


    Teacher teacher = new Teacher("1", "firstName", "lastName", new Date(), "gender");

    teachersService.addTeacher(teacher, new AsyncCallback<Teacher>() {
      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Teacher result) {

        teachersService.getTeacherDetails(new AsyncCallback<ArrayList<TeacherDetails>>() {

          @Override
          public void onFailure(Throwable caught) {
            fail("Request failure: " + caught.getMessage());
          }

          @Override
          public void onSuccess(ArrayList<TeacherDetails> result) {
            boolean teacherInList = false;
            boolean teacherNotInList = false;

            for (TeacherDetails teacherDetails : result) {
              if (teacherDetails.getDisplayName().equals(teacher.getFullName())) {
                teacherInList = true;
              }
              if (teacherDetails.getDisplayName().equals("test")) {
                teacherNotInList = true;
              }
            }
            assertTrue(teacherInList);
            assertFalse(teacherNotInList);
            finishTest();
          }
        });
      }
    });
  }

  public void assertTeacher() {
    assertTrue(true);
  }

    /*public void testDeleteTeacherService() {
        TeachersServiceAsync teachersService = GWT.create(TeachersService.class);
        ServiceDefTarget target = (ServiceDefTarget) teachersService;
        target.setServiceEntryPoint(GWT.getModuleBaseURL() + "teachers/teachersService");

        delayTestFinish(10000);

        Teacher teacher1 = new Teacher("1", "Jorge", "Potato", new Date(), "Feminino");

        teachersService.addTeacher(teacher1, new AsyncCallback<Teacher>() {
            @Override
            public void onFailure(Throwable caught) {
                fail("Request failure: " + caught.getMessage());
            }

            @Override
            public void onSuccess(Teacher result) {

                teachersService.getTeacherDetails(new AsyncCallback<ArrayList<TeacherDetails>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        fail("Request failure: " + caught.getMessage());
                    }

                    @Override
                    public void onSuccess(ArrayList<TeacherDetails> result) {
                        String LastId = "0";

                        for (TeacherDetails teacherDetails : result) {
                            if (Float.valueOf(teacherDetails.getId()) > Float.valueOf(LastId)) {
                                LastId = teacherDetails.getId();
                            }
                        }

                        int ultimoId = (int) (Float.valueOf(LastId) + 4);
                        LastId = String.valueOf(ultimoId);

                        //teacher1.setId(LastId);
                        //assertEquals(2213,result.size());

                        teachersService.deleteTeacher(LastId, new AsyncCallback<Boolean>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                fail("Request failure: " + caught.getMessage());
                            }

                            @Override
                            public void onSuccess(Boolean result) {

                                assertTrue(result);

                                teachersService.getTeacherDetails(new AsyncCallback<ArrayList<TeacherDetails>>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        fail("Request failure: " + caught.getMessage());
                                    }

                                    @Override
                                    public void onSuccess(ArrayList<TeacherDetails> result) {
                                        boolean teacherInList = false;

                                        for (TeacherDetails teacherDetails : result) {
                                            if (teacherDetails.getDisplayName().equals("Jorge Potato")) {
                                                teacherInList = true;
                                            }
                                        }
                                        //assertFalse(teacherInList);
                                        finishTest();
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    public void testDeleteTeachersService() {
        TeachersServiceAsync teachersService = GWT.create(TeachersService.class);
        ServiceDefTarget target = (ServiceDefTarget) teachersService;
        target.setServiceEntryPoint(GWT.getModuleBaseURL() + "teachers/teachersService");

        delayTestFinish(10000);


        Teacher teacher1 = new Teacher("1", "JorgeMultiple", "PotatoMultiple", new Date(), "Feminino");

        teachersService.addTeacher(teacher1, new AsyncCallback<Teacher>() {
            @Override
            public void onFailure(Throwable caught) {
                fail("Request failure: " + caught.getMessage());
            }

            @Override
            public void onSuccess(Teacher result) {
                teachersService.getTeacherDetails(new AsyncCallback<ArrayList<TeacherDetails>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        fail("Request failure: " + caught.getMessage());
                    }

                    @Override
                    public void onSuccess(ArrayList<TeacherDetails> result) {
                        ArrayList<String> strings = new ArrayList<String>();



                        for (TeacherDetails teacherDetails : result) {
                            if (Float.valueOf(teacherDetails.getId()) > Float.valueOf(LastId)) {
                                LastId = teacherDetails.getId();
                            }
                        }

                        int ultimoId = (int) (Float.valueOf(LastId) + 4);
                        LastId = String.valueOf(ultimoId);

                        strings.add("2");
                        strings.add(LastId);

                        teachersService.deleteTeachers(strings, new AsyncCallback<ArrayList<TeacherDetails>>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                fail("Request failure: " + caught.getMessage());
                            }

                            @Override
                            public void onSuccess(ArrayList<TeacherDetails> result) {

                                teachersService.getTeacherDetails(new AsyncCallback<ArrayList<TeacherDetails>>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        fail("Request failure: " + caught.getMessage());
                                    }

                                    @Override
                                    public void onSuccess(ArrayList<TeacherDetails> result) {

										String LastIdnovo = "0";

										for (TeacherDetails teacherDetails : result) {
											if (Float.valueOf(teacherDetails.getId()) > Float.valueOf(LastIdnovo)) {
												LastIdnovo = teacherDetails.getId();
											}
										}

										int ultimoId = (int) (Float.valueOf(LastIdnovo) + 4);
										LastIdnovo = String.valueOf(ultimoId);

										assertNotSame(LastId,LastIdnovo);

                                        boolean teacherInList = false;

                                        for (TeacherDetails teacherDetails : result) {
                                            if (teacherDetails.getDisplayName().equals("JorgeMultiples PotatoMultiple")) {
                                                teacherInList = true;
                                            }
                                        }
                                        assertFalse(teacherInList);
                                        finishTest();
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }*/

  //tests the View teacher functionality, not used since it is already being tested in testTeachersService()
	/*public void testViewTeachersService() {
		TeachersServiceAsync teachersService = GWT.create(TeachersService.class);
		ServiceDefTarget target = (ServiceDefTarget) teachersService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "teachers/teachersService");

		delayTestFinish(10000);


		Teacher teacher = new Teacher();
		Teacher teacher1 = new Teacher("1", "firstName", "lastName",  new Date(), "gender");

		teachersService.updateTeacher(teacher1, new AsyncCallback<Teacher>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Teacher result) {

						String id = String.valueOf(result.size() - 1);

						teachersService.getTeacher(id, new AsyncCallback<Teacher>() {
							@Override
							public void onFailure(Throwable caught) {
								fail("Request failure: " + caught.getMessage());
							}

							@Override
							public void onSuccess(Teacher result) {
								teacher = result;
								EditTeacherPresenter.this.display.getFirstName().setValue(teacher.getFirstName());
								EditTeacherPresenter.this.display.getLastName().setValue(teacher.getLastName());
								EditTeacherPresenter.this.display.getGender().setValue(teacher.getGender());
								EditTeacherPresenter.this.display.getBirthday().setValue(teacher.getBirthday());
								assertEquals(teacher, result);
								finishTest();
							}
						});

			}
		});

	}*/
}
