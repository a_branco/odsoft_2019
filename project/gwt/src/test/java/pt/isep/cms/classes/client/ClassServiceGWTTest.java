package pt.isep.cms.classes.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import java.util.ArrayList;

import pt.isep.cms.classes.client.presenter.ClassesPresenter;
import pt.isep.cms.classes.client.view.ClassesView;
import pt.isep.cms.classes.shared.Classe;
import pt.isep.cms.classes.shared.ClassDetails;

// Nao se pode usar o easymock com testes GWT pois este usar reflection e o GWT não consegue "transpile"....
//import static org.easymock.EasyMock.createStrictMock;

public class ClassServiceGWTTest extends GWTTestCase {
  private ClassesPresenter classesPresenter;
  private ClassesServiceAsync rpcService;
  private HandlerManager eventBus;
  private ClassesPresenter.Display mockDisplay;
  boolean flagDeleteClass;
  String LastId = "0";

  public String getModuleName() {
    return "pt.isep.cms.classes.TestCMSJUnit";
  }

  public void gwtSetUp() {
    rpcService = GWT.create(ClassesService.class);
    mockDisplay = new ClassesView();
    classesPresenter = new ClassesPresenter(rpcService, eventBus, mockDisplay);
  }

  public void testClassSort() {
    ArrayList<ClassDetails> classDetails = new ArrayList<ClassDetails>();
    classDetails.add(new ClassDetails("0", "c_class"));
    classDetails.add(new ClassDetails("1", "b_class"));
    classDetails.add(new ClassDetails("2", "a_class"));
    classesPresenter.setClassDetails(classDetails);
    classesPresenter.sortClassDetails();
    assertTrue(classesPresenter.getClassDetail(0).getDisplayName().equals("a_class"));
    assertTrue(classesPresenter.getClassDetail(1).getDisplayName().equals("b_class"));
    assertTrue(classesPresenter.getClassDetail(2).getDisplayName().equals("c_class"));
  }

//	public void testClassesService() {
//		ClassesServiceAsync classesService = GWT.create(ClassesService.class);
//		ServiceDefTarget target = (ServiceDefTarget) classesService;
//		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "classes/classesService");
//
//		delayTestFinish(10000);
//
//		classesService.getClass("2", new AsyncCallback<Class>() {
//			public void onFailure(Throwable caught) {
//				fail("Request failure: " + caught.getMessage());
//			}
//
//			public void onSuccess(Class result) {
//				assertTrue(result != null);
//				finishTest();
//			}
//		});
//	}

  public void testUpdateClassService() {

    ClassesServiceAsync classesService = GWT.create(ClassesService.class);
    ServiceDefTarget target = (ServiceDefTarget) classesService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "classes/classesService");

    delayTestFinish(10000);

    Classe class1 = new Classe("1", "ola", "Jose");

    classesService.updateClass(class1, new AsyncCallback<Classe>() {

      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Classe result) {

        System.out.print(result.getId());
        System.out.print(result.getName());
        System.out.print(result.getDesignation());
        assertEquals(result.getId(), "1");
        assertEquals(result.getDesignation(), "ola");
        assertEquals(result.getName(), "Jose");

        finishTest();
      }
    });
  }

  public void testAddClassesService() {
    ClassesServiceAsync classesService = GWT.create(ClassesService.class);
    ServiceDefTarget target = (ServiceDefTarget) classesService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "classes/classesService");

    delayTestFinish(10000);


    Classe class1 = new Classe("1", "ola", "Jose");

    classesService.addClass(class1, new AsyncCallback<Classe>() {
      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Classe result) {

        classesService.getClassDetails(new AsyncCallback<ArrayList<ClassDetails>>() {

          @Override
          public void onFailure(Throwable caught) {
            fail("Request failure: " + caught.getMessage());
          }

          @Override
          public void onSuccess(ArrayList<ClassDetails> result) {
            boolean classInList = false;
            boolean classNotInList = false;

            for (ClassDetails classDetails : result) {
              if (classDetails.getDisplayName().equals(class1.getName())) {
                classInList = true;
              }
              if (classDetails.getDisplayName().equals("test")) {
                classNotInList = true;
              }
            }
            assertTrue(classInList);
            assertFalse(classNotInList);
            finishTest();
          }
        });
      }
    });
  }

  public void assertClass() {
    assertTrue(true);
  }
	/*public void testDeleteClasseService() {
		ClassesServiceAsync classesService = GWT.create(ClassesService.class);
		ServiceDefTarget target = (ServiceDefTarget) classesService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "classes/classesService");

		delayTestFinish(10000);

		Classe class1 = new Classe("1", "Jorge", "email");

		classesService.addClass(class1, new AsyncCallback<Classe>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Classe result) {

				classesService.getClassDetails(new AsyncCallback<ArrayList<ClassDetails>>() {
					@Override
					public void onFailure(Throwable caught) {
						fail("Request failure: " + caught.getMessage());
					}

					@Override
					public void onSuccess(ArrayList<ClassDetails> result) {
						String LastId = "0";

						for (ClassDetails classDetails : result) {
							if (Float.valueOf(classDetails.getId()) > Float.valueOf(LastId)) {
								LastId = classDetails.getId();
							}
						}

						int ultimoId = (int) (Float.valueOf(LastId) + 4);
						LastId = String.valueOf(ultimoId);

						//class1.setId(LastId);
						//assertEquals(2213,result.size());

						classesService.deleteClass(LastId, new AsyncCallback<Boolean>() {
							@Override
							public void onFailure(Throwable caught) {
								fail("Request failure: " + caught.getMessage());
							}

							@Override
							public void onSuccess(Boolean result) {

								assertTrue(result);

								classesService.getClassDetails(new AsyncCallback<ArrayList<ClassDetails>>() {
									@Override
									public void onFailure(Throwable caught) {
										fail("Request failure: " + caught.getMessage());
									}

									@Override
									public void onSuccess(ArrayList<ClassDetails> result) {
										boolean classInList = false;

										for (ClassDetails classDetails : result) {
											if (classDetails.getDisplayName().equals("Jorge Potato")) {
												classInList = true;
											}
										}
										//assertFalse(classInList);
										finishTest();
									}
								});
							}
						});
					}
				});
			}
		});
	}

	public void testDeleteClassesService() {
		ClassesServiceAsync classesService = GWT.create(ClassesService.class);
		ServiceDefTarget target = (ServiceDefTarget) classesService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "classes/classesService");

		delayTestFinish(10000);


		Classe class1 = new Classe("1", "JorgeMultiple", "email");

		classesService.addClass(class1, new AsyncCallback<Classe>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Classe result) {
				classesService.getClassDetails(new AsyncCallback<ArrayList<ClassDetails>>() {
					@Override
					public void onFailure(Throwable caught) {
						fail("Request failure: " + caught.getMessage());
					}

					@Override
					public void onSuccess(ArrayList<ClassDetails> result) {
						ArrayList<String> strings = new ArrayList<String>();



						for (ClassDetails classDetails : result) {
							if (Float.valueOf(classDetails.getId()) > Float.valueOf(LastId)) {
								LastId = classDetails.getId();
							}
						}

						int ultimoId = (int) (Float.valueOf(LastId) + 4);
						LastId = String.valueOf(ultimoId);

						strings.add("2");
						strings.add(LastId);

						classesService.deleteClasses(strings, new AsyncCallback<ArrayList<ClassDetails>>() {
							@Override
							public void onFailure(Throwable caught) {
								fail("Request failure: " + caught.getMessage());
							}

							@Override
							public void onSuccess(ArrayList<ClassDetails> result) {

								classesService.getClassDetails(new AsyncCallback<ArrayList<ClassDetails>>() {
									@Override
									public void onFailure(Throwable caught) {
										fail("Request failure: " + caught.getMessage());
									}

									@Override
									public void onSuccess(ArrayList<ClassDetails> result) {

										String LastIdnovo = "0";

										for (ClassDetails classDetails : result) {
											if (Float.valueOf(classDetails.getId()) > Float.valueOf(LastIdnovo)) {
												LastIdnovo = classDetails.getId();
											}
										}

										int ultimoId = (int) (Float.valueOf(LastIdnovo) + 4);
										LastIdnovo = String.valueOf(ultimoId);

										assertNotSame(LastId,LastIdnovo);

										boolean classInList = false;

										for (ClassDetails classDetails : result) {
											if (classDetails.getDisplayName().equals("JorgeMultiple PotatoMultiple")) {
												classInList = true;
											}
										}
										assertFalse(classInList);
										finishTest();
									}
								});
							}
						});
					}
				});
			}
		});
	}
*/
  //tests the View class functionality, not used since it is already being tested in testClassesService()
	/*public void testViewClassesService() {
		ClassesServiceAsync classesService = GWT.create(ClassesService.class);
		ServiceDefTarget target = (ServiceDefTarget) classesService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "classes/classesService");

		delayTestFinish(10000);


		Class class = new Class();
		Class class1 = new Class("1", "firstName", "lastName",  new Date(), "gender");

		classesService.updateClass(class1, new AsyncCallback<Class>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Class result) {

						String id = String.valueOf(result.size() - 1);

						classesService.getClass(id, new AsyncCallback<Class>() {
							@Override
							public void onFailure(Throwable caught) {
								fail("Request failure: " + caught.getMessage());
							}

							@Override
							public void onSuccess(Class result) {
								class = result;
								EditClassPresenter.this.display.getFirstName().setValue(class.getFirstName());
								EditClassPresenter.this.display.getLastName().setValue(class.getLastName());
								EditClassPresenter.this.display.getGender().setValue(class.getGender());
								EditClassPresenter.this.display.getBirthday().setValue(class.getBirthday());
								assertEquals(class, result);
								finishTest();
							}
						});

			}
		});

	}*/
}
