package pt.isep.cms.teachers.shared;

import junit.framework.TestCase;

public class TeacherDetailsJRETest extends TestCase {

  public void testGet_SetId_TeacherDetails() {
    TeacherDetails tInitial = new TeacherDetails("3", "Ze Manel");
    assertEquals("3", tInitial.getId());

    tInitial.setId("100");
    assertEquals("100", tInitial.getId());
  }

  public void testGet_SetDisplayName_TeacherDetails() {
    TeacherDetails tInitial = new TeacherDetails("3", "Ze Manel");
    assertEquals("Ze Manel", tInitial.getDisplayName());

    tInitial.setDisplayName("Antonio Rogeroni");
    assertEquals("Antonio Rogeroni", tInitial.getDisplayName());
  }
}

