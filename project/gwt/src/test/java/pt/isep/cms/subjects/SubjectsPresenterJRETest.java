package pt.isep.cms.subjects;

import com.google.gwt.event.shared.HandlerManager;

import java.util.ArrayList;
import junit.framework.TestCase;

import pt.isep.cms.subjects.client.presenter.SubjectsPresenter;
import pt.isep.cms.subjects.client.SubjectsServiceAsync;
import pt.isep.cms.subjects.shared.SubjectDetails;


import static org.easymock.EasyMock.createStrictMock;

public class SubjectsPresenterJRETest extends TestCase {
  private SubjectsPresenter subjectsPresenter;
  private SubjectsServiceAsync mockRpcService;
  private HandlerManager eventBus;
  private SubjectsPresenter.Display mockDisplay;

  protected void setUp() {
    mockRpcService = createStrictMock(SubjectsServiceAsync.class);
    eventBus = new HandlerManager(null);
    mockDisplay = createStrictMock(SubjectsPresenter.Display.class);
    subjectsPresenter = new SubjectsPresenter(mockRpcService, eventBus, mockDisplay);
  }

  public void testContactSort() {
    ArrayList<SubjectDetails> subjectDetails = new ArrayList<SubjectDetails>();
    subjectDetails.add(new SubjectDetails("0", "c_subject"));
    subjectDetails.add(new SubjectDetails("1", "b_subject"));
    subjectDetails.add(new SubjectDetails("2", "a_subject"));
    subjectsPresenter.setSubjectDetails(subjectDetails);
    subjectsPresenter.sortSubjectDetails();
    assertTrue(subjectsPresenter.getSubjectDetail(0).getDisplayName().equals("a_subject"));
    assertTrue(subjectsPresenter.getSubjectDetail(1).getDisplayName().equals("b_subject"));
    assertTrue(subjectsPresenter.getSubjectDetail(2).getDisplayName().equals("c_subject"));
  }
}
