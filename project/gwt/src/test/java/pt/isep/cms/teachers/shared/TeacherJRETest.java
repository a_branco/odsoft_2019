package pt.isep.cms.teachers.shared;

import junit.framework.TestCase;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

public class TeacherJRETest extends TestCase {

  public void testGet_SetId_Teacher() {
    Teacher tInitial = new Teacher("3", "Ze", "Manel", new Date(), "Masculino");
    assertEquals("3", tInitial.getId());

    tInitial.setId("100");
    assertEquals("100", tInitial.getId());
  }

  public void testGet_SetFirstName_Teacher() {
    Teacher tInitial = new Teacher("3", "Ze", "Manel", new Date(), "Masculino");
    assertEquals("Ze", tInitial.getFirstName());

    tInitial.setFirstName("Antonio");
    assertEquals("Antonio", tInitial.getFirstName());
  }

  public void testGet_SetLastName_Teacher() {
    Teacher tInitial = new Teacher("3", "Ze", "Manel", new Date(), "Masculino");
    assertEquals("Manel", tInitial.getLastName());

    tInitial.setLastName("Rogeroni");
    assertEquals("Rogeroni", tInitial.getLastName());
  }

  public void testGet_SetGender_Teacher() {
    Teacher tInitial = new Teacher("3", "Ze", "Manel", new Date(), "Masculino");
    assertEquals("Masculino", tInitial.getGender());

    tInitial.setGender("helicopter");
    assertEquals("helicopter", tInitial.getGender());
  }

  public void testGet_SetBirthDay_Teacher() {
    Date nova = Date.from(LocalDate.parse("1974-04-25").atStartOfDay().toInstant(ZoneOffset.UTC));
    Teacher tInitial = new Teacher("3", "Ze", "Manel", nova, "Masculino");
    assertEquals(nova, tInitial.getBirthday());

    Date novaData = Date.from(LocalDate.parse("1999-04-25").atStartOfDay().toInstant(ZoneOffset.UTC));

    tInitial.setBirthday(novaData);
    assertEquals(novaData, tInitial.getBirthday());
  }

  public void testGetFullName_Teacher() {
    Teacher tInitial = new Teacher("3", "Ze", "Manel", new Date(), "Masculino");
    assertEquals("Ze Manel", tInitial.getFullName());
  }

  public void testGetLightWeight_Teacher() {
    Teacher tInitial = new Teacher("3", "Ze", "Manel", new Date(), "Masculino");
    TeacherDetails teacherDetails = new TeacherDetails("3", "Ze Manel");

    assertEquals(teacherDetails.getDisplayName(), tInitial.getLightWeightTeacher().getDisplayName());
  }

  public void testEquals_Teacher() {
    Teacher tInitial = new Teacher("3", "Ze", "Manel", new Date(), "Masculino");

    assertFalse(tInitial.equals(null));
    assertTrue(tInitial.equals(tInitial));

    TeacherDetails teacherDetails = new TeacherDetails();
    assertFalse(tInitial.equals(teacherDetails));

    Teacher tInitial_IdDiff = new Teacher("4", "Ze", "Manel", new Date(), "Masculino");
    assertFalse(tInitial.equals(tInitial_IdDiff));

    Teacher tInitial_FirstNameDiff = new Teacher("3", "Antonio", "Manel", new Date(), "Masculino");
    assertFalse(tInitial.equals(tInitial_FirstNameDiff));

    Teacher tInitial_LastNameDiff = new Teacher("3", "Ze", "Jorge", new Date(), "Masculino");
    assertFalse(tInitial.equals(tInitial_LastNameDiff));

    Teacher tInitial_GenderDiff = new Teacher("3", "Ze", "Manel", new Date(), "Helipcopter");
    assertFalse(tInitial.equals(tInitial_GenderDiff));

    Date nova = Date.from(LocalDate.parse("1999-04-25").atStartOfDay().toInstant(ZoneOffset.UTC));
    Teacher tInitial_BirthdayDiff = new Teacher("3", "Ze", "Manel", nova, "Masculino");
    assertFalse(tInitial.equals(tInitial_BirthdayDiff));
  }


}

