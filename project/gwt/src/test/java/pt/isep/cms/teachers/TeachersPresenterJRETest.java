package pt.isep.cms.teachers;

import com.google.gwt.event.shared.HandlerManager;

import java.util.ArrayList;
import junit.framework.TestCase;

import pt.isep.cms.teachers.client.presenter.TeachersPresenter;
import pt.isep.cms.teachers.client.TeachersServiceAsync;
import pt.isep.cms.teachers.shared.TeacherDetails;


import static org.easymock.EasyMock.createStrictMock;

public class TeachersPresenterJRETest extends TestCase {
  private TeachersPresenter teachersPresenter;
  private TeachersServiceAsync mockRpcService;
  private HandlerManager eventBus;
  private TeachersPresenter.Display mockDisplay;

  protected void setUp() {
    mockRpcService = createStrictMock(TeachersServiceAsync.class);
    eventBus = new HandlerManager(null);
    mockDisplay = createStrictMock(TeachersPresenter.Display.class);
    teachersPresenter = new TeachersPresenter(mockRpcService, eventBus, mockDisplay);
  }

  public void testContactSort() {
    ArrayList<TeacherDetails> teacherDetails = new ArrayList<TeacherDetails>();
    teacherDetails.add(new TeacherDetails("0", "c_teacher"));
    teacherDetails.add(new TeacherDetails("1", "b_teacher"));
    teacherDetails.add(new TeacherDetails("2", "a_teacher"));
    teachersPresenter.setTeacherDetails(teacherDetails);
    teachersPresenter.sortTeacherDetails();
    assertTrue(teachersPresenter.getTeacherDetail(0).getDisplayName().equals("a_teacher"));
    assertTrue(teachersPresenter.getTeacherDetail(1).getDisplayName().equals("b_teacher"));
    assertTrue(teachersPresenter.getTeacherDetail(2).getDisplayName().equals("c_teacher"));
  }
}
