package pt.isep.cms.subjects.shared;

import junit.framework.TestCase;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;

public class SubjectJRETest extends TestCase {

  public void testGet_SetId_Subject() {
    Subject tInitial = new Subject("3", "Ze", "Manel", "nifos", "Masculino");
    assertEquals("3", tInitial.getId());

    tInitial.setId("100");
    assertEquals("100", tInitial.getId());
  }

  public void testGet_SetName_Subject() {
    Subject tInitial = new Subject("3", "Ze", "Manel", "nifos", "Masculino");
    assertEquals("Ze", tInitial.getName());

    tInitial.setName("Antonio");
    assertEquals("Antonio", tInitial.getName());
  }

  public void testGet_SetDesignation_Subject() {
    Subject tInitial = new Subject("3", "Ze", "Manel", "nifos", "Masculino");
    assertEquals("Manel", tInitial.getDesignation());

    tInitial.setDesignation("Rogeroni");
    assertEquals("Rogeroni", tInitial.getDesignation());
  }

  public void testGet_SetInfos_Subject() {
    Subject tInitial = new Subject("3", "Ze", "Manel", "infos", "Masculino");
    assertEquals("infos", tInitial.getInfos());

    tInitial.setInfos("helicopter");
    assertEquals("helicopter", tInitial.getInfos());
  }

  public void testGet_SetIdTeacher_Subject() {
    Subject tInitial = new Subject("3", "Ze", "Manel", "infos", "1");
    assertEquals("1", tInitial.getIdTeacher());

    tInitial.setIdTeacher("2");
    assertEquals("2", tInitial.getIdTeacher());
  }

  public void testGetLightWeight_Subject() {
    Subject tInitial = new Subject("3", "Ze", "Manel", "asdasd", "Masculino");
    SubjectDetails subjectDetails = new SubjectDetails("3", "Manel");

    assertEquals(subjectDetails.getDisplayName(), tInitial.getLightWeightSubject().getDisplayName());
  }

  public void testEquals_Subject() {
    Subject tInitial = new Subject("3", "Ze", "Manel", "infos", "1");

    assertFalse(tInitial.equals(null));
    assertTrue(tInitial.equals(tInitial));

    SubjectDetails subjectDetails = new SubjectDetails();
    assertFalse(tInitial.equals(subjectDetails));

    Subject tInitial_IdDiff = new Subject("1", "Ze", "Manel", "infos", "1");
    assertFalse(tInitial.equals(tInitial_IdDiff));

    Subject tInitial_NameDiff = new Subject("3", "123", "Manel", "infos", "1");
    assertFalse(tInitial.equals(tInitial_NameDiff));

    Subject tInitial_DesignationDiff = new Subject("3", "Ze", "123", "infos", "1");
    assertFalse(tInitial.equals(tInitial_DesignationDiff));

    Subject tInitial_InfoDiff = new Subject("3", "Ze", "Manel", "123", "1");
    assertFalse(tInitial.equals(tInitial_InfoDiff));

  }


}

