package pt.isep.cms.classes.shared;

import junit.framework.TestCase;

public class ClassJRETest extends TestCase {

  public void testGet_SetId_Class() {
    Classe tInitial = new Classe("3", "Ze", "Manel");
    assertEquals("3", tInitial.getId());

    tInitial.setId("100");
    assertEquals("100", tInitial.getId());
  }

  public void testGet_SetName_Class() {
    Classe tInitial = new Classe("3", "Ze", "Manel");
    assertEquals("Manel", tInitial.getName());

    tInitial.setName("Antonio");
    assertEquals("Antonio", tInitial.getName());
  }

  public void testGet_SetDesignation_Class() {
    Classe tInitial = new Classe("3", "Ze", "Manel");
    assertEquals("Ze", tInitial.getDesignation());

    tInitial.setDesignation("helicopter");
    assertEquals("helicopter", tInitial.getDesignation());
  }

  public void testEquals_Class() {
    Classe tInitial = new Classe("3", "Ze", "Manel");

    assertFalse(tInitial.equals(null));
    assertTrue(tInitial.equals(tInitial));

    ClassDetails teacherDetails = new ClassDetails();
    assertFalse(tInitial.equals(teacherDetails));

    Classe tInitial_IdDiff = new Classe("4", "Ze", "Manel");
    assertFalse(tInitial.equals(tInitial_IdDiff));

    Classe tInitial_NameDiff = new Classe("3", "Antonio", "Manel");
    assertFalse(tInitial.equals(tInitial_NameDiff));

    Classe tInitial_DesignationDiff = new Classe("3", "Ze", "Jorge");
    assertFalse(tInitial.equals(tInitial_DesignationDiff));

  }


}

