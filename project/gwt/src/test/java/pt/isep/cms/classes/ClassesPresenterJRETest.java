package pt.isep.cms.classes;

import com.google.gwt.event.shared.HandlerManager;

import java.util.ArrayList;
import junit.framework.TestCase;

import pt.isep.cms.classes.client.ClassesServiceAsync;
import pt.isep.cms.classes.client.presenter.ClassesPresenter;
import pt.isep.cms.classes.shared.ClassDetails;

import static org.easymock.EasyMock.createStrictMock;

public class ClassesPresenterJRETest extends TestCase {
  private ClassesPresenter classesPresenter;
  private ClassesServiceAsync mockRpcService;
  private HandlerManager eventBus;
  private ClassesPresenter.Display mockDisplay;

  protected void setUp() {
    mockRpcService = createStrictMock(ClassesServiceAsync.class);
    eventBus = new HandlerManager(null);
    mockDisplay = createStrictMock(ClassesPresenter.Display.class);
    classesPresenter = new ClassesPresenter(mockRpcService, eventBus, mockDisplay);
  }

  public void testContactSort() {
    ArrayList<ClassDetails> classDetails = new ArrayList<ClassDetails>();
    classDetails.add(new ClassDetails("0", "c_teacher"));
    classDetails.add(new ClassDetails("1", "b_teacher"));
    classDetails.add(new ClassDetails("2", "a_teacher"));
    classesPresenter.setClassDetails(classDetails);
    classesPresenter.sortClassDetails();
    assertTrue(classesPresenter.getClassDetail(0).getDisplayName().equals("a_teacher"));
    assertTrue(classesPresenter.getClassDetail(1).getDisplayName().equals("b_teacher"));
    assertTrue(classesPresenter.getClassDetail(2).getDisplayName().equals("c_teacher"));
  }
}
