package pt.isep.cms.contacts.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import java.util.ArrayList;

import pt.isep.cms.contacts.client.presenter.ContactsPresenter;
import pt.isep.cms.contacts.client.view.ContactsView;
import pt.isep.cms.contacts.shared.Contact;
import pt.isep.cms.contacts.shared.ContactDetails;

// Nao se pode usar o easymock com testes GWT pois este usar reflection e o GWT não consegue "transpile"....
//import static org.easymock.EasyMock.createStrictMock;

public class ContactServiceGWTTest extends GWTTestCase {
  private ContactsPresenter contactsPresenter;
  private ContactsServiceAsync rpcService;
  private HandlerManager eventBus;
  private ContactsPresenter.Display mockDisplay;

  public String getModuleName() {
    return "pt.isep.cms.contacts.TestCMSJUnit";
  }

  public void gwtSetUp() {
    rpcService = GWT.create(ContactsService.class);
    mockDisplay = new ContactsView();
    contactsPresenter = new ContactsPresenter(rpcService, eventBus, mockDisplay);
  }

  public void testContactSort() {
    ArrayList<ContactDetails> contactDetails = new ArrayList<ContactDetails>();
    contactDetails.add(new ContactDetails("0", "c_contact"));
    contactDetails.add(new ContactDetails("1", "b_contact"));
    contactDetails.add(new ContactDetails("2", "a_contact"));
    contactsPresenter.setContactDetails(contactDetails);
    contactsPresenter.sortContactDetails();
    assertTrue(contactsPresenter.getContactDetail(0).getDisplayName().equals("a_contact"));
    assertTrue(contactsPresenter.getContactDetail(1).getDisplayName().equals("b_contact"));
    assertTrue(contactsPresenter.getContactDetail(2).getDisplayName().equals("c_contact"));
  }

  public void testContactsService() {
    // Create the service that we will test.
    ContactsServiceAsync contactsService = GWT.create(ContactsService.class);
    ServiceDefTarget target = (ServiceDefTarget) contactsService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "contacts/contactsService");

    // Since RPC calls are asynchronous, we will need to wait for a response
    // after this test method returns. This line tells the test runner to wait
    // up to 10 seconds before timing out.
    delayTestFinish(10000);

    // fail("Ainda nao implementado");

    // Send a request to the server.
    contactsService.getContact("2", new AsyncCallback<Contact>() {
      public void onFailure(Throwable caught) {
        // The request resulted in an unexpected error.
        fail("Request failure: " + caught.getMessage());
      }

      public void onSuccess(Contact result) {
        // Verify that the response is correct.
        assertTrue(result != null);

        // Now that we have received a response, we need to tell the test runner
        // that the test is complete. You must call finishTest() after an
        // asynchronous test finishes successfully, or the test will time out.
        finishTest();
      }
    });
  }

  public void testAddContactsService() {
    ContactsServiceAsync contactsService = GWT.create(ContactsService.class);
    ServiceDefTarget target = (ServiceDefTarget) contactsService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "contacts/contactsService");

    delayTestFinish(10000);


    Contact contact = new Contact("1", "firstName", "lastName", "email@email.com");

    contactsService.addContact(contact, new AsyncCallback<Contact>() {
      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Contact result) {

        contactsService.getContactDetails(new AsyncCallback<ArrayList<ContactDetails>>() {

          @Override
          public void onFailure(Throwable caught) {
            fail("Request failure: " + caught.getMessage());
          }

          @Override
          public void onSuccess(ArrayList<ContactDetails> result) {
            boolean contactInList = false;
            boolean contactNotInList = false;

            for (ContactDetails contactDetails : result) {
              if (contactDetails.getDisplayName().equals(contact.getFullName())) {
                contactInList = true;
              }
              if (contactDetails.getDisplayName().equals("test")) {
                contactNotInList = true;
              }
            }
            assertTrue(contactInList);
            assertFalse(contactNotInList);
            finishTest();
          }
        });
      }
    });
  }

  public void testUpdateContactService() {

    ContactsServiceAsync contactsService = GWT.create(ContactsService.class);
    ServiceDefTarget target = (ServiceDefTarget) contactsService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "contacts/contactsService");

    delayTestFinish(10000);


    Contact contact = new Contact("1", "Deolinda", "Pinto", "deolinda@pinto.com");

    contactsService.updateContact(contact, new AsyncCallback<Contact>() {

      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Contact result) {


        assertEquals(result.getId(), "1");
        assertEquals(result.getFirstName(), "Deolinda");
        assertEquals(result.getLastName(), "Pinto");
        assertEquals(result.getEmailAddress(), "deolinda@pinto.com");

        finishTest();
      }
    });
  }

  public void testDeleteContactService() {
    ContactsServiceAsync contactsService = GWT.create(ContactsService.class);
    ServiceDefTarget target = (ServiceDefTarget) contactsService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "contacts/contactsService");

    delayTestFinish(10000);

    Contact contact1 = new Contact("3", "Ze", "Manel", "ze@manel.com");

    contactsService.addContact(contact1, new AsyncCallback<Contact>() {
      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Contact result) {

        contactsService.deleteContact("23", new AsyncCallback<Boolean>() {
          @Override
          public void onFailure(Throwable caught) {
            fail("Request failure: " + caught.getMessage());
          }

          @Override
          public void onSuccess(Boolean result) {

            contactsService.getContactDetails(new AsyncCallback<ArrayList<ContactDetails>>() {
              @Override
              public void onFailure(Throwable caught) {
                fail("Request failure: " + caught.getMessage());
              }

              @Override
              public void onSuccess(ArrayList<ContactDetails> result) {
                boolean contactInList = false;

                for (ContactDetails contactDetails : result) {
                  if (contactDetails.getDisplayName().equals(contact1.getFullName())) {
                    contactInList = true;
                  }
                }
                assertFalse(contactInList);
                finishTest();
              }
            });
          }
        });
      }
    });
  }

  public void testDeleteContactsService() {
    ContactsServiceAsync contactsService = GWT.create(ContactsService.class);
    ServiceDefTarget target = (ServiceDefTarget) contactsService;
    target.setServiceEntryPoint(GWT.getModuleBaseURL() + "contacts/contactsService");

    delayTestFinish(10000);

    Contact contact1 = new Contact("3", "Ze", "Manel", "ze@manel.com");

    contactsService.addContact(contact1, new AsyncCallback<Contact>() {
      @Override
      public void onFailure(Throwable caught) {
        fail("Request failure: " + caught.getMessage());
      }

      @Override
      public void onSuccess(Contact result) {
        ArrayList<String> strings = new ArrayList<String>();
        strings.add("23");
        strings.add("24");
        contactsService.deleteContacts(strings, new AsyncCallback<ArrayList<ContactDetails>>() {
          @Override
          public void onFailure(Throwable caught) {
            fail("Request failure: " + caught.getMessage());
          }

          @Override
          public void onSuccess(ArrayList<ContactDetails> result) {

            contactsService.getContactDetails(new AsyncCallback<ArrayList<ContactDetails>>() {
              @Override
              public void onFailure(Throwable caught) {
                fail("Request failure: " + caught.getMessage());
              }

              @Override
              public void onSuccess(ArrayList<ContactDetails> result) {
                boolean contactInList = false;

                for (ContactDetails contactDetails : result) {
                  if (contactDetails.getDisplayName().equals(contact1.getFullName()) || contactDetails.getDisplayName().equals("Rogerio Rogeroni")) {
                    contactInList = true;
                  }
                }
                assertFalse(contactInList);
                finishTest();
              }
            });
          }
        });
      }
    });
  }
}
