Feature: Teachers

Scenario:  I want to add new teacher
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwTeachers"
            And I wait for 5 sec
            And I click on element having xpath "//button[text()='Add']"
            And I enter "Tiago" into input field having xpath "//table[@class='teachers-ListContainer']/tbody/tr[1]/td/input"
            And I enter "Coelho" into input field having xpath "//table[@class='teachers-ListContainer']/tbody/tr[2]/td/input"
            And I enter "male" into input field having xpath "//table[@class='teachers-ListContainer']/tbody/tr[3]/td/input"
			And I enter "04-06-1998" into input field having xpath "//table[@class='teachers-ListContainer']/tbody/tr[4]/td/input"
            Then I wait for 5 sec
            Then I click on element having xpath "//button[text()='Save']"
            Then I wait for 5 sec

Scenario: I want to edit a teacher
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwTeachers"
            And I wait for 5 sec
			And I click on element having xpath "//table[@class='teachers-ListContents']//td[text()='Tiago Coelho']"
			And I clear input field having xpath "//table[@class='teachers-ListContainer']/tbody/tr[2]/td/input"
			And I enter "Moreira" into input field having xpath "//table[@class='teachers-ListContainer']/tbody/tr[2]/td/input"
			And I click on element having xpath "//button[text()='Save']"
			And I wait for 5 sec

Scenario: I want to remove a teacher
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwTeachers"
			And I wait for 5 sec
			And I check the checkbox having xpath "//table[@class='teachers-ListContents']//td[text()='Tiago Moreira']/preceding-sibling::td//input"
			Then I click on element having xpath "//button[text()='Delete']"
			Then I wait for 5 sec
            And I close browser			