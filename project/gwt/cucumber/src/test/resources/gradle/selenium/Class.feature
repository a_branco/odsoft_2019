Feature: Class

Scenario:  I want to add new class
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwClasses"
            When I click on element having xpath "//button[text()='Add']"
            And I enter "MEI-3A" into input field having xpath "//table[@class='classes-ListContainer']/tbody/tr[1]/td/input"
            And I enter "Mestrado em Engenharia Informatica - Sistemas de Informacao" into input field having xpath "//table[@class='classes-ListContainer']/tbody/tr[2]/td/input"
            Then I wait for 5 sec
            Then I click on element having xpath "//button[text()='Save']"
            Then I wait for 5 sec

Scenario: I want to edit a class
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwClasses"
            And I wait for 5 sec
			And I click on element having xpath "//table[@class='classes-ListContents']//td[text()='Mestrado em Engenharia Informatica - Sistemas de Informacao']"
			And I clear input field having xpath "//table[@class='classes-ListContainer']/tbody/tr[1]/td/input"
			And I enter "MEI-1B" into input field having xpath "//table[@class='classes-ListContainer']/tbody/tr[1]/td/input"
			And I click on element having xpath "//button[text()='Save']"
			And I wait for 5 sec
			
Scenario: I want to remove a class
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwClasses"
			And I wait for 5 sec
			And I check the checkbox having xpath "//table[@class='classes-ListContents']//td[text()='Mestrado em Engenharia Informatica - Sistemas de Informacao']/preceding-sibling::td//input"
			Then I click on element having xpath "//button[text()='Delete']"