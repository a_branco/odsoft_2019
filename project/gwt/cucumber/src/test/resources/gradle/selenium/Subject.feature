Feature: Subjects

Scenario:  I want to add new subject
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwSubjects"
            When I click on element having xpath "//button[text()='Add']"
            And I enter "Analise Matematica" into input field having xpath "//table[@class='subjects-ListContainer']/tbody/tr[1]/td/input"
            And I enter "AMATA" into input field having xpath "//table[@class='subjects-ListContainer']/tbody/tr[2]/td/input"
            And I enter "No Information Attached" into input field having xpath "//table[@class='subjects-ListContainer']/tbody/tr[3]/td/input"
            Then I wait for 5 sec
            Then I click on element having xpath "//button[text()='Save']"
            Then I wait for 5 sec

Scenario: I want to edit a subject
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwSubjects"
            And I wait for 5 sec
			And I click on element having xpath "//table[@class='subjects-ListContents']//td[text()='AMATA']"
			And I clear input field having xpath "//table[@class='subjects-ListContainer']/tbody/tr[1]/td/input"
			And I enter "Analise das Matematicas" into input field having xpath "//table[@class='subjects-ListContainer']/tbody/tr[1]/td/input"
			And I click on element having xpath "//button[text()='Save']"
			And I wait for 5 sec
			
Scenario: I want to remove a subject
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwSubjects"
			And I wait for 5 sec
			And I check the checkbox having xpath "//table[@class='subjects-ListContents']//td[text()='AMATA']/preceding-sibling::td//input"
			Then I click on element having xpath "//button[text()='Delete']"