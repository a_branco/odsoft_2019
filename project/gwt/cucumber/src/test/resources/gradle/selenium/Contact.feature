Feature: Contacts

Scenario:  I want to add new contact
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwContacts"
            When I click on element having xpath "//button[text()='Add']"
            And I enter "Tiago" into input field having xpath "//table[@class='contacts-ListContainer']/tbody/tr[1]/td/input"
            And I enter "Coelho" into input field having xpath "//table[@class='contacts-ListContainer']/tbody/tr[2]/td/input"
            And I enter "tiagocoelho@hotmail.com" into input field having xpath "//table[@class='contacts-ListContainer']/tbody/tr[3]/td/input"
            Then I wait for 5 sec
            Then I click on element having xpath "//button[text()='Save']"
            Then I wait for 5 sec

Scenario: I want to edit a contact
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwContacts"
            And I wait for 5 sec
			And I click on element having xpath "//table[@class='contacts-ListContents']//td[text()='Tiago Coelho']"
			And I clear input field having xpath "//table[@class='contacts-ListContainer']/tbody/tr[2]/td/input"
			And I enter "Moreira" into input field having xpath "//table[@class='contacts-ListContainer']/tbody/tr[2]/td/input"
			And I click on element having xpath "//button[text()='Save']"
			And I wait for 5 sec
			
Scenario: I want to remove a contact
			Given I navigate to "http://192.168.99.100:9004/gwt-1.0/#!CwContacts"
			And I wait for 5 sec
			And I check the checkbox having xpath "//table[@class='contacts-ListContents']//td[text()='Tiago Moreira']/preceding-sibling::td//input"
			Then I click on element having xpath "//button[text()='Delete']"