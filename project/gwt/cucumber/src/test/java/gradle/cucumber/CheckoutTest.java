package gradle.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

// @CucumberOptions(format = {"json:target/cucumber.json"}, plugin = {"pretty", "html:target/cucumber"})
@RunWith(Cucumber.class)
@CucumberOptions(format = {"pretty", "html:build/target/cucumber", "json:build/target/cucumber.json"})
public class CheckoutTest {
}
