package gradle.cucumber;

public class Checkout {

  // 5th -> 8th iteration
  // public void add(int count, int price) { }

  // 5th iteration
  //public int total() {
  //  return 0;
  //}

  // 6th iteration
  //public int total() {
  //  return 40;
  //}

  // 9th iteration
  private int runningTotal = 0;

  public void add(int count, int price) {
     runningTotal += (count * price);
     // System.out.println("total="+runningTotal);
   }

   public int total() {
     return runningTotal;
  }
}
