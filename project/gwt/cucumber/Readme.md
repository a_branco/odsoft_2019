# Install geckodriver

` $ gradle selenium` does not work, because geckodriver is missing.

Install it according to the following instructions:

```
$ wget https://github.com/mozilla/geckodriver/releases/download/v0.23.0/geckodriver-v0.23.0-macos.tar.gz

$ gunzip geckodriver-v0.23.0-macos.tar.gz

$ tar -xf geckodriver-v0.23.0-macos.tar

$ rm geckodriver-v0.23.0-macos.tar
```

