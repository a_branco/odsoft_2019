-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 18-Dez-2019 às 23:35
-- Versão do servidor: 10.4.8-MariaDB
-- versão do PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `cms_odsoft`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `class`
--

CREATE TABLE `class` (
  `idClass` int(11) NOT NULL,
  `designation` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `class`
--

INSERT INTO `class` (`idClass`, `designation`, `name`) VALUES
(2, 'MEI-1A', 'Mestrado em Engenharia Informática - Sistemas Gráficos e Multimédia'),
(3, 'MEI-2A', 'Mestrado em Engenharia Informática - Sistemas Computacionais');

-- --------------------------------------------------------

--
-- Estrutura da tabela `class_has_subject`
--

CREATE TABLE `class_has_subject` (
  `class_idClass` int(11) NOT NULL,
  `subject_idsubject` int(11) NOT NULL,
  `subject_teacher_idTeacher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `student`
--

CREATE TABLE `student` (
  `idStudent` int(11) NOT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `birthDate` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `class_idClass` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `subject`
--

CREATE TABLE `subject` (
  `idsubject` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `designation` varchar(45) DEFAULT NULL,
  `infos` varchar(45) DEFAULT NULL,
  `teacher_idTeacher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `teacher`
--

CREATE TABLE `teacher` (
  `idTeacher` int(11) NOT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `birthDate` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `teacher`
--

INSERT INTO `teacher` (`idTeacher`, `firstName`, `lastName`, `gender`, `birthDate`) VALUES
(4, 'asd', 'qq', 'rr', '1965-12-12'),
(5, 'as', 'a', 'a', '1954-12-12'),
(6, 'Professor', 'OLA', 'Masculino', '1954-12-12'),
(7, 'asd', 'ads', 'asd', '1954-12-12'),
(10, 'asd', 'asd', 'asd', '2019-12-03');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`idClass`);

--
-- Índices para tabela `class_has_subject`
--
ALTER TABLE `class_has_subject`
  ADD PRIMARY KEY (`class_idClass`,`subject_idsubject`,`subject_teacher_idTeacher`),
  ADD KEY `fk_class_has_subject_subject1_idx` (`subject_idsubject`,`subject_teacher_idTeacher`),
  ADD KEY `fk_class_has_subject_class1_idx` (`class_idClass`);

--
-- Índices para tabela `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`idStudent`),
  ADD KEY `fk_student_class_idx` (`class_idClass`);

--
-- Índices para tabela `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`idsubject`,`teacher_idTeacher`),
  ADD KEY `fk_subject_teacher1_idx` (`teacher_idTeacher`);

--
-- Índices para tabela `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`idTeacher`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `class`
--
ALTER TABLE `class`
  MODIFY `idClass` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `student`
--
ALTER TABLE `student`
  MODIFY `idStudent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `teacher`
--
ALTER TABLE `teacher`
  MODIFY `idTeacher` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `class_has_subject`
--
ALTER TABLE `class_has_subject`
  ADD CONSTRAINT `fk_class_has_subject_class1` FOREIGN KEY (`class_idClass`) REFERENCES `class` (`idClass`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_class_has_subject_subject1` FOREIGN KEY (`subject_idsubject`,`subject_teacher_idTeacher`) REFERENCES `subject` (`idsubject`, `teacher_idTeacher`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `fk_student_class` FOREIGN KEY (`class_idClass`) REFERENCES `class` (`idClass`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `fk_subject_teacher1` FOREIGN KEY (`teacher_idTeacher`) REFERENCES `teacher` (`idTeacher`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
